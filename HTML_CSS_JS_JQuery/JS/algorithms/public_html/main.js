function max1(a, b, c) {
    if (a > b) {
        if (a > c) {
            return a;
        } else {
            return c;
        }
    } else {
        if (b > c) {
            return b;
        } else {
            return c;
        }
    }
}
function max2(list) {
    var maxObj = {
        max: list[0],
        index: [0]
    };
    for (var i = 1; i < list.length; i++) {
        if (list[i] > maxObj.max) {
            maxObj.index = [];
            maxObj.index = [i];
            maxObj.max = list[i];
        } else if (maxObj.max === list[i]) {
            maxObj.index.push(i);
        }
    }
    return maxObj;
}
function max3(array) {
    var max = array[0];
    for (var element in array) {
        if (array[element] > max) {
            max = array[element];
        }
    }
    return max;
}
function factorial(n) {
    if(n <= 1){
        return 1;
    }
    return factorial(n-1) * n;
}

function fibo(m) {
    if (m > 1){
        return (fibo(m-1)+fibo(m-2));
    }else if (m === 1){
        return 1;
    }else {
        return 0;
    }
}


var array = [1, 11, 24, 3, 5, 24, 0, 24, 1, 7, 0, 24, 0];
console.log(max1(11, 11, 1));
console.log(array);
console.log(max2(array));
console.log(max3(array));
console.log(factorial(5));
console.log("Fibo:");
console.log(fibo(10));


