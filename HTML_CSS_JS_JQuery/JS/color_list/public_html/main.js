var colorList = [];

function addColor() {
    var newColor = document.getElementById("color_input").value;
    if (containElement(colorList, newColor)) {
        return;
    }
    if (newColor.length > 3 && newColor.length < 15 && newColor !== "wpisz kolor!") {
        document.getElementById("color_input").className = "";
        colorList.push(newColor);
    } else {
        document.getElementById("color_input").className = "incorect";
        document.getElementById("color_input").value = "wpisz kolor!";
    }
    showColor();
}

function deleteColor() {
    var oldColor = document.getElementById("color_input").value;
    if(!containElement(colorList, oldColor)){
        alert("brak koloru na liscie");
        return;
    }
    deleteColorList(colorList, oldColor);
    showColor();
}

function showColor() {
    var divColorList = document.getElementById("color_list");
    divColorList.innerHTML = "";

    for (var i = 0; i < colorList.length; i++) {
        divColorList.innerHTML += "<option>" + colorList[i] + "</option>";
    }
}

function containElement(list, element) {
    for (var i = 0; i < list.length; i++) {
        if (list[i] === element) {
            return true;
        }
    }
    return false;
}

function containElement2(list, element) {
    for (var i in list) {
        if (i === element) {
            return true;
        }
    }
    return false;
}

function elementId(list, element) {
    for (var i = 0; i < list.length; i++)
        if (list[i] === element) {
            return i;
        }
    return -1;
}
function elementIds(list, element) {
    var ids = [];
    for (var i = 0; i < list.length; i++) {
        if (list[i] === element) {
            ids.push(i);
        }
    }
    return ids;
}
function deleteColorList(list, element) {
    for (var i = 0; i < list.length; i++) {
        if (list[i] === element) {
            list.splice(i, 1);
            i--;
        }
    }
}