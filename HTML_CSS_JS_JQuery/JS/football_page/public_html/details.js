function parametrUrl() {
    var qs = window.location.search.substring(1);
    qs = qs.split('+').join(' ');
    var params = {},
            tokens,
            re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
        params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
    }
    return params;
}

function showPlayerInfo() {
    var indexPlayer = parametrUrl().player_index;
    var spanPlayer = document.getElementById("player_details");
    if (players[indexPlayer] === undefined) {
        spanPlayer.innerHTML = "Błędne dane!";
        return;

    }
    spanPlayer.innerHTML = players[indexPlayer].name + " " + players[indexPlayer].surname;
    document.getElementById("player_age").innerHTML = players[indexPlayer].age;
    document.getElementById("player_speed").innerHTML = players[indexPlayer].speed;
    document.getElementById("player_tricks").innerHTML = players[indexPlayer].tricks;
}