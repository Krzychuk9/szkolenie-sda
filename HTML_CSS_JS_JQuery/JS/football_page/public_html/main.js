
var players = [];
function player(name, surname, age, speed, tricks) {
    this.name = name;
    this.surname = surname;
    this.age = age;
    this.speed = speed;
    this.tricks = tricks;
}
players.push(new player("Tomasz", "Zimoch", 60, 40, 99));
players.push(new player("Tomasz", "Kukulski", 25, 60, 69));
players.push(new player("Robert", "Lewandowski", 27, 100, 100));
players.push(new player("Jakub", "Błaszczykowski", 28, 90, 100));
players.push(new player("Łukasz", "Piszczek", 25, 60, 50));


function addPlayers() {
    var divPlayers = document.getElementById("players");
    for (var i = 0; i < players.length; i++) {
        divPlayers.innerHTML += '<a class="player" href="details.html?player_index=' + i + '">' + players[i].name + " " + players[i].surname + '</a>';
    }
}

function addPlatersTable() {
    var tablePlayer = document.getElementById("table_players");
    for (var i = 0; i < players.length; i++) {
        tablePlayer.innerHTML += "<tr>" + "<td>" + players[i].name + "</td>" + "<td>" + players[i].surname + "</td>" + "<td>" + players[i].age + "</td>" + "<td>" + players[i].speed + "</td>" + "<td>" + players[i].tricks + "</td>" + "</tr>";

    }
}