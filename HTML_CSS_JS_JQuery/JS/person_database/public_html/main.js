var people = [];

function addPerson() {
    var name = document.getElementById("name_input").value;
    var surname = document.getElementById("surname_input").value;
    var age = document.getElementById("age_input").value;

    if (isNaN(age) || age === "") {
        alert("Wiek nie jest liczbą");
        return;
    }
    age = Number(age);
    var person = {
        name: name,
        surname: surname,
        age: age
    };
    people.push(person);

    updateHtml();
}

function updateHtml() {
    var select = document.getElementById("people_list");
    select.innerHTML = "";

    for (var i = 0; i < people.length; i++) {
        var personData = people[i].name + " " + people[i].surname;
        select.innerHTML += '<option value = "' + i + '">' + personData + '</option';
    }

    update_age();
}

function update_age() {
    var divAge = document.getElementById("age_output");
    if (people.length === 0) {
        divAge.innerHTML = "";
        return;
    }

    var index = document.getElementById("people_list").value;
    divAge.innerHTML = people[index].age;

}

function popPerson() {
    var index = document.getElementById("people_list").value;
    people.splice(index, 1);
    updateHtml();
}

function updatePerson() {
    var index = document.getElementById("people_list").value;
    var name = document.getElementById("name_input").value;
    var surname = document.getElementById("surname_input").value;
    var age = document.getElementById("age_input").value;
    if (name !== "") {
        people[index].name = name;
    }
    if (surname !== "") {
        people[index].surname = surname;
    }
    if (age !== "") {
        people[index].age = age;
    }
    updateHtml();
}
function serch() {
    var divResults = document.getElementById("results");
    divResults.innerHTML = "";
    var ageL = document.getElementById("ageL").value;
    var ageU = document.getElementById("ageU").value;
    for (var i = 0; i < people.length; i++) {
        var personSerch = people[i];
        if (personSerch.age >= ageL && personSerch.age <= ageU) {
            divResults.innerHTML += "<div>" + personSerch.name + " " + personSerch.surname + " , wiek: " + personSerch.age + "</div>";
        }

    }
}

function serchName() {
    var sName = document.getElementById("sname").value;
    var divResultsName = document.getElementById("resultsname");
    divResultsName.innerHTML = "";

    for (var i = 0; i < people.length; i++) {
        var result1 = people[i].name + " " + people[i].surname;
        var result2 = people[i].surname + " " + people[i].name;

        var person = people[i];
        if (result1.indexOf(sName) !== -1 || result2.indexOf(sName) !== -1) {
            divResultsName.innerHTML += "<div>" + person.name + " " + person.surname + ", wiek: " + person.age + "</div>";
        }


    }
}