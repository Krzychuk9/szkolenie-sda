function fraction(n) {
    var result = 1;
    for (var i = 2; i <= n; i++) {
        result *= i;
    }
    return result;
}
function kwadrat(n) {
    var result = n * n;
    return result;
}
function trzecia(n) {
    return n * n * n;
}
function dziesiata(n) {
    var result = n;
    for (var i = 1; i < 10; i++) {
        result = result * n;
    }
    return result;
}
function pierwiastek(n) {
    return Math.sqrt(n);
}
function przeciwna(n) {
    return n * -1;
}
function odwrotna(n) {
    return 1 / n;
}

function showError(errorMessage, id) {
    document.getElementById(id).value = errorMessage;
    //document.getElementById("input").style.backgroundColor = "#ee4444";
    document.getElementById(id).className = "incorect";
    document.getElementById("output").value = "######";
}

function calc() {
    var inPutValue = document.getElementById("input").value;

    if (isNaN(inPutValue)) {
        showError("Podana wartość nie jest liczbą", "input");
        return;
    } else {
        var value = Number(inPutValue);
    }
    if (value <= 0) {
        showError("Podano liczbę mniejszą od 0", "input");
        return;
    }
    if (Math.floor(value) !== value) {
        showError("Nie podano liczby całkowitej", "input");
        return;
    }
    document.getElementById("input").className = "";
    document.getElementById("input_option").className = "";
    //var option1 = document.getElementById("input_option").value;
    var slist = document.getElementById("slist");
    var option1 = slist.options[slist.selectedIndex].value;

    switch (option1) {
        case " silnia":
            document.getElementById("output").value = fraction(value);
            break;
        case " kwadrat":
            document.getElementById("output").value = kwadrat(value);
            break;
        case " do trzeciej":
            document.getElementById("output").value = trzecia(value);
            break;
        case " do dziesiatej":
            document.getElementById("output").value = dziesiata(value);
            break;
        case " pierwiastek":
            document.getElementById("output").value = pierwiastek(value);
            break;
        case " przeciwna":
            document.getElementById("output").value = przeciwna(value);
            break;
        case " odwrotna":
            document.getElementById("output").value = odwrotna(value);
            break;
        default:
            showError("Wybierz prawidłową opcję", "input_option");
    }
}
function clearValue() {
    document.getElementById("input").value = " ";
    document.getElementById("input").className = "";
}
function clearValue2() {
    document.getElementById("input_option").value = " ";
    document.getElementById("input_option").className = "";
}
function choosenOption() {
    var slist = document.getElementById("slist");
    var option1 = slist.options[slist.selectedIndex].value;
    document.getElementById("input_option").value = option1;
}
