SELECT DISTINCT
	pilkarz.imie, pilkarz.nazwisko
FROM
	historia
    inner join 
    druzyna on historia.id_druzyna = druzyna.id_druzyna
    inner join
    pilkarz on  historia.id_pilkarz = pilkarz.id_pilkarz    
WHERE 
	historia.id_druzyna = 1;