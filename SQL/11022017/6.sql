Select
pilkarz.imie, pilkarz.nazwisko, druzyna.nazwa
from
druzyna inner join
pilkarz on druzyna.id_druzyna = pilkarz.id_aktualnej_druzyny
inner join
(SELECT
max(pilkarz.strzal) As 'max_strzal', druzyna.id_druzyna AS 'ID'
FROM 
pilkarz inner join
druzyna on pilkarz.id_aktualnej_druzyny = druzyna.id_druzyna
group by
druzyna.id_druzyna) AS druzyna_max on pilkarz.id_aktualnej_druzyny = druzyna_max.ID AND pilkarz.strzal = druzyna_max.max_strzal
