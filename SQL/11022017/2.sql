SELECT
	druzyna.nazwa
FROM
	druzyna
    inner join
    pilkarz on druzyna.id_druzyna = pilkarz.id_aktualnej_druzyny
Group by
	druzyna.nazwa
HAVING
	count(*) >= 2