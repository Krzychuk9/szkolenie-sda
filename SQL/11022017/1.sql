SELECT
	marka.id_marka, nazwa, count(*) AS 'liczba osob w zarzadzie', AVG(osoba.rok_urodzenia) AS 'sredni rok urodzenia'
FROM
	zarzad
    INNER JOIN
    osoba ON zarzad.id_osoba = osoba.id_osoba
    INNER JOIN
    marka ON zarzad.id_marka = marka.id_marka
Group by
	marka.nazwa, marka.id_marka
order by marka.id_markadruzynadruzyna