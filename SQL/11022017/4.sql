SELECT
	pilkarz.imie AS Imie, pilkarz.nazwisko AS Nazwisko
FROM
	pilkarz
    inner join
    historia on pilkarz.id_pilkarz = historia.id_pilkarz
GROUP BY
	pilkarz.imie, pilkarz.nazwisko
HAVING
	count(*) >= 2