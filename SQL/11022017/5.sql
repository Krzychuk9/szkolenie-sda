SELECT
	pilkarz.imie AS Imie, pilkarz.nazwisko AS Nazwisko
FROM
	pilkarz
    inner join
    historia on pilkarz.id_pilkarz = historia.id_pilkarz
    inner join
    druzyna on druzyna.id_druzyna = historia.id_druzyna
GROUP BY
	pilkarz.imie, pilkarz.nazwisko, druzyna.id_druzyna
HAVING
	count(*) >= 2