SELECT
	druzyna.nazwa
FROM
	druzyna
    inner join
    pilkarz on druzyna.id_druzyna = pilkarz.id_aktualnej_druzyny
GROUP BY
	druzyna.nazwa
HAVING
	avg(pilkarz.strzal) > 50