update
	employees
    inner join titles on titles.emp_no = employees.emp_no
set
	employees.curr_title = 
    (SELECT 
		titles.title 
			FROM 
				titles
                where titles.emp_no = employees.emp_no
            group by 
				titles.emp_no 
            order by titles.to_date DESC limit 1)
            
