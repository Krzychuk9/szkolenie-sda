UPDATE
	pilkarz
    INNER JOIN
    druzyna on pilkarz.id_aktualnej_druzyny = druzyna.id_druzyna
SET
	pilkarz.Factor_x = 
    CASE
    WHEN druzyna.rok_zalozenia < 1960 THEN 'Y'
    ELSE
    'N'
    END