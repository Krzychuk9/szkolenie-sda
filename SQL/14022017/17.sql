SELECT
	count(*), druzyna.id_druzyna
FROM
	druzyna
    left join
    pilkarz on pilkarz.id_aktualnej_druzyny = druzyna.id_druzyna
group by
	druzyna.id_druzyna
having
	count(*) < 12;
