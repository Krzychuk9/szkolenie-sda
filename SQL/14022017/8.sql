INSERT INTO
	pilkarz(imie, nazwisko, rok_urodzenia, strzal, szybkosc, id_aktualnej_druzyny, Factor_x)
SELECT
	CONCAT("kopia ", pilkarz.imie) AS imie,
    CONCAT("kopia ", pilkarz.nazwisko) AS nazwisko,
    pilkarz.rok_urodzenia,
    pilkarz.strzal + 10 as strzal,
    pilkarz.szybkosc +20 as szybkosc,
    pilkarz.id_aktualnej_druzyny,
    if(pilkarz.Factor_x = 'Y','N','Y') as Factor_X
FROM
	pilkarz