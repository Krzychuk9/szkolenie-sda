SELECT 
    @MAX_STRZAL:=MAX(strzal)
FROM
    pilkarz;

SELECT 
    *
FROM
    pilkarz
WHERE
    pilkarz.strzal = @MAX_STRZAL;