package pl.sda.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import pl.sda.domain.Department;
import pl.sda.domain.Employee;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.math.BigDecimal;
import java.util.List;

public class EmpDAOImpl implements EmpDAO {
    private final SessionFactory sessionFactory;

    public EmpDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Employee findById(int id) throws Exception {
        Session session = sessionFactory.openSession();
        Employee employee = session.get(Employee.class, id);
        session.close();
        return employee;
    }

    @Override
    public void create(Employee employee) throws Exception {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.persist(employee);
            tx.commit();
        } catch (HibernateException ex) {
            ex.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
    }

    @Override
    public void update(Employee employee) throws Exception {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(employee);
            tx.commit();
        } catch (HibernateException ex) {
            ex.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
    }

    @Override
    public void delete(int id) throws Exception {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Employee employee = session.get(Employee.class, id);
            session.remove(employee);
            tx.commit();
        } catch (HibernateException ex) {
            ex.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
    }

    @Override
    public void create(List<Employee> employees) throws Exception {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            employees.forEach(e -> session.persist(e));
            tx.commit();
        } catch (HibernateException ex) {
            ex.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
    }

    @Override
    public BigDecimal getTotalSalaryByDept(int dept) throws Exception {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("select sum(salary) FROM Employee GROUP BY dept having dept.deptno = :no");
        query.setParameter("no", dept);
        BigDecimal singleResult = (BigDecimal) query.getSingleResult();
        session.close();
        return singleResult;
    }

    @Override
    public List<Employee> getEmployeesByDept(int deptNo) {
        Session session = sessionFactory.openSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Employee> criteria = builder.createQuery(Employee.class);
        Root<Employee> root = criteria.from(Employee.class);
        ParameterExpression<Integer> p = builder.parameter(Integer.class);
        criteria.select(root).where(builder.equal(root.get("dept").get("deptno"), p));

        TypedQuery<Employee> query = session.createQuery(criteria);
        query.setParameter(p, deptNo);
        List<Employee> employees = query.getResultList();
        session.close();
        return employees;
    }

    @Override
    public List<Employee> getEmployeeByName(String ename) {
        Session session = sessionFactory.openSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Employee> criteria = builder.createQuery(Employee.class);
        Root<Employee> root = criteria.from(Employee.class);
        ParameterExpression<String> p = builder.parameter(String.class);
        criteria.select(root).where(builder.equal(root.get("ename"), p));

        TypedQuery<Employee> query = session.createQuery(criteria);
        query.setParameter(p, ename);
        List<Employee> resultList = query.getResultList();
        session.close();
        return resultList;
    }
}
