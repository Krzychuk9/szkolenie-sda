package pl.sda.dao;

import org.hibernate.*;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.hibernate.transform.ResultTransformer;
import pl.sda.domain.Department;

import java.io.Serializable;
import java.util.List;

public class DeptDAOImpl implements DeptDAO {
    private final SessionFactory sessionFactory;

    public DeptDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Department findById(int id) throws Exception {
        Session session = sessionFactory.openSession();
        Department department = session.get(Department.class, id);
        session.close();
        return department;

    }

    @Override
    public void create(Department department) throws Exception {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.persist(department);
            tx.commit();
        } catch (HibernateException ex) {
            ex.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
    }

    @Override
    public void update(Department department) throws Exception {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(department);
            tx.commit();
        } catch (HibernateException ex) {
            ex.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
    }

    @Override
    public void updateName(int id, String dname) throws Exception {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Department dep = session.get(Department.class, id);
            dep.setDname(dname);
            session.update(dep);
            tx.commit();
        } catch (HibernateException ex) {
            ex.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
    }

    @Override
    public void delete(int id) throws Exception {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Department department = session.get(Department.class, id);
            session.remove(department);
            tx.commit();
        } catch (HibernateException ex) {
            ex.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
    }

    @Override
    public List<Department> findByName(String dname) {
        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(Department.class);
        criteria.add(Restrictions.eq("dname", dname));
        criteria.setResultTransformer(CriteriaSpecification.ROOT_ENTITY);
        List<Department> list = criteria.list();
        return list;
    }

    @Override
    public List<Department> findByLocation(String location) {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("FROM Department WHERE location = :location");
        query.setParameter("location", location);
        List<Department> list = query.list();
        return list;
    }
}
