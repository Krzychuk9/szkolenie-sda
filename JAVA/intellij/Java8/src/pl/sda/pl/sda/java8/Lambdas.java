package pl.sda.pl.sda.java8;

import java.util.function.Function;
import java.util.function.Predicate;

public class Lambdas {
    private int i = 2;

    interface MyFunction<Param1, Param2, Param3, Param4> {
        Param4 apply(Param1 param1, Param2 param2, Param3 param3);
    }

    public MyFunction<Integer, Integer, Integer, Integer> multiply() {
        return (a, b, c) -> (a * b * c);
    }

    public Function<Integer, Integer> factorial() {
        return (a) -> {
            int result = 1;
            for (int i = 2; i <= a; i++) {
                result *= i;
            }
            return result;
        };
    }

    public Function<String, String> reverse() {
        return (a) -> {
            String c = "";
            for (int i = a.length() - 1; i >= 0; i--) {
                c += a.charAt(i);
            }
            return c;
        };
    }

    interface Number {
        double getValue(int a);
    }

    public Function<Integer, Integer> modulo() {
        return (a) -> (a % i);
    }

//    public double getPI() {
//       Number n2 = new Number(){() -> 3.14};
//        Number n = () -> 3.14;
//        return n.getValue();
//    }

    public double modulo(int x) {
        Number n = (a) -> (a % i);
        return n.getValue(x);
    }

    Predicate<String> p = (s) -> s.contains("Java");

    public static void main(String[] args) {
        Lambdas l = new Lambdas();
        System.out.println(l.modulo(9));
        System.out.println(l.multiply().apply(2, 11, 4));
        System.out.println(l.factorial().apply(5));
        System.out.println(l.reverse().apply("kot"));
        System.out.println(l.p.test("Kurs Java"));
    }
}
