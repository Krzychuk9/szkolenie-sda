package pl.sda.pl.sda.javaold;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        OldJava oldJava = new OldJava();
//        System.out.println(oldJava.factorial(0));
//        oldJava.setMonthsOld();

        Drivers drivers = new Drivers();
        try {
            drivers.getBadDrivers("punkty.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            oldJava.website("adresy.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
