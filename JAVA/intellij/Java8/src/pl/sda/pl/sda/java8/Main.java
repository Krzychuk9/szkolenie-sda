package pl.sda.pl.sda.java8;

import java.util.function.Function;

public class Main {


    public static void main(String[] args) {
        Function<Integer, Integer> multiply = (e) -> e * 2;
        Function<Integer, Integer> sqrt = (e) -> e * e;
        System.out.println(multiply.compose(sqrt).apply(2));
        System.out.println(multiply.andThen(sqrt).apply(2));
    }

}
