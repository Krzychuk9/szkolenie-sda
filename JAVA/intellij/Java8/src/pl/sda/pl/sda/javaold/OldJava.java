package pl.sda.pl.sda.javaold;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

public class OldJava {
    List<String> monthsOld = new ArrayList<>();


    public void setMonthsOld() {
        monthsOld.add("Styczeń");
        monthsOld.add("Luty");
        monthsOld.add("Marzec");
        monthsOld.add("Kwiecień");
        monthsOld.add("Maj");
        monthsOld.add("Czerwiec");
        monthsOld.add("Lipiec");
        monthsOld.add("Sierpień");
        monthsOld.add("Wrzesień");
        monthsOld.add("Październik");
        monthsOld.add("Listopad");
        monthsOld.add("Grzudzień");
        Collections.sort(monthsOld);

        for (String s : monthsOld) {
            System.out.println(s);
        }
    }


    public long factorial(long n) {
        long result = 1;
        for (int i = 2; i <= n; i++) {
            result *= i;
        }
        return result;
    }

    public void website(String fileIn) throws IOException {
        List<String> websites = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(new File(fileIn)))) {
            String line;
            String regex = ".+\\.{1}pl"; //line.endsWith(".pl");
            while ((line = br.readLine()) != null) {
                if (Pattern.matches(regex, line)) {
                    websites.add(line);
                }
            }
            Collections.sort(websites);
            for (String website : websites) {
                System.out.println(website);
            }
        }

    }

}
