package pl.sda.pl.sda.java8;

import java.util.ArrayList;
import java.util.List;

public class LambdaList {
    List<String> list = new ArrayList<>();

    public LambdaList() {
        list.add("Krzysztof");
        list.add("Dawid");
        list.add("Marek");
        list.add("Mikołaj");
    }

    public void print() {
        list.stream().forEach(System.out::println);
    }

    public void sort() {
        list.stream().sorted().forEach(System.out::println);
        list.stream().forEach(s -> System.out.println(s));
    }

    public long count() {
        return list.stream().count();
    }

    public void filter(){
        list.stream().filter((s) -> s.startsWith("M")).filter((s) -> s.length() == 7).forEach(System.out::println);
    }


    public static void main(String[] args) {
        LambdaList col = new LambdaList();
        col.print();
        col.sort();
        System.out.println(col.count());
        col.filter();
    }
}
