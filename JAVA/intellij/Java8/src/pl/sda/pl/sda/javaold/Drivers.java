package pl.sda.pl.sda.javaold;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Drivers {
    private Map<String, Integer> driverData = new HashMap<>();
    private Set<String> badDrivers = new HashSet<>();

    public void getBadDrivers(String fileIn) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(new File(fileIn)))) {
            String line;
            String[] data;
            while ((line = br.readLine()) != null) {
                data = line.split("\\s+");
                driverData.put(data[0], Integer.parseInt(data[1]));
            }
        }
        for (String surname : driverData.keySet()) {
            if (driverData.get(surname) > 24) {
                badDrivers.add(surname);
            }
        }
        for (String badDriver : badDrivers) {
            System.out.println(badDriver);
        }

    }
}
