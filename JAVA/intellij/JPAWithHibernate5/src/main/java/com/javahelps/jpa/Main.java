package com.javahelps.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class Main {
    private static final EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence
            .createEntityManagerFactory("JavaHelps");

    public static void main(String[] args) {

        //dodaj trzy rekordy do bazy danych
        create(1, "Mateusz", 22);
        create(2, "Kasia", 20);
        create(3, "Franek", 25);

        //zaktualizuj wiek rekordu o id = 2
        update(2, "Kasia", 25);

        //usu? z bazy rekord z id = 1
        delete(1);

        //wy?wietl wszystkie rekordy
        List<Student> students = readAll();
        if (students != null) {
            for (Student stu : students) {
                System.out.println(stu);
            }
        }

        ENTITY_MANAGER_FACTORY.close();
    }


    public static void create(int id, String name, int age) {
        EntityManager entityManager = null;
        EntityTransaction transaction = null;
        try {
            entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();
            transaction = entityManager.getTransaction();
            transaction.begin();

            Student student = new Student();
            student.setId(id);
            student.setName(name);
            student.setAge(age);

            entityManager.persist(student);
            transaction.commit();
        } catch (Exception ex) {
            transaction.rollback();
            ex.printStackTrace();
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }

    }

    public static List<Student> readAll() {
        List<Student> students = null;
        EntityManager entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        students = entityManager.createQuery("FROM Student").getResultList();
        transaction.commit();
        return students;
    }

    public static void delete(int id) {
        EntityManager entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        Student student = entityManager.find(Student.class, id);
        entityManager.remove(student);
        transaction.commit();
    }

    public static void update(int id, String name, int age) {
        EntityManager entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        Student student = entityManager.find(Student.class, id);
        student.setName(name);
        student.setAge(age);
        entityManager.persist(student);
        transaction.commit();
    }
}
