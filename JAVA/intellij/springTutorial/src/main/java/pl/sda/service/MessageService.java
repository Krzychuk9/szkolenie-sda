package pl.sda.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.dto.MessageDto;
import pl.sda.model.Message;
import pl.sda.repository.MessageReposiory;

import java.util.Date;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Created by adrian on 01.04.17.
 */
@Service
public class MessageService {

    @Autowired
    private MessageReposiory messageReposiory;

    public void saveMessageToDatabase(MessageDto messageDto) {
        Message message = new Message();
        if (messageDto.getId() != null) {
            message.setId(messageDto.getId());
        }
        message.setSummary(messageDto.getSummary());
        message.setText(messageDto.getText());
        message.setCreated(new Date());
        messageReposiory.save(message);
    }

    public List<MessageDto> getAllMessages() {
        List<Message> all = messageReposiory.findAll();
        List<MessageDto> collect = all.stream().map(message -> new MessageDto(message.getId(), message.getText(), message.getSummary())).collect(Collectors.toList());
        return collect;
    }

    public MessageDto getMessageById(int id) {
        Message messageFromDB = messageReposiory.findOne(id);
        MessageDto messageDto = new MessageDto(messageFromDB.getId(), messageFromDB.getText(), messageFromDB.getSummary());
        return messageDto;
    }
}




















