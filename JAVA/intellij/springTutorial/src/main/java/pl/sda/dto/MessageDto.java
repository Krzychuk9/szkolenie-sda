package pl.sda.dto;

/**
 * Created by adrian on 28.03.17.
 */
public class MessageDto {
    private Integer id;
    private String text;
    private String summary;

    public MessageDto(Integer id, String text, String summary) {
        this.id = id;
        this.text = text;
        this.summary = summary;
    }

    public MessageDto() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
}
