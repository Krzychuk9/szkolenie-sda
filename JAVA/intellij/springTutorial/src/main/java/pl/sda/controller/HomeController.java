/*
 * Copyright 2012-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.sda.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.dto.MessageDto;
import pl.sda.repository.MenuRepository;
import pl.sda.repository.MessageReposiory;
import pl.sda.service.MessageService;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Rob Winch
 * @author Doo-Hwan Kwak
 */
@Controller
@RequestMapping("/")
public class HomeController {

    @Autowired
    @Qualifier(value = "menuRepository")
    private MenuRepository menuRepository;

    @Autowired
    private MessageService messageService;

    @GetMapping("/form")
    public ModelAndView form(ModelMap modelMap) {

        modelMap.addAttribute("messageDto", new MessageDto());
        return new ModelAndView("messages/form", modelMap);
    }

    @GetMapping("/{id}")
    public ModelAndView showUpdatePage(@PathVariable Integer id, ModelMap map) {
        MessageDto messageDto = messageService.getMessageById(id);
        map.addAttribute("messageDto", messageDto);
        return new ModelAndView("messages/form", map);
    }

    @PostMapping("/form")
    public ModelAndView submit(@ModelAttribute(name = "messageDto") MessageDto messageDto) {
        System.out.println(messageDto.getSummary());
        messageService.saveMessageToDatabase(messageDto);
        return new ModelAndView("redirect:/");
    }

    @GetMapping
    public ModelAndView list(ModelMap modelMap) {
        modelMap.addAttribute("paramValue", new Integer(8));
        List<MessageDto> allMessages = messageService.getAllMessages();


        modelMap.addAttribute("messages", allMessages);
        return new ModelAndView("messages/list", modelMap);
    }

}


























