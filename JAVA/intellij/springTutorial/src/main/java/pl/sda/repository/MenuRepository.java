package pl.sda.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.model.Menu;

/**
 * Created by adrian on 28.03.17.
 */
@Repository
public interface MenuRepository extends JpaRepository<Menu, Integer> {
}
