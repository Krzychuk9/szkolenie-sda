package pl.sda.invoicingMaven;

import pl.sda.invoicingMaven.bean.Invoice;
import pl.sda.invoicingMaven.bean.Item;
import pl.sda.invoicingMaven.bean.User;
import pl.sda.invoicingMaven.database.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Session session = sessionFactory.openSession();

//        Query query = session.createQuery("from User u");
//        for (Object o : query.list()) {
//            User user = (User) o;
//            System.out.println(user.getFirstName() + " " + user.getLastName());
//        }

//        Query query = session.createQuery("FROM Invoice");
//        for (Object o : query.list()) {
//            Invoice invoice = (Invoice) o;
//            System.out.println("Invoice nr: " + invoice.getDocumentNumber());
//            for (Item item : invoice.getItems()) {
//                System.out.println(item.getName() + " price: " + item.getPrice().toString());
//            }
//        }
        String hql = "FROM pl.sda.invoicingMaven.bean.User AS u WHERE u.firstName = :firstname";
        Query query = session.createQuery(hql);
        query.setParameter("firstname", "Krzysztof");
        for (Object o : query.list()) {
            User user = (User) o;
            System.out.println(user.getFirstName() + " " + user.getLastName());
        }
        session.close();
        sessionFactory.close();
    }

    public static void saveToDB(Session session) {
        Transaction transaction = session.beginTransaction();

        User userSeller = new User();
        userSeller.setFirstName("Elo");
        userSeller.setLastName("Ziomek");
        userSeller.setAge(22);
        userSeller.setSex(User.Sex.MALE.toString());

        User user = new User();
        user.setFirstName("Daniela");
        user.setLastName("Kasprowska");
        user.setAge(30);
        user.setSex(User.Sex.FEMALE.toString());

        Item item = new Item();
        item.setName("Item1");
        item.setPrice(new BigDecimal(88.90));
        item.setQty(new BigDecimal(1));
        item.setTax(20);
        Item item2 = new Item();
        item2.setName("Item2");
        item2.setPrice(new BigDecimal(11.90));
        item2.setQty(new BigDecimal(4));
        item2.setTax(20);
        List<Item> items = new ArrayList<>();
        items.add(item);
        items.add(item2);

        Invoice invoice = new Invoice();
        invoice.setBuyer(user);
        invoice.setItems(items);
        invoice.setDate(new Date());
        invoice.setDocumenteNumber("No1");
        invoice.setSeller(userSeller);
        BigDecimal total = new BigDecimal(0);
        for (Item item1 : items) {
            total.add(item1.getPrice());
        }
        invoice.setTotal(total);
        item.setInvoice(invoice);
        item2.setInvoice(invoice);

        session.saveOrUpdate(user);
        session.saveOrUpdate(userSeller);
        session.saveOrUpdate(invoice);
        transaction.commit();
    }
}
