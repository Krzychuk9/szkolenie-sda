package pl.sda.invoicingMaven.bean;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
public class Document implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @ManyToOne
    @JoinColumn(name = "seller_id")
    private User seller;
    @Column
    private String documenteNumber;

    public Document() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDocumenteNumber() {
        return documenteNumber;
    }

    public void setDocumenteNumber(String documenteNumber) {
        this.documenteNumber = documenteNumber;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public User getSeller() {
        return seller;
    }

    public void setSeller(User seller) {
        this.seller = seller;
    }

    public String getDocumentNumber() {
        return documenteNumber;
    }

    public void setDocumentNumber(String invoiceNumber) {
        this.documenteNumber = invoiceNumber;
    }

    @Override
    public String toString() {
        return "Document{" +
                "date=" + date +
                ", seller=" + seller +
                ", items=" +
                ", invoiceNumber='" + documenteNumber + '\'' +
                '}';
    }
}
