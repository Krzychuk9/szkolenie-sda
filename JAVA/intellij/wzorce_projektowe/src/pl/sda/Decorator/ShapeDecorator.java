package pl.sda.Decorator;
public class ShapeDecorator implements Shape {
	 protected Shape decorator;

   public ShapeDecorator(Shape decorator){
      this.decorator = decorator;
   }

   public void draw(){
  	 decorator.draw();
   }	
}
