package pl.sda.Decorator;

public class BorderShapeDecorator extends ShapeDecorator {
	
	public BorderShapeDecorator(Shape decoratedShape) {
    super(decoratedShape);		
	}

 @Override
 public void draw() {
	 decorator.draw();	       
	 setYellowBorder(decorator);
 }

 private void setYellowBorder(Shape decoratedShape){
    System.out.println("Kolor obramowania: ��ty");
 }
}
