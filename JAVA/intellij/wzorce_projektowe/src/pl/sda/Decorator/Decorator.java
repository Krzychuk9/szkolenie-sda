package pl.sda.Decorator;
public class Decorator {
	 public static void main(String[] args) {
		 Shape circle = new Circle();
     Shape yellowCircle = new BorderShapeDecorator(new Circle());

     Shape yellowTriangle = new BorderShapeDecorator(new Triangle());
     System.out.println("Ko�o z normalnym obramowanie");
     circle.draw();

     System.out.println("\nKo�o z ��tym obramowaniem");
     yellowCircle.draw();

     System.out.println("\nTr�jk�t z ��tym obramowaniem");
     yellowTriangle.draw();
  }
}
