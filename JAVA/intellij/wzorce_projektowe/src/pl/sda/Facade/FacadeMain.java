package pl.sda.Facade;

public class FacadeMain {
	 public static void main(String[] args) {
     Facade maker = new Facade();

     maker.drawCircle();
     maker.drawTriangle();
  }
}
