package pl.sda.Facade;

public class Facade {
	private Shape circle;
  private Shape triangle;

  public Facade() {
     circle = new Circle();
     triangle = new Triangle();
  }

  public void drawCircle(){
     circle.draw();
  }
  
  public void drawTriangle(){
     triangle.draw();
  }

}
