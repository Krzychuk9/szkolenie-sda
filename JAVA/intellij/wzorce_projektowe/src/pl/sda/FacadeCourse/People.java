package pl.sda.FacadeCourse;

import java.util.ArrayList;
import java.util.List;

public class People {
    List<String> people = new ArrayList<>();

    public People() {
        people.add("Krzysztof");
        people.add("Dawid");
        people.add("Marek");
        people.add("Mikołaj");
    }

    public void sort() {
        System.out.println("W kolejności dodania: ");
        people.stream().forEach(System.out::println);
        System.out.println("Posortowani uczestnicy: ");
        people.stream().sorted().forEach(System.out::println);
    }

    public void filter(int x) {
        System.out.println("Imiona o ograniczonej długości: ");
        people.stream().filter((s) -> s.length() < x).forEach(System.out::println);
    }

    public void filter(String x) {
        System.out.println("Imiona zaczynające się an wybraną literę: ");
        people.stream().filter((s) -> s.startsWith(x)).forEach(System.out::println);
    }


}
