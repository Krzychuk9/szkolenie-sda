package pl.sda.FacadeCourse;

public class PeopleFacade {
    private People people;

    public PeopleFacade() {
        this.people = new People();
    }

    public void doIt(int i, String s) {
        people.sort();
        people.filter(i);
        people.filter(s);
    }
}
