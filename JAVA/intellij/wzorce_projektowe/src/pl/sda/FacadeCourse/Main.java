package pl.sda.FacadeCourse;

public class Main {
    public static void main(String[] args) {
        PeopleFacade peopleFacade = new PeopleFacade();
        peopleFacade.doIt(7,"M");
    }
}
