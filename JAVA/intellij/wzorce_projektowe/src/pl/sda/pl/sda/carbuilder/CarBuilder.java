package pl.sda.pl.sda.carbuilder;

public interface CarBuilder {
    CarBuilder setEngine(double capacity);

    CarBuilder setChassis(String type);

    CarBuilder setBody(String type);

    CarBuilder setInterior(String color);

    Car build();


}
