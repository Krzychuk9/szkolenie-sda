package pl.sda.pl.sda.carbuilder;

public class CarBuilderImpl implements CarBuilder {
    private Car car;

    public CarBuilderImpl() {
        this.car = new Car();
    }

    @Override
    public CarBuilder setEngine(double capacity) {
        car.setEngine(capacity);
        return this;
    }

    @Override
    public CarBuilder setChassis(String type) {
        car.setChassis(type);
        return this;
    }

    @Override
    public CarBuilder setBody(String type) {
        car.setBody(type);
        return this;
    }

    @Override
    public CarBuilder setInterior(String color) {
        car.setInterior(color);
        return this;
    }

    @Override
    public Car build() {
        return car;
    }
}
