package pl.sda.pl.sda.carbuilder;

public class Manufacturer {
    private CarBuilder builder;

    public Manufacturer(CarBuilder builder) {
        this.builder = builder;
    }

    public Car construct() {
        return builder.setEngine(2.0).setChassis("sportowe").setBody("sedan").setInterior("czerwone").build();
    }
}
