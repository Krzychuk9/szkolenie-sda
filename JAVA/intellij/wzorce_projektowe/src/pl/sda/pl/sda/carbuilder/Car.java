package pl.sda.pl.sda.carbuilder;

public class Car {
    private double engine;
    private String chassis;
    private String body;
    private String interior;

    public Car() {
    }

    public double getEngine() {
        return engine;
    }

    public void setEngine(double engine) {
        this.engine = engine;
    }

    public String getChassis() {
        return chassis;
    }

    public void setChassis(String chassis) {
        this.chassis = chassis;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getInterior() {
        return interior;
    }

    public void setInterior(String interior) {
        this.interior = interior;
    }

    @Override
    public String toString() {
        return "Wyprodukowano samochód! Silnik: " + engine + " Podwozie: " + chassis + " Nadwozie: " + body + " Wnętrze: " + interior;
    }
}
