package pl.sda.pl.sda.carbuilder;

public class Main {
    public static void main(String[] args) {
        Manufacturer manufacturer = new Manufacturer(new CarBuilderImpl());
        System.out.println(manufacturer.construct());
    }
}
