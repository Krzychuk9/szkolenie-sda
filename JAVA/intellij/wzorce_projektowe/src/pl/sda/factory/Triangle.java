package pl.sda.factory;

public class Triangle implements Shape {	
	public void draw() {
		System.out.println("Rysuj� tr�jk�t");
	}
	private double a;
	private double h;

	public Triangle(double a, double h) {
		this.a = a;
		this.h = h;
	}

	@Override
	public double area() {
		return 0.5 * a * h;
	}

	@Override
	public double circuit() {

		return 0;
	}
}
