package pl.sda.factory;

/**
 * Created by RENT on 2017-01-23.
 */
public class Trapezoid implements Shape {
    @Override
    public void draw() {
        System.out.println("Rysuje trapez");
    }

    private double h;
    private double a;
    private double b;

    public Trapezoid(double h, double a, double b) {
        this.h = h;
        this.a = a;
        this.b = b;
    }

    @Override
    public double area() {
        return (a + b) * h / 2;
    }

    @Override
    public double circuit() {
        return 0;
    }
}
