package pl.sda.factory;

public class Rectangle implements Shape {
    public void draw() {
        System.out.println("Rysuj� prostok�t");
    }

    private double a;
    private double b;

    public Rectangle(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double area() {
        return a * b;
    }

    @Override
    public double circuit() {

        return 2 * a + 2 * b;
    }
}