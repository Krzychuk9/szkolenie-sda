package pl.sda.factory;

public class Circle implements Shape {
    public void draw() {
        System.out.println("Rysuj� ko�o");
    }

    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }


    @Override
    public double area() {
        return Math.PI * radius * radius;
    }

    @Override
    public double circuit() {
        return (2 * radius * Math.PI);
    }
}
