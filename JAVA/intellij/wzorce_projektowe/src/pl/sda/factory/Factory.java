package pl.sda.factory;

public class Factory {
    public Shape getShape(String type) {
        if (type == null)
            return null;
        if (type.equals("CIRCLE"))
            return new Circle(3.0);
        if (type.equals("TRIANGLE"))
            return new Triangle(3, 5);
        if (type.equals("RECTANGLE"))
            return new Rectangle(4, 3);
        if (type.equals("Trapezoid"))
            return new Trapezoid(1, 2, 3);
        return null;
    }
}
