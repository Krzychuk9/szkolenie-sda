package pl.sda.factory;

public class Main {

    public static void main(String[] args) {
        Factory factory = new Factory();

        Shape shape1 = factory.getShape("CIRCLE");
        shape1.draw();

        Shape shape2 = factory.getShape("TRIANGLE");
        shape2.draw();

        Shape shape3 = factory.getShape("RECTANGLE");
        shape3.draw();
        System.out.println(shape3.area());

        Shape shape4 = factory.getShape("Trapezoid");
        shape4.draw();
    }
}
