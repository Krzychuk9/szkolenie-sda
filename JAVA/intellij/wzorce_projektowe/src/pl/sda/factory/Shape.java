package pl.sda.factory;

public interface Shape {
    void draw();

    double area();

    double circuit();
}
