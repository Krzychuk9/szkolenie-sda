package pl.sda.singleton;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public enum LoggerEnum {
    INSTANCE;  //public final class LoggerEnum   public static final LoggerEnum INSTANCE = new LoggerEnum();

    LoggerEnum() {
    }

    public static LoggerEnum getInstance() {
        return INSTANCE;
    }

    public void saveToLog(String info) throws IOException {
        String fileName = "log.txt";
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(new File(fileName), true))) {
            bw.write(info);
            bw.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
