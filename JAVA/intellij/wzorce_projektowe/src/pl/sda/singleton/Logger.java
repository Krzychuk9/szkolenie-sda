package pl.sda.singleton;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Logger {
    private static Logger instance = null;
    private Logger() {
    }

    public static Logger getInstance() {
        if (instance == null) {
            synchronized (Logger.class) {
                if (instance == null)
                    instance = new Logger();
            }
        }
        return instance;
    }

    public void saveToLog(String info) throws IOException {
        String fileName = "log.txt";
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(new File(fileName), true))) {
            bw.write(info);
            bw.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
