package pl.sda.singleton;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        try {
            Logger.getInstance().saveToLog("Hello");
            LoggerEnum.getInstance().saveToLog("Enum");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
