package pl.sda.Builder.Builder;

public class House {
	private int numberOfRooms;
	private int numberOfDoors;
	private String color;
	
	public House() {}
	
	public int getNumberOfRooms() {
		return this.numberOfRooms;
	}
	
	public int getNumberOfDoors() {
		return this.numberOfDoors;
	}
	
	public String getColor() {
		return this.color;
	}
	
	public void setNumberOfRooms(int numberOfRooms) {
		this.numberOfRooms = numberOfRooms;
	}
	
	public void setNumberOfDoors(int numberOfDoors) {
		this.numberOfDoors = numberOfDoors;
	}
	
	public void setColor(String color) {
		this.color = color;
	}
	
	 @Override
   public String toString() {
       return color + " dom. Ilość drzwi: " + numberOfDoors + " Ilość pokoi: " + numberOfRooms;
   }
}
