package pl.sda.Builder.Builder;

public class HouseBuildDirector {
	
	private HouseBuilder builder;
	
	public HouseBuildDirector(HouseBuilder builder) {
		this.builder = builder;
	}
	
	public House construct() {
		return builder.setColor("Bia�y").setNumberOfDoors(10).setNumberOfRooms(4).build();
	}
	
	 public static void main(final String[] arguments) {
		 HouseBuilder builder = new HouseBuilderImpl();
		 HouseBuildDirector houseBuilderDirector = new HouseBuildDirector(builder);
		 System.out.println(houseBuilderDirector.construct());
 }

}
