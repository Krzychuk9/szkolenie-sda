package pl.sda.Builder.Builder;

public interface HouseBuilder {
	
	HouseBuilder setNumberOfRooms(int numberOfRooms);

	HouseBuilder setNumberOfDoors(int numberOfDoors);
	
	HouseBuilder setColor(String color);
	
	House build();
}
