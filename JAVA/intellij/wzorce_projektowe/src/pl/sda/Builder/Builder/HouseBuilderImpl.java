package pl.sda.Builder.Builder;

public class HouseBuilderImpl implements HouseBuilder {
	
	private House house;
	
	public HouseBuilderImpl() {
		house = new House();
	}
	
	@Override
	public HouseBuilder setNumberOfRooms(int numberOfRooms) {
		house.setNumberOfRooms(numberOfRooms);
		return this;
	}

	@Override
	public HouseBuilder setNumberOfDoors(int numberOfDoors) {
		house.setNumberOfDoors(numberOfDoors);
		return this;
	}
	
	@Override
	public HouseBuilder setColor(String color) {
		house.setColor(color);
		return this;
	}
	
	public House build() {
		return house;
	}
}
