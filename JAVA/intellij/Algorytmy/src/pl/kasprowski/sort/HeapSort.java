package pl.kasprowski.sort;

import java.util.Arrays;

public class HeapSort<T extends Comparable<T>> {

    public HeapSort() {
    }

    public void sort(T[] elements) {
        buildHeap(elements);

        for (int i = 0; i < elements.length; i++) {
            switchNodes(elements, 0, elements.length - 1 - i);
            verifyDownwards(elements, 0, elements.length - i - 1);
        }
    }

    private void verifyDownwards(T[] elements, int nodeIndex, int heapSize) {
        int leftChildIndex = getLeftChildIndex(nodeIndex);
        T leftChild = leftChildIndex < heapSize ? elements[leftChildIndex] : null;
        int rightChildIndex = getRightChildIndex(nodeIndex);
        T rightChild = rightChildIndex < heapSize ? elements[rightChildIndex] : null;
        if (leftChild != null) {
            int maxChildIndex;
            T maxChild;
            if (rightChild != null && rightChild.compareTo(leftChild) > 0) {
                maxChildIndex = rightChildIndex;
                maxChild = rightChild;
            } else {
                maxChildIndex = leftChildIndex;
                maxChild = leftChild;
            }
            if (elements[nodeIndex].compareTo(maxChild) < 0) {
                switchNodes(elements, nodeIndex, maxChildIndex);
                verifyDownwards(elements, maxChildIndex, heapSize);
            }
        }
    }

    private int getLeftChildIndex(int nodeIndex) {
        return nodeIndex * 2 + 1;
    }

    private int getRightChildIndex(int nodeIndex) {
        return nodeIndex * 2 + 2;
    }

    private void buildHeap(T[] elements) {
        for (int i = 0; i < elements.length; i++) {
            verifyUpwards(elements, i);
        }
    }

    private void verifyUpwards(T[] elements, int nodeIndex) {
        if (nodeIndex > 0) {
            int parentIndex = getParentIndex(nodeIndex);
            if (elements[nodeIndex].compareTo(elements[parentIndex]) > 0) {
                switchNodes(elements, nodeIndex, parentIndex);
                verifyUpwards(elements, parentIndex);
            }
        }
    }

    private int getParentIndex(int index) {
        return (index - 1) / 2;
    }

    public void switchNodes(T[] elements, int nodeIndex, int parentIndex) {
        T tmp = elements[nodeIndex];
        elements[nodeIndex] = elements[parentIndex];
        elements[parentIndex] = tmp;
    }
}
