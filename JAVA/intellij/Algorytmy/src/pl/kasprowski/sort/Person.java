package pl.kasprowski.sort;

public class Person implements Comparable<Person> {
    private String name;
    private String surname;

    public Person(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    @Override
    public int compareTo(Person o) {
        int result;
        if ((result = this.surname.compareTo(o.surname)) != 0) {
            return result;
        } else {
            return this.name.compareTo(o.name);
        }
    }

    @Override
    public String toString() {
        return this.name + " " + this.surname;
    }
}
