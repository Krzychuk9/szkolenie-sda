package pl.kasprowski.sort;

import java.util.Arrays;

public class OtherSort {

    public void sort(int[] array) {
        int[] result = new int[array.length];
        int maxValue;
        int maxIndex;
        for (int i = 0; i < array.length; i++) {
            maxValue = array[0];
            maxIndex = 0;
            for (int j = 0; j < array.length; j++) {
                if (maxValue < array[j]) {
                    maxValue = array[j];
                    maxIndex = j;
                }
            }
            result[result.length - 1 - i] = maxValue;
            array[maxIndex] = 0;
        }
        for (int i = 0; i < array.length; i++) {
            array[i] = result[i];
        }
    }
}
