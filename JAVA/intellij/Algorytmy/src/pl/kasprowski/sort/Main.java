package pl.kasprowski.sort;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Integer[] array = {1, 5, 7, 2, 4, 7, 2, 46, 7};
        Person[] personArray = {new Person("Dawid", "Slowinski"), new Person("Krzysztof", "Kasprowski"), new Person("Marek", "Urzon"), new Person("Marek", "Kasprowski")};
        //BubleSort bubleSort = new BubleSort();
        //bubleSort.sort(array);
        //OtherSort otherSort = new OtherSort();
        //otherSort.sort(array);
        HeapSort<Integer> heapSort = new HeapSort<>();
        HeapSort<Person> heapSortPerson = new HeapSort<>();
        heapSort.sort(array);
        heapSortPerson.sort(personArray);
        Arrays.stream(array).forEach(e -> System.out.print(e + " "));
        Arrays.stream(personArray).forEach(s -> System.out.print(s+ " "));
    }
}
