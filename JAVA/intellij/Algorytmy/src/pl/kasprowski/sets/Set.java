package pl.kasprowski.sets;

import java.util.Arrays;

public class Set extends Element {
    private Element[] elements;

    public Set() {
        super.type = ElementType.SET;
    }

    public void addElement(Element element) {
        if (elements == null || elements.length == 0) {
            this.elements = new Element[1];
            this.elements[0] = element;
        } else {
            if (!this.contains(element)) {
                int size = elements.length;
                this.elements = Arrays.copyOf(elements, size + 1);
                this.elements[size] = element;
            }
        }
    }

    public void subtractElement(Element element) {
        if (this.contains(element)) {
            Element[] tmpArray = new Element[elements.length];
            int i = 0;
            for (Element el : elements) {
                if (!el.equals(element))
                    tmpArray[i++] = el;
            }
            this.elements = Arrays.copyOf(tmpArray, i);
        }
    }

    public void add(Set setToBeAdded) {
        Arrays.stream(setToBeAdded.elements).forEach(this::addElement);
    }

    public void subtract(Set setToBeSubtraced) {
        Arrays.stream(setToBeSubtraced.elements).forEach(this::subtractElement);
    }

    public void print() {
        System.out.println(Arrays.toString(this.elements));
    }

    public Set intersection(Set setToBeIntersected) {
        Set resultSet = new Set();
        Element[] tmpArray = new Element[this.elements.length];
        int i = 0;
        for (Element element : this.elements) {
            if (setToBeIntersected.contains(element)) {
                tmpArray[i++] = element;
            }
        }
        resultSet.elements = Arrays.copyOf(tmpArray, i);
        return resultSet;
    }

    public boolean isSubSet(Set subSet) {
        boolean isSubSet = true;
        for (Element element : subSet.elements) {
            if (!this.contains(element)) {
                isSubSet = false;
            }
        }
        return isSubSet;
    }

    private boolean contains(Element element) {
        return Arrays.stream(this.elements).anyMatch(e -> e.equals(element));
    }

    @Override
    protected boolean equals(Element element) {
        if (this.type == element.type && this.isSubSet((Set) element) && ((Set) element).isSubSet(this)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        Arrays.stream(elements).forEach(s->sb.append(s));
        return "{" + sb.toString() + "}";
    }
}