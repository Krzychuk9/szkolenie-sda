package pl.kasprowski.sets;

public class IntegerElement extends Element {
    private Integer value;

    public IntegerElement(Integer value) {
        super.type = ElementType.INTEGER;
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "{" + value + "}";
    }

    @Override
    protected boolean equals(Element element) {
        if (this.type == element.type && this.value.equals(((IntegerElement) element).getValue())) {
            return true;
        } else {
            return false;
        }
    }


}
