package pl.kasprowski.sets;

public abstract class Element {
    protected ElementType type;

    protected abstract boolean equals(Element element);

}
