package pl.kasprowski.sets;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        Set set1 = new Set();
        Set set2 = new Set();
        Set set3 = new Set();
        Set set4 = new Set();

        set4.addElement(new IntegerElement(4));
        set4.addElement(new IntegerElement(10));

        set1.addElement(new IntegerElement(1));
        set1.addElement(new IntegerElement(1));
        set1.addElement(new IntegerElement(2));
        set1.addElement(set4);
        set1.addElement(new IntegerElement(10));
        set2.addElement(new IntegerElement(5));
        set2.addElement(new IntegerElement(5));
        set2.addElement(new IntegerElement(1));
        set2.addElement(new IntegerElement(4));
        set2.addElement(set4);

        set1.add(set2);
        set3 = set1.intersection(set2);
        set1.print();
        set2.print();
        set3.print();
        System.out.println(set1.isSubSet(set3));
    }
}
