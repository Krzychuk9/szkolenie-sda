package pl.kasprowski.gene;

import java.util.Arrays;

public class Specimen {
    private static final Double MUTATION_PROPABILITY = 0.01;
    private boolean[] chromosome;
    private int totalSize;
    private int totalValue;
    private GenePool genePool;

    public Specimen(boolean[] chromosome, GenePool genePool) {
        this.chromosome = chromosome;
        this.genePool = genePool;
        updateTotals();
    }

    public Specimen(Specimen candidate, GenePool genePool) {
        this(Arrays.copyOf(candidate.chromosome, candidate.chromosome.length), genePool);

    }

    private void updateTotals() {
        totalSize = 0;
        totalValue = 0;
        for (int i = 0; i < chromosome.length; i++) {
            if (chromosome[i]) {
                Item item = genePool.getItemForGene(i);
                totalSize += item.getSize();
                totalValue += item.getValue();
            }
        }
    }

    public int getTotalSize() {
        return totalSize;
    }

    public int getTotalValue() {
        return totalValue;
    }

    public static Specimen getChild(Specimen parent1, Specimen parent2, GenePool genePool) {
        boolean[] childChromosome = new boolean[parent1.chromosome.length];
        for (int i = 0; i < childChromosome.length; i += 2) {
            childChromosome[i] = parent1.chromosome[i];
        }
        for (int i = 1; i < childChromosome.length; i += 2) {
            childChromosome[i] = parent2.chromosome[i];
        }
        return new Specimen(childChromosome, genePool);
    }

    @Override
    public String toString() {
        return "Total value: " + totalValue + ", total size: " + totalSize;
    }

    public void mutate() {
        boolean mutated = false;
        for (int i = 0; i < chromosome.length; i++) {
            if (Math.random() < MUTATION_PROPABILITY) {
                chromosome[i] = !chromosome[i];
                mutated = true;
            }
        }
        if (mutated) {
            updateTotals();
        }
    }
}
