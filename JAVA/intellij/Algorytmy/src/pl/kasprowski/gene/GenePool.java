package pl.kasprowski.gene;

import java.util.Random;

public class GenePool {
    private Item[] pool;

    public GenePool(Item[] pool) {
        this.pool = pool;
    }

    public GenePool(int poolSize, int maxItemSize, int maxItemValue) {
        pool = new Item[poolSize];
        Random random = new Random();
        for (int i = 0; i < poolSize; i++) {
            int itemSize = random.nextInt(maxItemSize - 1) + 1;
            int itemValue = random.nextInt(maxItemValue);
            pool[i] = new Item(itemSize, itemValue);
        }
    }

    public boolean[] getRandomChromosome() {
        boolean[] result = new boolean[pool.length];
        Random random = new Random();
        for (int i = 0; i < result.length; i++) {
            result[i] = random.nextBoolean();
        }
        return result;
    }

    public Item getItemForGene(int geneIndex) {
        return this.pool[geneIndex];
    }
}
