package pl.kasprowski.gene;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Optional;
import java.util.Random;

public class Population {
    private Specimen[] population;
    private GenePool genePool;
    private int backpackSize;
    private Specimen bestSpecimen;

    public Population(int size, GenePool genePool, int backpackSize) {
        this.backpackSize = backpackSize;
        this.genePool = genePool;
        this.population = new Specimen[size];
        this.randomizePopulation();
    }

    public void mutate() {
        Arrays.stream(population).forEach(Specimen::mutate);
    }

    public Integer getPopulationValue() {
        return Arrays.stream(this.population).mapToInt(p -> p.getTotalValue()).sum();
    }

    public void nextGeneration() {
        Specimen[] bestSpecimen = this.getBestSpecimens();
        if (bestSpecimen.length == 0) {
            this.randomizePopulation();
        } else {
            Specimen[] nextGeneration = new Specimen[population.length];

            int bestSpecimensInNextGen = 0;

            for (; bestSpecimensInNextGen < population.length / 4 && bestSpecimensInNextGen < bestSpecimen.length; bestSpecimensInNextGen++) {
                nextGeneration[bestSpecimensInNextGen] = bestSpecimen[bestSpecimensInNextGen];
            }

            Random random = new Random();
            for (int i = bestSpecimensInNextGen; i < nextGeneration.length; i++) {
                int parent1Index = random.nextInt(bestSpecimen.length);
                int parent2Index = random.nextInt(bestSpecimen.length);
                Specimen parent1 = bestSpecimen[parent1Index];
                Specimen parent2 = bestSpecimen[parent2Index];
                nextGeneration[i] = Specimen.getChild(parent1, parent2, genePool);
            }
            population = nextGeneration;
        }
    }


    private Specimen[] getBestSpecimens() {
        Specimen[] filtredSpecimen = Arrays.stream(population).filter(p -> p.getTotalSize() <= backpackSize * 1.1).toArray(size -> new Specimen[size]);
        return Arrays.stream(filtredSpecimen).sorted((s1, s2) -> Double.compare(s2.getTotalValue(), s1.getTotalValue())).limit(population.length / 2).toArray(size -> new Specimen[size]);
    }

    private void randomizePopulation() {
        for (int i = 0; i < population.length; i++) {
            this.population[i] = new Specimen(genePool.getRandomChromosome(), genePool);
        }
    }

    public Specimen findSolution(int nextGenerationCount) {
        for (int i = 0; i < nextGenerationCount; i++) {
            nextGeneration();
            mutate();
            updateBestSpecimen();
        }
        return bestSpecimen;
    }

    public void updateBestSpecimen() {
        Optional<Specimen> ObestSpecimenCandidate = Arrays.stream(population).filter(p -> p.getTotalSize() < backpackSize).max(Comparator.comparingInt(Specimen::getTotalValue));
        if (ObestSpecimenCandidate.isPresent()) {
            Specimen bestSpecimenCandidate = ObestSpecimenCandidate.get();
            if (bestSpecimen == null || bestSpecimenCandidate.getTotalValue() > bestSpecimen.getTotalValue()) {
                bestSpecimen = new Specimen(bestSpecimenCandidate, genePool);
            }
        }

    }
}
