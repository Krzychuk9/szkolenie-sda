package pl.kasprowski.gene;

public class Main {
    public static void main(String[] args) {
        Item[] pool = new Item[10];
        pool[0] = new Item(1, 5);
        pool[1] = new Item(4, 15);
        pool[2] = new Item(7, 5);
        pool[3] = new Item(2, 1);
        pool[4] = new Item(10, 10);
        pool[5] = new Item(5, 5);
        pool[6] = new Item(6, 2);
        pool[7] = new Item(1, 5);
        pool[8] = new Item(6, 9);
        pool[9] = new Item(7, 7);

        GenePool genePool = new GenePool(pool);
        Population population = new Population(100, genePool, 25);
        System.out.println("Najlepszy ziomek: " + population.findSolution(10000));
    }
}
