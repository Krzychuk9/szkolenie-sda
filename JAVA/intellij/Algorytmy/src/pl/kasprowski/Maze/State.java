package pl.kasprowski.Maze;

public class State {
    private Point point;
    private Point origin;
    private boolean visited;
    private double scoreFnValue;

    public State(Point point, Point origin, boolean visited, double scoreFnValue) {
        this.point = point;
        this.origin = origin;
        this.visited = visited;
        this.scoreFnValue = scoreFnValue;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public Point getPoint() {
        return point;
    }

    public Point getOrigin() {
        return origin;
    }

    public boolean isVisited() {
        return visited;
    }

    public double getScoreFnValue() {
        return scoreFnValue;
    }
}
