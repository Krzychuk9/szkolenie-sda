package pl.kasprowski.Maze;

public class Main {
    public static void main(String[] args) {
        Maze maze = new Maze("maze.txt");
        MazeSolver mazeSolver = new MazeSolver(maze);
        mazeSolver.solve();
    }
}
