package pl.kasprowski.Maze;

import java.lang.reflect.Array;
import java.util.*;

public class MazeSolver {
    private Map<Point, State> stateByPoint;
    private Maze maze;
    private Point endPoint;

    public MazeSolver(Maze maze) {
        this.stateByPoint = new HashMap<>();
        this.maze = maze;
        this.endPoint = maze.getEndPont();
    }

    public List<Point> solve() {
        Point startPoint = maze.getStartPont();
        State startState = new State(startPoint, null, true, 0);
        stateByPoint.put(startPoint, startState);
        Point point = startPoint;
        while (true) {
            takeALookAround(point);
            State nextState = getBestUnvisitedPoint();
            if (nextState.getScoreFnValue() == 0) {
                break;
            }
            nextState.setVisited(true);
            point = nextState.getPoint();
            System.out.println(point);
        }
        return getRoute();
    }

    private State getBestUnvisitedPoint() {
        return stateByPoint.values().stream().filter(s -> !s.isVisited()).min(Comparator.comparingDouble(State::getScoreFnValue)).get();
    }

    private void takeALookAround(Point point) {
        Point leftPoint = new Point(point.getX() - 1, point.getY());
        Point rightPoint = new Point(point.getX() + 1, point.getY());
        Point overPoint = new Point(point.getX(), point.getY() - 1);
        Point underPoint = new Point(point.getX(), point.getY() + 1);
        List<Point> surrounding = Arrays.asList(leftPoint, rightPoint, overPoint, underPoint);
        surrounding.stream().forEach(p -> {
            Field filed = maze.getFieldAt(p);
            if (filed != null && filed != Field.WALL && !stateByPoint.containsKey(p)) {
                stateByPoint.put(p, new State(p, point, false, getScoreFnValue(p)));
            }
        });
    }

    private double getScoreFnValue(Point p) {
        return Math.abs(endPoint.getX() - p.getX()) + Math.abs(endPoint.getY() - p.getY());
    }

    public List<Point> getRoute() {
        State state = stateByPoint.get(endPoint);
        List<Point> result = new ArrayList<>();
        while (state.getOrigin() != null) {
            result.add(state.getPoint());
            state = this.stateByPoint.get(state.getOrigin());
        }
        Collections.reverse(result);
        return result;
    }
}
