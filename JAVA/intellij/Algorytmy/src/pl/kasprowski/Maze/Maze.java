package pl.kasprowski.Maze;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class Maze {
    private Field[][] fields;

    public Maze(String fileName) {
        try {
            List<String> lines = Files.lines(Paths.get(fileName)).collect(Collectors.toList());
            fields = new Field[lines.size()][lines.get(0).length()];
            for (int i = 0; i < lines.size(); i++) {
                fillFieldsRow(lines.get(i), i);
            }
        } catch (IOException e) {
            throw new RuntimeException("File read exception");
        }
    }

    private void fillFieldsRow(String line, int rowIndex) {
        for (int i = 0; i < line.length(); i++) {
            fields[rowIndex][i] = getFiledFromChar(line.charAt(i));
        }
    }

    private Field getFiledFromChar(char c) {
        switch (c) {
            case ' ':
                return Field.ROAD;
            case '-':
                return Field.WALL;
            case '+':
                return Field.WALL;
            case '|':
                return Field.WALL;
            case 'S':
                return Field.START;
            case 'E':
                return Field.END;
            default:
                return Field.WALL;
        }
    }

    public Field getFieldAt(Point point) {
        int x = point.getX();
        int y = point.getY();
        if (x < 0 || y < 0 || y >= fields.length || x >= fields[0].length) {
            return null;
        }
        return fields[y][x];
    }

    public Point getEndPont() {
        for (int i = 0; i < fields.length; i++) {
            for (int j = 0; j < fields[0].length; j++) {
                if (fields[i][j] == Field.END) {
                    return new Point(j, i);
                }
            }
        }
        throw new RuntimeException("Brak końca labiryntu");
    }

    public Point getStartPont() {
        for (int i = 0; i < fields.length; i++) {
            for (int j = 0; j < fields[0].length; j++) {
                if (fields[i][j] == Field.START) {
                    return new Point(j, i);
                }
            }
        }
        throw new RuntimeException("Brak startu labiryntu");
    }
}
