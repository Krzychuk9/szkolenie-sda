package pl.sda.listy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class Main {

    private static Collection<String> countries;

    public static void main(String[] args) {

        fillCountries();
        List<String> list = shortCountries((List) countries);

        for (String country : list) {
            System.out.println(country);
        }
        System.out.println(list.size());

    }

    public static List<String> shortCountries(List<String> list) {
        List<String> newList = new ArrayList<>();
        Iterator<String> it = list.iterator();
        while (it.hasNext()) {
            String next = it.next();
            if (next.length() < 6) {
                newList.add(next);
            }
        }
        return newList;
    }

    public static int deleteACountries(List<String> list) {
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            String next = iterator.next();
            if (next.endsWith("a")) {
                iterator.remove();
            }
        }
        return list.size();
    }

    public static int countriesEndsWithA(Collection<String> list) {
        int result = 0;
        for (String s : list) {
            if (s.endsWith("a")) {
                result++;
            }
        }
        return result;
    }

    private static void fillCountries() {
        countries = new ArrayList<>();
        countries.add("San Escobar");
        countries.add("Korea Południowa");
        countries.add("Korea Północna");
        countries.add("Kosowo");
        countries.add("Kostaryka");
        countries.add("Kuba");
        countries.add("Kuwejt");
        countries.add("Laos");
        countries.add("Lesotho");
        countries.add("Liban");
        countries.add("Liberia");
        countries.add("Libia");
        countries.add("Liechtenstein");
        countries.add("Litwa");
        countries.add("Luksemburg");
        countries.add("Łotwa");
        countries.add("Macedonia");
        countries.add("Madagaskar");
        countries.add("Malawi");
        countries.add("Malediwy");
        countries.add("Malezja");
        countries.add("Mali");
        countries.add("Malta");
        countries.add("Maroko");
        countries.add("Bośnia I Hercegowina");
        countries.add("Botswana");
        countries.add("Brazylia");
        countries.add("Brunei");
        countries.add("Bułgaria");
        countries.add("Burkina Faso");
        countries.add("Burundi");
        countries.add("Chile");
        countries.add("Chińska Republika Ludowa");
        countries.add("Chorwacja");
        countries.add("Cypr");
        countries.add("Cypr Północny");
        countries.add("Czad");
        countries.add("Czarnogóra");
        countries.add("Dania");
        countries.add("Demokratyczna Republika Konga");
        countries.add("Dominika");
        countries.add("Dominikana");
        countries.add("Dżibuti");
        countries.add("Egipt");
        countries.add("Ekwador");
        countries.add("Erytrea");
        countries.add("Estonia");
        countries.add("Etiopia");
        countries.add("Fidżi");
        countries.add("Filipiny");
        countries.add("Finlandia");
        countries.add("Francja");
        countries.add("Gabon");
        countries.add("Gambia");
        countries.add("Ghana");
        countries.add("Górski Karabach");
        countries.add("Grecja");
        countries.add("Grenada");
        countries.add("Gruzja");
        countries.add("Gujana");
        countries.add("Gwatemala");
        countries.add("Gwinea");
        countries.add("Abchazja");
        countries.add("Afganistan");
        countries.add("Albania");
        countries.add("Algieria");
        countries.add("Andora");
        countries.add("Angola");
        countries.add("Antigua I Barbuda");
        countries.add("Arabia Saudyjska");
        countries.add("Argentyna");
        countries.add("Armenia");
        countries.add("Australia");
        countries.add("Austria");
        countries.add("Azerbejdżan");
        countries.add("Bahamy");
        countries.add("Bahrajn");
        countries.add("Bangladesz");
        countries.add("Barbados");
        countries.add("Belgia");
        countries.add("Belize");
        countries.add("Benin");
        countries.add("Bhutan");
        countries.add("Białoruś");
        countries.add("Birma");
        countries.add("Boliwia");
        countries.add("Gwinea Bissau");
        countries.add("Gwinea Równikowa");
        countries.add("Haiti");
        countries.add("Hiszpania");
        countries.add("Holandia");
        countries.add("Somaliland");
        countries.add("Sri Lanka");
        countries.add("Stany Zjednoczone");
        countries.add("Suazi");
        countries.add("Sudan");
        countries.add("Surinam");
        countries.add("Syria");
        countries.add("Szwajcaria");
        countries.add("Szwecja");
        countries.add("Tadżykistan");
        countries.add("Tajlandia");
        countries.add("Tajwan");
        countries.add("Tanzania");
        countries.add("Timor Wschodni");
        countries.add("Togo");
        countries.add("Tonga");
        countries.add("Trynidad I Tobago");
        countries.add("Tunezja");
        countries.add("Turcja");
        countries.add("Turkmenistan");
        countries.add("Tuvalu");
        countries.add("Uganda");
        countries.add("Ukraina");
        countries.add("Urugwaj");
        countries.add("Honduras");
        countries.add("Indie");
        countries.add("Indonezja");
        countries.add("Irak");
        countries.add("Iran");
        countries.add("Irlandia");
        countries.add("Islandia");
        countries.add("Izrael");
        countries.add("Jamajka");
        countries.add("Japonia");
        countries.add("Jemen");
        countries.add("Jordania");
        countries.add("Kambodża");
        countries.add("Kamerun");
        countries.add("Kanada");
        countries.add("Katar");
        countries.add("Kazachstan");
        countries.add("Kenia");
        countries.add("Kirgistan");
        countries.add("Kiribati");
        countries.add("Kolumbia");
        countries.add("Komory");
        countries.add("Kongo");
        countries.add("Mauretania");
        countries.add("Mauritius");
        countries.add("Meksyk");
        countries.add("Mikronezja");
        countries.add("Mołdawia");
        countries.add("Monako");
        countries.add("Mongolia");
        countries.add("Mozambik");
        countries.add("Naddniestrze");
        countries.add("Namibia");
        countries.add("Nauru");
        countries.add("Nepal");
        countries.add("Niemcy");
        countries.add("Niger");
        countries.add("Nigeria");
        countries.add("Nikaragua");
        countries.add("Norwegia");
        countries.add("Nowa Zelandia");
        countries.add("Oman");
        countries.add("Osetia Południowa");
        countries.add("Pakistan");
        countries.add("Palau");
        countries.add("Palestyna");
        countries.add("Panama");
        countries.add("Papua Nowa Gwinea");
        countries.add("Paragwaj");
        countries.add("Peru");
        countries.add("Polska");
        countries.add("Portugalia");
        countries.add("Republika Czeska");
        countries.add("Republika Południowej Afryki");
        countries.add("Republika Środkowoafrykańska");
        countries.add("Republika Zielonego Przylądka");
        countries.add("Rosja");
        countries.add("Wietnam");
        countries.add("Włochy");
        countries.add("Wybrzeże Kości Słoniowej");
        countries.add("Wyspy Marshalla");
        countries.add("Wyspy Salomona");
        countries.add("Wyspy Świętego Tomasza I Książęca");
        countries.add("Zambia");
        countries.add("Zimbabwe");
        countries.add("Zjednoczone Emiraty Arabskie");
        countries.add("Rumunia");
        countries.add("Rwanda");
        countries.add("Sahara Zachodnia");
        countries.add("Saint Kitts I Nevis");
        countries.add("Saint Lucia");
        countries.add("Saint Vincent I Grenadyny");
        countries.add("Salwador");
        countries.add("Samoa");
        countries.add("San Marino");
        countries.add("Senegal");
        countries.add("Serbia");
        countries.add("Seszele");
        countries.add("Sierra Leone");
        countries.add("Singapur");
        countries.add("Słowacja");
        countries.add("Słowenia");
        countries.add("Somalia");
        countries.add("Uzbekistan");
        countries.add("Vanuatu");
        countries.add("Watykan");
        countries.add("Wenezuela");
        countries.add("Węgry");
        countries.add("Wielka Brytania");

    }
}
