package pl.sda.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        File f = new File("C:/Users/RENT/IdeaProjects/SDA_Collection_Interface/test.txt");

        try(BufferedWriter bw = new BufferedWriter(new FileWriter(f))){
            bw.write("Coś do pliku");
            bw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

//        if(f.isDirectory()){
//            for (String s : f.list()) {
//                System.out.println(s);
//            }
//        }

//        try {
//            f.createNewFile();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        System.out.println(f.getAbsolutePath());

    }
}
