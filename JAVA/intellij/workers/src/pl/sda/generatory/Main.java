package pl.sda.generatory;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        Generator gen = new Generator();
        List<Integer> list = gen.nextN(5);
        for (Integer element : list) {
            System.out.println(element);
        }


        GeneratorDiv3 genDiv3 = new GeneratorDiv3();
        for (int i = 0; i < 100; i++) {
            System.out.println(genDiv3.next());
        }

        GeneratorPrimeNumbers gpn = new GeneratorPrimeNumbers();
        for(int i = 0; i<5; i++)
        System.out.println(gpn.next());
        GeneratorPrimeNumbers gpn2 = new GeneratorPrimeNumbers();
        for(int i = 0; i<5; i++)
            System.out.println(gpn2.next2());

        FiboGenerator fg = new FiboGenerator();
        for (int i = 0; i <15 ; i++) {
            System.out.println(fg.next());
        }
        SixGenerator sg = new SixGenerator();
        while (sg.hasNext()){
            System.out.println("Wyniki: " + sg.next());
        }
    }
}

