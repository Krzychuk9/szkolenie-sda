package pl.sda.generatory;

public class GeneratorPrimeNumbers {
    private int number = 2;

    public int next() {
        boolean prime = true;
        if (number > 2) {
            if (number % 2 == 0) {
                prime = false;
            }
            int g = (int) Math.sqrt(number);
            for (int i = 3; i <= g; i += 2) {
                if (number % i == 0) {
                    prime = false;
                }
            }
        }
        if (prime) {
            return number++;
        } else {
            number++;
        }
        return this.next();
    }

    public int next2() {
        boolean prime = true;
        if (number > 2) {
            if (number % 2 == 0) {
                prime = false;
            }
            for (int i = 3; i < number; i += 2) {
                if (number % i == 0) {
                    prime = false;
                }
            }
        }
        if (prime) {
            return number++;
        } else {
            number++;
        }
        return this.next2();
    }

    public int next3() {
        boolean isPrime = false;
        do {
            int counter = 0;
            for (int i = 2; i < number; i++) {
                if (number % i == 0) {
                    counter++;
                }
                if (counter == 0) {
                    isPrime = true;
                } else {
                    number++;
                }
            }
        } while (!isPrime);
        int result = number;
        number++;
        return result;
    }

}
