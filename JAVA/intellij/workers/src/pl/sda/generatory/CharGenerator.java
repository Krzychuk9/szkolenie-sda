package pl.sda.generatory;

public class CharGenerator {
    private char letter = 'A';

    public char next() {
        char result = letter;
        letter++;
        return result;
    }

    public boolean hasNext() {
        char end = letter;
        letter--;
        return end != 'Z';
    }


}
