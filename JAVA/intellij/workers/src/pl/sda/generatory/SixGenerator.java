package pl.sda.generatory;

import java.util.Random;

public class SixGenerator {
    Random r = new Random();
    private int number;

    public int next() {
        return number = (r.nextInt(6) + 1);
    }

    public boolean hasNext() {
        return number != 6;
    }
}
