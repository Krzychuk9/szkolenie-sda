package pl.sda.generatory;

public class FiboGenerator {
    private int fibo1 = 1;
    private int fibo2 = 0;
    private int fibo = 0;
    private int couter = 0;

    public int next() {
        if (couter == 0) {
            couter++;
            return 0;
        } else if (couter == 1) {
            couter++;
            return 1;
        } else {
            fibo = fibo1 + fibo2;
            fibo2 = fibo1;
            fibo1 = fibo;
            return fibo;
        }
    }

}
