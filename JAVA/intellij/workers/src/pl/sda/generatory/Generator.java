package pl.sda.generatory;


import java.util.ArrayList;
import java.util.List;

public class Generator {
    private int counter = 1;

    public Generator() {
    }

    public int next() {
        int result = counter++;
        return result * result;
    }
    public List<Integer> nextN(int n){
        List<Integer> list = new ArrayList<>();
        for(int i = 0; i<n; i++){
            list.add(this.next());
        }
        return list;
    }

}
