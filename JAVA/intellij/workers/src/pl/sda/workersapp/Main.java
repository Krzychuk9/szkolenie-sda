package pl.sda.workersapp;

import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = null;
        try {
            System.out.println("Podaj ścieżke do pliku: ");
            sc = new Scanner(System.in);
            String fileIn = sc.nextLine();
            WorkersReader workersReader = new WorkersReader(fileIn);
            Employe[] employes = workersReader.employesReader();
            //BubbleSort bsort = new BubbleSort();
            // bsort.bubbleEmpSalary(employes);
            MathForEmploye analyzer = new MathForEmploye(employes);
            analyzer.analyze();
            System.out.println("Najwyższa pensja: " + analyzer.getMaxSalary());
            System.out.println("Najniższa pensja: " + analyzer.getMinSalary());
            System.out.println("Sredni wiek pracownikow: " + analyzer.getAvgAge());
            System.out.println("Srednia pensja: " + analyzer.getAvgSalary());
            for (Employe employe : employes) {
                System.out.println(employe.getName() + "\t" + employe.getAge() + "\t" + employe.getSalary());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (sc != null) {
                sc.close();
            }
        }
    }
}
