package pl.sda.workersapp;

public class QuickSort {

    /**
     * Sortowanie przy użyciu algorytmu quicksort
     *
     * @param workers Tablica do posortowania
     * @param start   Indeks pierwszego elementu sortowanej tablicy
     * @param end     Indeks ostatniego elementu sortowanej tablicy
     */
    public void quicksort(Employe workers[], int start, int end) {

        int i, j;
        double v;
        Employe tmp;

        i = start;
        j = end;
        v = workers[(start + end) / 2].getSalary(); //TODO: określamy punkt podziału tablicy
        do {
            //jeśli element i jest mniejszy od punktu podziału, to przesuwamy indeks i w prawą stronę
            while (workers[i].getSalary() < v)
                i++;

            //jeśli element j jest większy od punktu podziału, to przesuwamy indeks j w lewą stronę
            while (v < workers[j].getSalary())
                j--;

            //jeśli po lewej stronie punktu podziału jest element większy od punktu podziały
            //a po prawej stronie mniejszy od punktu podziału, to zamieniamy te elementy
            if (i <= j) {
                tmp = workers[i];
                workers[i] = workers[j];
                workers[j] = tmp;
                i++;
                j--;
            }
        }
        while (i <= j); //pętlę wykonujemy do czasu aż i "minie się" z j

        if (start < j)
            quicksort(workers, start, j);//TODO: rekurencyjnie wywołujemy quicksort dla pierwszej części tablicy
        if (i < end)
            quicksort(workers, i, end);//TODO: rekurencyjnie wywołujemy quicksort dla drugiej częsci tablicy
    }
}
