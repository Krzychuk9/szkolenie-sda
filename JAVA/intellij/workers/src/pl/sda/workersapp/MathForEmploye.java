package pl.sda.workersapp;


public class MathForEmploye {
    private double avgSalary;
    private double avgAge;
    private double minSalary;
    private double maxSalary;
    private Employe[] workers;

    public MathForEmploye(Employe[] workers) {
        this.workers = workers;
    }

    /**
     * do crazy things with workers! and set fileds
     */
    public void analyze() {
//        BubbleSort bSort = new BubbleSort();
//        bSort.bubbleEmpSalary(workers);
        QuickSort qSort = new QuickSort();
        qSort.quicksort(workers, 0, workers.length-1);
        minSalary = workers[0].getSalary();
        maxSalary = workers[workers.length - 1].getSalary();
        double sum = 0;
        int age = 0;
        for (int i = 0; i < workers.length; i++) {
            sum += workers[i].getSalary();
            age += workers[i].getAge();
        }
        avgSalary = sum / workers.length;
        avgAge = age / workers.length;

    }

    public double getAvgSalary() {
        return avgSalary;
    }

    public double getAvgAge() {
        return avgAge;
    }

    public double getMinSalary() {
        return minSalary;
    }

    public double getMaxSalary() {
        return maxSalary;
    }


}
