package pl.sda.workersapp;

public class BubbleSort {
//    public void bubbleSort(int[] array) {
//        int n = array.length;
//        int tmp;
//        do {
//            for (int i = 0; i < n - 1; i++) {
//                if (array[i] > array[i + 1]) {
//                    tmp = array[i];
//                    array[i] = array[i + 1];
//                    array[i + 1] = tmp;
//                }
//            }
//            n = n - 1;
//        } while (n > 1);
//    }

    public void bubbleEmpSalary(Employe[] workers) {
        int n = workers.length;
        Employe tmp = null;
        do {
            for (int i = 0; i < n - 1; i++) {
                if (workers[i].getSalary() > workers[i + 1].getSalary()) {
                    tmp = workers[i];
                    workers[i] = workers[i + 1];
                    workers[i + 1] = tmp;
                }
            }
            n = n - 1;
        } while (n > 1);
    }

}
