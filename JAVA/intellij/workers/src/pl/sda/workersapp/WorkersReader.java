package pl.sda.workersapp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class WorkersReader {
    private String fileIn;

    public WorkersReader(String fileIn) {
        this.fileIn = fileIn;
    }

    /**
     * Read employee data from array
     *
     * @return employe array
     * @throws IOException
     */
    public Employe[] employesReader() throws IOException {
        Employe[] emp = null;
        int arraySize = 0;
        try (BufferedReader br = new BufferedReader(new FileReader(new File(fileIn)))) {
            if (br.readLine() != null) {
                String line;
                String[] data;
                while ((line = br.readLine()) != null) {
                    data = line.split("\\t+");
                    Employe worker = new Employe(data[0], Integer.parseInt(data[1]), Double.parseDouble(data[2]));
                    if (emp == null) {
                        emp = new Employe[1];
                        emp[0] = worker;
                        arraySize = 1;
                    } else {
                        arraySize++;
                        emp = Arrays.copyOf(emp, arraySize);
                        emp[arraySize - 1] = worker;
                    }
                }
            }

        }
        return emp;
    }

}
