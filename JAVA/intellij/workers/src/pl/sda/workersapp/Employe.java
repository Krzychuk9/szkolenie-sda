package pl.sda.workersapp;

public class Employe {
    private String name;
    private int age;
    private double salary;

    /**
     * Constructor
     *
     * @param name
     * @param age
     * @param salary
     */

    public Employe(String name, int age, double salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}
