package pl.kasprowski0901;

public class Kolo extends Figura implements Obracalne {
    private double radius;

    public Kolo(double radius) {
        this.radius = radius;
    }

    @Override
    public Double pole() throws ZeroException, MniejszaOdZeraException{
        if(this.radius < 0){
            throw new MniejszaOdZeraException("Promien mniejszy od zera!");
        } else if (this.radius == 0){
            throw new ZeroException("Promień równy zero!");
        }
        double result = this.radius * this.radius * Math.PI;
        return result;
    }

    @Override
    public void obroc() {
        System.out.println("Obracam o 90 stopni");
    }
}
