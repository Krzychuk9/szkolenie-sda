package pl.kasprowski0901;

public class DziwnaFigura extends Figura {

    /**
     * Liczy objetosc dziwnej figury
     *
     * @param h wysokosc dziwnej figury
     * @param w pierwsza długość boku podstawy
     * @param d druga długość boku podstawy
     * @return double objetosc
     * @throws ObjetoscException       jeżeli wysokość jest mniejsza lub równa zero
     * @throws MniejszaOdZeraException jeżeli któryś z boków podstawy jest mniejszy od zera
     */
    public double obliczObjetosc(double h, double w, double d) throws MniejszaOdZeraException, ObjetoscException {
        if (w <= 0 || d <= 0) {
            throw new MniejszaOdZeraException("Blędy wymiar boku podstawy");
        } else if (h <= 0) {
            throw new ObjetoscException("Figura płaska");
        }
        return h * d * w;
    }

    public double obliczObwod(double a, double b) {
        double result = 0;
        try {
            if (a <= 0 || b <= 0) {
                throw new Exception("Blędny wymiar");
            } else {
                result =  2 * a + 2 * b;
                return result;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }finally {
            System.out.println("finally");
        }
        return result;
    }

    @Override
    public Double pole() throws ZeroException, MniejszaOdZeraException {
        return -1.0;
    }
}
