package pl.kasprowski0901;

public class Prostokat extends Figura {
    protected double bok1;
    protected double bok2;

    public Prostokat(double bok1, double bok2) {
        this.bok1 = bok1;
        this.bok2 = bok2;
    }

    @Override
    public Double pole() throws MniejszaOdZeraException, ZeroException {
        if(this.bok1 > 0  && this.bok2 > 0){
            return this.bok1 * this.bok2;
        }else if (this.bok1 == 0 || this.bok2 == 0){
            throw new ZeroException("Bok równy zero!");
        }else{
            throw new MniejszaOdZeraException("Bok mniejszy od zera!");
        }

    }
}
