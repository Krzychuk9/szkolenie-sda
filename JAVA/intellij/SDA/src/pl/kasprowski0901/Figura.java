package pl.kasprowski0901;

public abstract class Figura {
    /**
     * Liczy pole
     * @return double pole
     * @throws ZeroException jeżeli bok równy zero
     * @throws MniejszaOdZeraException jeżeli bok jest mniejszy od zera
     */
    public abstract Double pole() throws ZeroException, MniejszaOdZeraException;
}
