package pl.kasprowski;

public class User {
    private String name;
    private String surName;
    private int age;

    private enum Sex {
        FAMALE, Male
    }

    private Sex sex;

    public User(String name) {
        this.name = name;
    }

    public User(String name, String surName, int age, Sex sex) {
        this.name = name;
        this.surName = surName;
        this.age = age;
        this.sex = sex;
    }
}
