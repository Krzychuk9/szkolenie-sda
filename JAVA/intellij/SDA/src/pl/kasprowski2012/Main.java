package pl.kasprowski2012;

public class Main {

    public static void main(String[] args) {

        StringTest st = new StringTest();
        System.out.println(st.getSth());

        FirstString fs = new FirstString();
        System.out.println(fs.getSth());

        SecondString ss = new SecondString("Hello from second class");
        System.out.println(ss.getSth());

        StaticVsDyn obj1 = new StaticVsDyn(1);
        StaticVsDyn obj2 = new StaticVsDyn(2);

        // obj1.ID = 9999999; cant change final !!!
        System.out.println(StaticVsDyn.ID);
        obj1.dynId = 1;
        obj2.dynId = 2;
        System.out.println(obj1.dynId);
        System.out.println(obj2.dynId);

    }
}
