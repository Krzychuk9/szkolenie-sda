package pl.sda;

import java.io.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        try {
            Scanner sc = new Scanner(System.in);
            boolean correct = false;
            String fileIn;
            String fileOut;
            String text;
            do {
                System.out.println("Podaj plik wejściowy: ");
                fileIn = sc.nextLine();
                if (fileIn.equals("")) {
                    System.out.println("Nie podano ścieżki do pliku!");
                } else {
                    correct = true;
                }
            } while (!correct);
            do {
                correct = false;
                System.out.println("Podaj plik wyjściowy: ");
                fileOut = sc.nextLine();
                if (fileOut.equals("")) {
                    System.out.println("Nie podano ścieżki do pliku!");
                } else {
                    correct = true;
                }
            } while (!correct);
            do {
                correct = false;
                System.out.println("Podaj dopisywany tekst: ");
                text = sc.nextLine();
                if (text.equals("")) {
                    System.out.println("Brak tekstu!");
                } else {
                    correct = true;
                }
            } while (!correct);

            //opyFile(fileIn, fileOut, text);
            //copyFileBufferd(fileIn, fileOut);
            System.out.println(returnIntArray(fileIn).length);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void copyFile(String fileIn, String fileOut, String text) throws IOException {
        FileReader reader = null;
        FileWriter writer = null;
        char[] tmp = text.toCharArray();
        try {
            reader = new FileReader(fileIn);
            writer = new FileWriter(fileOut);
            int c;
            while ((c = reader.read()) != -1) {
                writer.write(c);
            }
            for (int i = 0; i < tmp.length; i++) {
                writer.write(tmp[i]);
            }
        } finally {
            if (reader != null) {
                reader.close();
            }
            if (writer != null) {
                writer.close();
            }

        }

    }

    public static void copyFileBufferd(String fileIn, String fileOut) throws IOException {
        BufferedReader in = null;
        PrintWriter out = null;
        try {
            in = new BufferedReader(new FileReader(fileIn));
            out = new PrintWriter(new FileWriter(fileOut));
            String l;
            while ((l = in.readLine()) != null) {
                out.println(l.toUpperCase());
                out.println(l.length());
            }
        } finally {
            if (in != null)
                in.close();
            if (out != null)
                out.close();
        }
    }

    public static int[] returnIntArray(String fileIn) throws IOException{
        File f = new File(fileIn);
        Scanner sc = null;
        int[] intArray  = null;
        try{
            sc = new Scanner(f);
            if (sc.hasNext()){

                intArray = new int[sc.nextInt()];
            }else{
                return null;
            }

            for(int i = 0; i<intArray.length; i++){
                if(sc.hasNextInt()){
                    intArray[i] = sc.nextInt();
                }
            }
        }finally{
            if(sc != null){
                sc.close();
            }
        }
        return intArray;
    }
}
