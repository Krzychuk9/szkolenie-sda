package MavenProject.pl.sda;

import org.apache.commons.lang3.text.WordUtils;

public class WordValidator {
    public String getInitials(String txt) {
        return WordUtils.initials(txt);
    }

    public boolean containsWords(String word, String[] words) {
        return WordUtils.containsAllWords(word, words);
    }

}
