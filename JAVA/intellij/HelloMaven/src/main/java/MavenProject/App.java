package MavenProject;

import MavenProject.pl.sda.NumberValidator;
import MavenProject.pl.sda.StringValidator;
import MavenProject.pl.sda.Validator;
import MavenProject.pl.sda.WordValidator;

public class App {
    public static void main(String[] args) {
        Validator validator = new Validator();
        System.out.println(
                validator.validateURL("http://commons.apache.org/proper/commons-validator/apidocs/index.html"));
        System.out.println(validator.validateISBM("978-83-7432-357-4"));
        System.out.println(validator.validateEmail("kasprowski.krzysztof@gmail.com"));

        WordValidator word = new WordValidator();
        String[] words = {"hello", "world", "maven"};
        System.out.println(word.getInitials("Hello World Hello Maven"));
        System.out.println(word.containsWords("hello world hello maven hello spring", words));

        NumberValidator number = new NumberValidator();
        int[] numbers = {1, 2, 3, 4, 5, 6, 7};

        System.out.println(number.getMax(numbers));
        System.out.println(number.getMin(numbers));

        StringValidator stringVal = new StringValidator();
        System.out.println(stringVal.getEquals("Hello", "Helo"));
    }
}
