package pl.sda;

import pl.sda.model.Car;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import java.util.List;

/**
 * Managed bean to klasa kontrolera, którą będzie można wywołać w widoku
 * w tym przypadku poprzez #{applicationController}
 * <p>
 * Stan tej klasy bedzie dostępny poprzez cały czas istnienia aplikacji
 */
@ManagedBean(name = "applicationController")
@ApplicationScoped
public class ApplicationController {
    @ManagedProperty(value = "#{carService}")
    private CarService carService;

    public CarService getCarService() {
        return carService;
    }

    public void setCarService(CarService carService) {
        this.carService = carService;
    }

    public List<Car> getCars() {
        return carService.getAllCars();
    }
}
