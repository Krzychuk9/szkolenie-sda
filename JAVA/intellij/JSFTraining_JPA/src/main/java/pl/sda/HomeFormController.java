package pl.sda;

import pl.sda.model.Car;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import java.util.List;

@ManagedBean(name = "form")
@RequestScoped
public class HomeFormController {
    private String name;
    private String regPlate;
    private String vin;
    private Integer totalMass;

    @ManagedProperty(value = "#{applicationController}")
    private ApplicationController applicationController;

    @ManagedProperty(value = "#{carService}")
    private CarService carService;

    public CarService getCarService() {
        return carService;
    }

    public void setCarService(CarService carService) {
        this.carService = carService;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegPlate() {
        return regPlate;
    }

    public void setRegPlate(String regPlate) {
        this.regPlate = regPlate;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public Integer getTotalMass() {
        return totalMass;
    }

    public void setTotalMass(Integer totalMass) {
        this.totalMass = totalMass;
    }

    public void save() {
        Car car = new Car(name, regPlate, vin, totalMass);
        List<Car> cars = carService.getAllCars();
        cars.add(car);
        carService.saveList(cars);
    }

    public ApplicationController getApplicationController() {
        return applicationController;
    }

    public void setApplicationController(ApplicationController applicationController) {
        this.applicationController = applicationController;
    }
}
