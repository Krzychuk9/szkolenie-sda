package pl.sda;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class HibernateUtils {
    private static final EntityManagerFactory entityManagerFactory;

    static {
        entityManagerFactory = Persistence.createEntityManagerFactory("JSFCrudPU");
    }

    private HibernateUtils() {
    }

    public static EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }
}
