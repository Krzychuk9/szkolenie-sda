package pl.sda.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by RENT on 2017-04-01.
 */
@Entity
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String regPlate;
    private String vin;
    private Integer totalMass;

    public Car() {
    }

    public Car(String name, String regPlate, String vin, Integer totalMass) {
        this.name = name;
        this.regPlate = regPlate;
        this.vin = vin;
        this.totalMass = totalMass;
    }

    public String getName() {
        return name;
    }

    public String getRegPlate() {
        return regPlate;
    }

    public String getVin() {
        return vin;
    }

    public Integer getTotalMass() {
        return totalMass;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRegPlate(String regPlate) {
        this.regPlate = regPlate;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public void setTotalMass(Integer totalMass) {
        this.totalMass = totalMass;
    }
}
