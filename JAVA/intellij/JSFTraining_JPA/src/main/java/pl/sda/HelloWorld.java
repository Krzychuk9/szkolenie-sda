package pl.sda;

import javax.faces.bean.ManagedBean;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Date;

/**
 * Managed bean to klasa kontrolera, którą będzie można wywołać w widoku
 * w tym przypadku poprzez #{helloWorld}
 */
@ManagedBean(name = "helloWorld", eager = true)
public class HelloWorld {
    EntityManagerFactory emf;
    EntityManager em;
    private String data;

    public HelloWorld() {
        emf = Persistence.createEntityManagerFactory("JSFCrudPU");
        em = emf.createEntityManager();
        System.out.println("HelloWorld started!");
    }

    /**
     * to ta metoda odpowiada za wyświetlenie "Hello World" na stronie
     * @return
     */
    public String getMessage() {
        return "Hello World!";
    }

    /**
     * Zwraca czas wywołania
     * @return
     */
    public String getTime(){
        return new Date().toString();
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
