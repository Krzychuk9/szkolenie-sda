package pl.sda;

import org.hibernate.HibernateException;
import pl.sda.model.Car;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import java.util.List;

@ManagedBean(name = "carService")
@ApplicationScoped
public class CarService {
    EntityManagerFactory factory;
    EntityManager entityManager;

    public CarService() {
        this.factory = HibernateUtils.getEntityManagerFactory();
        this.entityManager = factory.createEntityManager();
    }

    public List<Car> getAllCars() {
        List<Car> cars = entityManager.createQuery("from Car").getResultList();
        return cars;
    }

    public void save(Car car) {
        EntityTransaction transaction = null;
        try {
            transaction = entityManager.getTransaction();
            transaction.begin();
            entityManager.persist(car);
            transaction.commit();
        } catch (HibernateException ex) {
            ex.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }

    public void saveList(List<Car> cars) {
        EntityTransaction transaction = null;
        try {
            transaction = entityManager.getTransaction();
            transaction.begin();
            cars.forEach(c -> entityManager.persist(c));
            transaction.commit();
        } catch (HibernateException ex) {
            ex.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }
}
