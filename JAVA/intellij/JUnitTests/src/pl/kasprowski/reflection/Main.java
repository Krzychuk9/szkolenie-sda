package pl.kasprowski.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class Main {

    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException, NoSuchFieldException {
        ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();
        Class<?> aClass = systemClassLoader.loadClass("pl.kasprowski.reflection.User");
        Constructor<?> constructor = aClass.getConstructor(String.class);
        Object instance = constructor.newInstance("elo");
        //aClass.newInstance();


        for (Field field : aClass.getDeclaredFields()) {
            System.out.println(field.getName());
            field.set(instance,"ziomek");
            System.out.println(field.get(instance));
        }

    }
}
