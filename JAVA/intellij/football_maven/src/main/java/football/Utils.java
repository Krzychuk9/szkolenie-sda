package football;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class Utils {

    public static List<String> splitToTrimmedList(String line, String seperator) {
        return Arrays.asList(line.split(seperator)).stream().map(String::trim).collect(Collectors.toList());
    }

    public static Date getDataFromText(String text, String errorMessageText) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = sdf.parse(text);
        } catch (ParseException e) {
            throw new RuntimeException(errorMessageText);
        }
        return date;
    }

    public static Integer getIntegerFromString(String text, String errorMessageText) {
        Integer i;
        try {
            i = Integer.parseInt(text);
        } catch (NumberFormatException e) {
            throw new RuntimeException(errorMessageText);
        }
        return i;
    }

    public static List<String> splitToTrimmedList(String line, String seperator, int expectedSize, String errorMessageText) {
        List<String> list = Utils.splitToTrimmedList(line, seperator);
        if (list.size() != expectedSize) {
            throw new RuntimeException(errorMessageText);
        }
        return list;
    }

    public static Integer getIntegerFromString(String text, String messageText, boolean nullable) {
        if (nullable && text != null && text.equals("null")) {
            return null;
        } else {
            return Utils.getIntegerFromString(text, messageText);
        }
    }

    public boolean isNull(Object obj) {
        return obj == null;
    }
}
