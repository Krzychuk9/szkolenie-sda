package football.controllers;

import football.entities.Player;
import football.repositories.DB.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/player")
public class PlayerController {
    @Autowired
    private PlayerRepository playerRepository;

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Player get(@PathVariable("id") int id) {
        return playerRepository.get(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public void add(@RequestBody Player player) {
        playerRepository.add(player);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void delete(@RequestBody int id) {
        playerRepository.delete(id);
    }
}
