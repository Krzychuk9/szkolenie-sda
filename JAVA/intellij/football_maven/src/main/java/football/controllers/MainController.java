package football.controllers;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

    @RequestMapping("/abc/{id}")
    public String abc(@PathVariable("id") String id) {
        return id;
    }

    @RequestMapping("/abc")
    public String getMyString() {
        return "abc";
    }

}
