package football.controllers;

import football.entities.Team;
import football.repositories.DB.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/team")
public class TeamController {
    @Autowired
    private TeamRepository teamRepository;



    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Team get(@PathVariable("id") int id) {
        return teamRepository.get(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public void add(@RequestBody Team team) {
        teamRepository.add(team);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void delete(@RequestBody int id) {
        teamRepository.delete(id);
    }

}
