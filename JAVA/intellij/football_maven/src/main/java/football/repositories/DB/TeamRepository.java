package football.repositories.DB;

import football.entities.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Repository
public class TeamRepository extends AbstractRepository<Team> {

    public TeamRepository() {
        super();
    }

    @Override
    protected String getAddSQL() {
        return "{CALL create_team(?,?,?,?)}";
    }

    @Override
    protected String getDeleteSQL() {
        return "{CALL delete_team(?)}";
    }

    @Override
    protected String getGetSQL() {
        return "SELECT * FROM druzyna WHERE id_druzyna = ?";
    }

    @Override
    public void add(Team element) {
        try {
            addCallableStatement.setString(1, element.getName());
            addCallableStatement.setInt(2, Integer.parseInt(new SimpleDateFormat("yyyy").format(element.getFoundationDate())));
            addCallableStatement.setInt(3, 1);
            addCallableStatement.registerOutParameter(4, Types.INTEGER);
            addCallableStatement.executeQuery();
            element.setId(addCallableStatement.getInt(4));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Team get(int id) {
        Team team = null;
        try {
            getPreparedStatement.setInt(1, id);
            ResultSet resultSet = getPreparedStatement.executeQuery();
            if (resultSet.next()) {
                String name = resultSet.getString("nazwa");
                Date year = new SimpleDateFormat("yyyy").parse(String.valueOf(resultSet.getInt("rok_zalozenia")));
                team = new Team(id, name, year);
            } else {
                throw new RuntimeException("Brak druzyny o wskazanym id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return team;
    }
}
