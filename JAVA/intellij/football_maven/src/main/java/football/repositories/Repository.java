package football.repositories;

public interface Repository<T> {
    void add(T element);

    void delete(int id);

    T get(int id);

}
