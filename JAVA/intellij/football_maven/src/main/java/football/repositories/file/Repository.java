package football.repositories.file;

import football.entities.AbstractEntity;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class Repository<T extends AbstractEntity> implements football.repositories.Repository<T> {
    protected List<T> list;


    public Repository() {
        Stream<String> fileLineStream = null;
        try {
            fileLineStream = Files.lines(Paths.get(this.getFileName()));
            this.list = fileLineStream.map(t -> this.getObjectFromFile(t)).collect(Collectors.toList());
            this.list.stream().forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileLineStream != null) {
                fileLineStream.close();
            }
        }
    }

    protected abstract void upDateObj(T e1, Optional<T> e2);

    protected abstract String getFileName();

    protected abstract T getObjectFromFile(String line);

    protected abstract String getFileLineFromObject(T element);

    protected abstract T getNewObject(T obj);

    public void upData(T element) {
        Optional<T> optional = this.list.stream().filter(pl -> pl.getId() == element.getId()).findFirst();
        if (optional.isPresent()) {
            upDateObj(element, optional);
        } else {
            throw new RuntimeException("Brak obiektu o wskazanym id");
        }
        this.saveObjectToFile();
    }

    protected void saveObjectToFile() {
        try {
            Files.write(Paths.get(this.getFileName()), this.list.stream().map(e -> this.getFileLineFromObject(e)).collect(Collectors.toList()), Charset.forName("UTF-8"), StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException("Błąd zapisu");
        }
    }

    public T get(int id) {
        Optional<T> optionalObject = this.list.stream().filter(e -> e.getId() == id).findFirst();
        if (optionalObject.isPresent()) {
            return this.getNewObject(optionalObject.get());
        }
        return null;
    }

    public void add(T obj) {
        if (this.list.stream().map(e -> e.getId()).anyMatch(i -> i == obj.getId())) {
            throw new RuntimeException("Obiekt o danym ID istnieje!");
        }
        this.list.add(obj);
        this.saveObjectToFile();
    }

    public void add(Collection<T> elements) {
        list.stream().forEach(e -> this.add(e));
    }

    public void delete(int id) {
        if (!this.list.stream().map(e -> e.getId()).anyMatch(i -> i == id)) {
            throw new RuntimeException("Brak obiektu o wskazanym id");
        }
        this.list.remove(this.list.stream().filter(e -> e.getId() == id).findFirst().get());
        this.saveObjectToFile();
    }

    public void delete(Collection<Integer> ints) {
        ints.stream().forEach(i -> this.delete(i));
    }


    public boolean exist(int id) {
        return this.list.stream().anyMatch(e -> e.getId() == id);
    }

    public List<T> getAll() {
        return this.list;
    }
}

