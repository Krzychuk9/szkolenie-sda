package football.repositories.file;

import football.Utils;
import football.entities.Team;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class TeamRepository extends Repository<Team> {
    private static final String FILE_NAME = "team.csv";

    public TeamRepository() {
        super();
    }

    @Override
    protected void upDateObj(Team team, Optional<Team> optional) {
        Team uTeam = optional.get();
        uTeam.setName(team.getName());
        uTeam.setFoundationDate(team.getFoundationDate());
    }

    @Override
    protected String getFileName() {
        return FILE_NAME;
    }

    @Override
    protected Team getNewObject(Team obj) {
        return new Team(obj);
    }


    @Override
    protected Team getObjectFromFile(String line) {
        List<String> words = Utils.splitToTrimmedList(line, ",", 3, "Nie prawidłowe dane");
        Integer id = Utils.getIntegerFromString(words.get(0), "Błędne ID");
        String name = words.get(1);
        Date date = Utils.getDataFromText(words.get(2), "Nie prawidłowy format daty");
        return new Team(id, name, date);
    }

    @Override
    protected String getFileLineFromObject(Team team) {
        return team.getId() + "," + team.getName() + "," + new SimpleDateFormat("yyyy-MM-dd").format(team.getFoundationDate());
    }
}