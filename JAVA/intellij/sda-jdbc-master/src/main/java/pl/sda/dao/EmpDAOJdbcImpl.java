package pl.sda.dao;

import pl.sda.domain.Employee;

import javax.naming.ldap.PagedResultsControl;
import java.math.BigDecimal;
import java.sql.*;
import java.util.*;
import java.sql.Date;


public class EmpDAOJdbcImpl implements EmpDAO {

    private final JdbcConnectionManager jdbcConnectionManager;

    public EmpDAOJdbcImpl(JdbcConnectionManager jdbcConnectionManager) {
        this.jdbcConnectionManager = jdbcConnectionManager;
    }

    @Override
    public Employee findById(int id) throws Exception {
        Connection connection = jdbcConnectionManager.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM emp WHERE emp.empno = ?");
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet.next()) {
            int dbID = resultSet.getInt(1);
            String name = resultSet.getString(2);
            String job = resultSet.getString(3);
            Integer manager = resultSet.getInt(4);
            java.util.Date heredate = resultSet.getDate(5);
            BigDecimal salary = resultSet.getBigDecimal(6);
            BigDecimal commision = resultSet.getBigDecimal(7);
            int deptno = resultSet.getInt(8);
            connection.close();
            return new Employee(dbID, name, job, manager, heredate, salary, commision, deptno);
        }
        return null;
    }

    @Override
    public void create(Employee employee) throws Exception {
        Connection connection = null;
        try {
            connection = jdbcConnectionManager.getConnection();
            connection.setAutoCommit(false);
            Statement statement = connection.createStatement();
            int empno = employee.getEmpno();
            String name = employee.getEname();
            String job = employee.getJob();
            Integer manager = employee.getManager();
            Date heredate = new Date(employee.getHiredate().getTime());
            BigDecimal salary = employee.getSalary();
            BigDecimal commision = employee.getCommision();
            int deptno = employee.getDeptno();
            String values = "(" + empno + ", '" + name + "', '" + job + "', " + manager + ", '" + heredate + "', " + salary + ", " + commision + ", " + deptno + ")";
            String sql = "INSERT INTO emp(empno,ename,job,manager,hiredate,salary,commision,deptno) VALUES" + values;
            statement.executeUpdate(sql);
            connection.commit();
        } catch (SQLException e) {
            throw new Exception();
        } finally {
            connection.close();
        }

    }

    @Override
    public void update(Employee employee) throws Exception {
        Connection connection = jdbcConnectionManager.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement("UPDATE emp SET ename=?, job = ?,manager=?,hiredate=?,salary=?, commision=?,deptno=? WHERE empno = ?");
        preparedStatement.setString(1, employee.getEname());
        preparedStatement.setString(2, employee.getJob());
        preparedStatement.setInt(3, employee.getManager());
        preparedStatement.setDate(4, new Date(employee.getHiredate().getTime()));
        preparedStatement.setBigDecimal(5, employee.getSalary());
        preparedStatement.setBigDecimal(6, employee.getCommision());
        preparedStatement.setInt(7, employee.getDeptno());
        preparedStatement.setInt(8, employee.getEmpno());
        preparedStatement.executeUpdate();
        connection.close();
    }

    @Override
    public void delete(int id) throws Exception {
        Connection connection = jdbcConnectionManager.getConnection();
        PreparedStatement ps = connection.prepareStatement("DELETE FROM sda.emp WHERE sda.emp.empno =?");
        ps.setInt(1, id);
        ps.executeUpdate();
        connection.close();
    }

    @Override
    public void create(List<Employee> employees) throws Exception {
        Connection connection = null;
        connection = jdbcConnectionManager.getConnection();
        connection.setAutoCommit(false);
        Statement statement = connection.createStatement();
        for (Employee employee : employees) {
//            this.create(employee);
            int empno = employee.getEmpno();
            String name = employee.getEname();
            String job = employee.getJob();
            Integer manager = employee.getManager();
            Date heredate = new Date(employee.getHiredate().getTime());
            BigDecimal salary = employee.getSalary();
            BigDecimal commision = employee.getCommision();
            int deptno = employee.getDeptno();
            String values = "(" + empno + ", '" + name + "', '" + job + "', " + manager + ", '" + heredate + "', " + salary + ", " + commision + ", " + deptno + ")";
            String sql = "INSERT INTO emp(empno,ename,job,manager,hiredate,salary,commision,deptno) VALUES" + values;
            statement.addBatch(sql);
        }
        int[] ints = statement.executeBatch();
        System.out.println(Arrays.stream(ints).filter(p -> p < 3).count());
        connection.commit();
        connection.close();
    }

    @Override
    public BigDecimal getTotalSalaryByDept(int dept) throws Exception {
        Connection connection = jdbcConnectionManager.getConnection(); //SELECT sda.emp.salary FROM sda.emp WHERE empno = ?
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT SUM(sda.emp.salary) FROM sda.emp GROUP BY deptno HAVING deptno = ?");
        preparedStatement.setInt(1, dept);
        ResultSet resultSet = preparedStatement.executeQuery();
        BigDecimal bigDecimal = null;
        if (resultSet.next()) {
            bigDecimal = resultSet.getBigDecimal(1);
        }
        return bigDecimal;
    }
}
