package sda.pawel;

import java.sql.*;

public class CreateConnection {

    public static void main(String[] args) throws SQLException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("Class not found");
            return;
        }

        Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/world?useSSL=false", "root", "");
            connection.setAutoCommit(false);

            Statement statement = connection.createStatement();
            int i = statement.executeUpdate("INSERT INTO world.city(Name, CountryCode, District, Population) VALUE ('Mikolow', 'POL', 'Poland',500)");
            System.out.println(i);
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            connection.rollback();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
