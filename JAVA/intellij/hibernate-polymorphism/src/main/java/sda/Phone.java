package sda;

import javax.persistence.*;

@Entity
@Table
@PrimaryKeyJoinColumn(name="phone_id")
public class Phone extends ElectricalProduct {
    private String screenSize;
    private String storage;
    @Column(name = "screen_size")
    public String getScreenSize() {
        return screenSize;
    }

    public void setScreenSize(String screenSize) {
        this.screenSize = screenSize;
    }
    @Column(name = "storage")
    public String getStorage() {
        return storage;
    }

    public void setStorage(String storage) {
        this.storage = storage;
    }


}
