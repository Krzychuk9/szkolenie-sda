package sda;

import javax.persistence.*;

@Entity
@Table(name="CHAIR")
@AttributeOverrides({@AttributeOverride(name="id",column = @Column(name = "id")),@AttributeOverride(name="name",column = @Column(name = "name")),@AttributeOverride(name="description",column = @Column(name = "description"))})
public class Chair extends FurnitureProduct {
	private String material;

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}
	
	

}
