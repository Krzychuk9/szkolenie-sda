package sda;

import static org.testng.Assert.assertEquals;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import sda.CampingStove;
import sda.Chair;
import sda.Phone;
import sda.Product;
import sda.RingProduct;
import sda.Tent;

public class HibernatePolymorphismTest 
{
	private BasicDataSource dataSource;
	private SessionFactory sessionFactory;
	
	public BasicDataSource getDataSource() {
		return dataSource;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@BeforeMethod
	public void setUp() throws SQLException {
        dataSource = new BasicDataSource();
        dataSource.setDriverClassName("org.hsqldb.jdbcDriver");
        dataSource.setUrl("jdbc:hsqldb:mem:HibernateDB");
        dataSource.setUsername("sa");
        dataSource.setDefaultAutoCommit(false);
        dataSource.setPassword("");
        dataSource.setAccessToUnderlyingConnectionAllowed(true);
		sessionFactory = new Configuration().configure().buildSessionFactory();
	}
	
	@AfterMethod
	public void tearDown() throws SQLException {
		dataSource.close();
		dataSource = null;
		sessionFactory.close();
		sessionFactory = null;
	}
	
	@Test
	public void basicSave() throws SQLException {
		Product product = new Product();
    	product.setName("Test product");

    	Session session = getSessionFactory().openSession();
    	Transaction tx = session.beginTransaction();
    	session.save(product);
    	System.out.println("Saved id: " + product.getId());
    	
    	tx.commit();
    	session.close();
    	
    	Connection connection = getDataSource().getConnection();
    	Statement statement = connection.createStatement();
    	ResultSet resultSet = statement.executeQuery("select count(*) from Product");
    	resultSet.next();
    	assertEquals(resultSet.getLong(1),1);
    	
    	statement = connection.createStatement();
    	resultSet = statement.executeQuery("select id,name from Product");
    	while (resultSet.next()) {
    		System.out.println("Id: " + resultSet.getLong("id") + " Name: " + resultSet.getString("name"));
    	}
    	connection.close();
	}
	
	@Test
	public void implicitPolymorphism() {
		System.out.println("Testing implicit polymorphism");
		RingProduct ringProduct = new RingProduct();
		ringProduct.setName("Diamond ring");
		ringProduct.setStoneSize("0.5ct");
		ringProduct.setStoneType("Diamond");
		
		Session session = getSessionFactory().openSession();
    	Transaction tx = session.beginTransaction();
    	session.save(ringProduct);
    	System.out.println("Saved id: " + ringProduct.getId());

    	List<Product> products = session.createQuery("from Product").list();
    	assertEquals(products.size(),1);
    	System.out.println("Found " + products.size() + " products.");
    	RingProduct product = (RingProduct) products.iterator().next();
    	System.out.println("Got product with name: " + product.getName() + " stone size: " + product.getStoneSize() + " and stone type: " + product.getStoneType());
    	
    	tx.commit();
    	session.close();
	}
	
	@Test
	public void tablePerConcreteSubclass() {
		System.out.println("Testing table per concrete subclass");
		Chair chair = new Chair();
		chair.setName("Cross back oak dining chair");
		chair.setDescription("A traditional dining chair with carved feet");
		chair.setMaterial("Velvet");
		
		Session session = getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		session.save(chair);
		
		List furnitureProducts = session.createQuery("from FurnitureProduct").list();
		assertEquals(furnitureProducts.size(),1);
		
		tx.commit();
		session.close();
	}
	
	@Test
	public void tablePerClass() throws SQLException {
		System.out.println("Testing table per class");
		Phone phone = new Phone();
		phone.setName("Samsung S4");
		phone.setDescription("Latest incarnation of Samsung's top of the range smartphone.");
		phone.setScreenSize("4.2 inches");
		phone.setStorage("16Gb");
		
		Session session = getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		session.save(phone);
		
		List electricalProducts = session.createQuery("from ElectricalProduct").list();
		assertEquals(electricalProducts.size(),1);
		
		tx.commit();
		session.close();
	
	 	Connection connection = getDataSource().getConnection();
    	Statement statement = connection.createStatement();
    	ResultSet resultSet = statement.executeQuery("select count(*) from ElectricalProduct");
    	resultSet.next();

    	assertEquals(resultSet.getLong(1),1);
    	resultSet = statement.executeQuery("select count(*) from phone");
    	resultSet.next();
    	assertEquals(resultSet.getLong(1),1);
    	connection.close();
	}
	
	@Test
	public void tablePerClassHierarchy() throws SQLException {
		CampingStove campingStove = new CampingStove();
		campingStove.setName("Firefly");
		campingStove.setDescription("Lightweight stove suitable for backpackers and wilderness campers");
		campingStove.setFuelType("White spirit");
		
		Tent tent = new Tent();
		tent.setName("Mistral");
		tent.setDescription("Lightweight three pole geodesic tent");
		tent.setWeight(3.1);
		tent.setCapacity(2);
		
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.save(campingStove);
		session.save(tent);
		
		List campingProducts = session.createQuery("from CampingProduct").list();
		assertEquals(campingProducts.size(),2);
		
		tx.commit();
		session.close();

	 	Connection connection = getDataSource().getConnection();
    	Statement statement = connection.createStatement();
    	ResultSet resultSet = statement.executeQuery("select count(*) from CampingProduct");
    	resultSet.next();
    	assertEquals(resultSet.getLong(1),2);
    	
    	resultSet = statement.executeQuery("select type,name from campingproduct");
    	while (resultSet.next()) {
    		System.out.println("Product type: " + resultSet.getString("type") + " Name: " + resultSet.getString("name"));
    	}
    	connection.close();
		
	}
}
