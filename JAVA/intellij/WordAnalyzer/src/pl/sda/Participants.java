package pl.sda;

import java.io.*;
import java.util.*;

public class Participants {
    private Set<Person> participants = null;

    public Participants() {
        this.setParticipants();
    }

    private void setParticipants() {
        participants = new HashSet<>();
        participants.add(new Person("Michał"));
        participants.add(new Person("Michał G."));
        participants.add(new Person("Bartek"));
        participants.add(new Person("Kamil"));
        participants.add(new Person("Lukasz"));
        participants.add(new Person("Marek"));
        participants.add(new Person("Szymon"));
        participants.add(new Person("Tymoteusz"));
        participants.add(new Person("Krzysztof"));
        participants.add(new Person("Dawid"));
        participants.add(new Person("Lukasz S"));
        participants.add(new Person("Maciej"));
        participants.add(new Person("Maria"));
        participants.add(new Person("Kamila"));
        participants.add(new Person("Mirosław"));
    }

    public List<Set<Person>> getCouple() {
        List<Set<Person>> couple = new ArrayList<>();
        List<Person> randomOrderList = new ArrayList<>();
        randomOrderList.addAll(participants);
        Collections.shuffle(randomOrderList);

        for (int i = 0; i < randomOrderList.size(); i += 2) {
            Set<Person> singleCouple = new HashSet<>();
            try {
                singleCouple.add(randomOrderList.get(i));
                singleCouple.add(randomOrderList.get(i + 1));
            }catch (IndexOutOfBoundsException e){
                singleCouple.add(randomOrderList.get(i));
                singleCouple.add(new Person("Forever alone"));
            }
            couple.add(singleCouple);
        }
        return couple;
    }

    public void saveToHTML(String fileIn) throws IOException {
        try (PrintWriter bw = new PrintWriter(new File(fileIn))) {
            List<Set<Person>> coupleList = this.getCouple();

            bw.println("<!DOCTYPE html>");
            bw.println("<html>");
            bw.println("<head>");
            bw.println("<meta charset='UTF-8'>");
            bw.println("<title>Kurs SDA</title>");
            bw.println("<style>\ntable, th, td {border: 1px solid black; padding: 5px; text-align: center;}");
            bw.println("thead {background-color: #50394c; color: #fff;}");
            bw.println("tbody {background-color: #f4e1d2;}");
            bw.println("</style>");
            bw.println("</head>");
            bw.println("<body>");
            bw.println("<table style='border-collapse: collapse;'>");
            bw.println("<thead>");
            bw.println("<tr>");
            bw.println("<th>Uczestnik nr 1</th><th>Uczestnik nr 2</th>");
            bw.println("</tr>");
            bw.println("</thead>");
            bw.println("<tbody>");
            for (Set<Person> people : coupleList) {
                StringBuilder sb = new StringBuilder();
                sb.append("<tr>");
                for (Person person : people) {
                    sb.append("<td>" + person.toString() + "</td>");
                }
                sb.append("</tr>");
                bw.println(sb.toString());
            }
            bw.println("</tbody>");
            bw.println("</table>");
            bw.println("</body>");
        }
    }

    public void print(List<Set<Person>> coupleList) {
        for (Set<Person> people : coupleList) {
            for (Person person : people) {
                System.out.print(person + " ");
            }
            System.out.println();
        }
    }
}
