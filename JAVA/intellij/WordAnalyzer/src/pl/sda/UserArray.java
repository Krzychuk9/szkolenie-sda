package pl.sda;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

public class UserArray {
    private Scanner sc = null;

    public void getUserInput() {
        int[] array = new int[1];
        int input;
        int counter = 0;
        try {
            sc = new Scanner(System.in);
            System.out.println("Hello! Podaj ciąg liczb całkowitych! 0 kończy zabawę :)");
            do {
                try{
                    input = sc.nextInt();
                }catch (InputMismatchException e){
                    System.out.println("To nie jest liczba całkowita!");
                    sc.nextLine();
                    continue;
                }

                if (input == 0) {
                    break;
                }
                array[counter++] = input;
                array = Arrays.copyOf(array, array.length + 1);
            } while (true);
        } finally {
            sc.close();
        }
        array = Arrays.copyOfRange(array, 0, counter);
        this.getMax(array);
        this.getMin(array);
        this.getAvrg(array);
    }

    private void getMax(int[] array) {
        System.out.print("Maksymalna podana wartość to: ");
        Arrays.stream(array).max().ifPresent(System.out::println);
    }

    private void getMin(int[] array) {
        System.out.print("Minimalna podana wartość to: ");
        Arrays.stream(array).min().ifPresent(System.out::println);
    }

    private void getAvrg(int[] array) {
        int avrg = (int) (Arrays.stream(array).sum() / Arrays.stream(array).count());
        System.out.println("Srednia z podanych wartość to: " + avrg);
    }

}
