package pl.sda;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        Participants participants = new Participants();
        try {
            participants.saveToHTML("index.html");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
