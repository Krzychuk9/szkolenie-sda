package pl.sda;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.function.Predicate;

public class WordCounter {

    public int countWords(String fileIn) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(new File(fileIn)))) {
            String line;
            char[] charLine;
            int counter = 0;
            while ((line = br.readLine()) != null) {
                charLine = line.toCharArray();
                for (int i = 0; i < charLine.length; i++) {
                    if (!Character.isWhitespace(charLine[i])) {
                        counter++;
                        do {
                            if (i == charLine.length - 1) {
                                break;
                            }
                            i++;
                        } while (!Character.isWhitespace(charLine[i]));
                    }
                }
            }
            return counter;
        }
    }

    public int count(String textToFind, String text){
        int counter = 0;
        String[] array = text.split("\\s+");
        for (String s : array) {
            if(s.equals(textToFind)){
                counter++;
            }
        }
        return counter;
    }

    public int count2(String textToFind, String text){
        return (int)Arrays.stream(text.split("\\s+")).filter(s -> s.equals(textToFind)).count();
    }


    public int count3(String textToFind, String path) throws IOException{
        int counter = 0;
        try(FileReader fr = new FileReader(new File(path))){
            int c;
            StringBuffer sb = new StringBuffer();
            while((c = fr.read()) != -1) {
                sb.append((char)c);
            }
            counter = (int)Arrays.stream(sb.toString().split("\\s+")).filter(s -> s.equals(textToFind)).count();
        }
        return counter;
    }
}


