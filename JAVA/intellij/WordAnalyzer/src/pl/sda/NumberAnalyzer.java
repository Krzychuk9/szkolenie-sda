package pl.sda;

import java.util.Arrays;

public class NumberAnalyzer {

    /**
     * Return numbers order in array
     *
     * @param array array of ints
     * @return reversed array of ints
     */
    public int[] reverseArray(int[] array) {
        int[] result = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = array[array.length - 1 - i];
        }
        return result;
    }

    /**
     * Removes duplicates from array of ints
     *
     * @param array array of ints
     * @return array of ints without duplicates
     */
    public int[] deleteDuplicates(int[] array) {
        int[] result = new int[1];
        boolean exist = false;
        boolean isZero = false;
        int counter = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < result.length; j++) {
                if (array[i] == result[j]) {
                    exist = true;
                    break;
                }
            }
            if (!isZero && array[i] == 0) {
                exist = false;
                isZero = true;
            }
            if (!exist) {
                result[counter++] = array[i];
                result = Arrays.copyOf(result, result.length + 1);
            }
            exist = false;
        }
        result = Arrays.copyOfRange(result, 0, counter);
        return result;
    }

}
