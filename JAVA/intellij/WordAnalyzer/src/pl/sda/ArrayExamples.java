package pl.sda;

public class ArrayExamples {

    public void randomNumber() {
        int[] array = new int[100];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) ((Math.random() * 10) + 1);
        }
        int[] tmp = new int[10];

        for (int i : array) {
            tmp[i - 1] = ++tmp[i - 1];
        }
        for (int i = 0; i < tmp.length; i++) {
            System.out.println((i + 1) + "- " + tmp[i]);
        }
    }

    public int[][] getMatrix() {
        int[][] matrix = new int[5][5];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = (int) ((Math.random() * 11));
            }
        }
        return matrix;
    }
    public void printMatrix(int[][] matrix){
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    public void maxInRow(int[][] matrix) {
        int[] max = new int[5];
        int[] min = new int[5];
        for (int i = 0; i < matrix.length; i++) {
            int maxRow = matrix[i][0];
            int minRow = matrix[i][0];
            for (int j = 1; j < matrix[i].length; j++) {
                if (matrix[i][j] > maxRow) {
                    maxRow = matrix[i][j];
                }
                if (matrix[i][j] < minRow) {
                    minRow = matrix[i][j];
                }
            }
            max[i] = maxRow;
            min[i] = minRow;
        }
        for (int i = 0; i < max.length; i++) {
            System.out.println("Rząd nr " + (i + 1) + ", Maks: " + max[i] + ", Min: " + min[i]);
        }
    }

    public void maxInCol(int[][] matrix) {
        int[] max = new int[5];
        int[] min = new int[5];
        for (int i = 0; i < matrix.length; i++) {
            int maxCol = matrix[0][i];
            int minCol = matrix[0][i];
            for (int j = 1; j < matrix.length; j++) {
                if (matrix[j][i] > maxCol) {
                    maxCol = matrix[j][i];
                }
                if (matrix[j][i] < minCol) {
                    minCol = matrix[j][i];
                }
            }
            max[i] = maxCol;
            min[i] = minCol;
        }
        for (int i = 0; i < max.length; i++) {
            System.out.println("Kolumna nr " + (i + 1) + ", Maks: " + max[i] + ", Min: " + min[i]);
        }
    }

    public void maxInDiagonal(int[][] matrix) {
        int[] max = new int[2];
        int[] min = new int[2];
        max[0] = matrix[0][0];
        max[1] = matrix[0][4];
        min[0] = matrix[0][0];
        min[1] = matrix[0][4];

        for (int i = 1, j = 1; i < matrix.length && j < matrix.length; i++, j++) {
            if (max[0] < matrix[i][j]) {
                max[0] = matrix[i][j];
            }
            if (min[0] > matrix[i][j]) {
                min[0] = matrix[i][j];
            }
        }
        for(int i = 1, j = 3; j > -1 && i < matrix.length; i++, j--){
            if (max[1] < matrix[i][j]) {
                max[1] = matrix[i][j];
            }
            if (min[1] > matrix[i][j]) {
                min[1] = matrix[i][j];
            }
        }
        System.out.println("Przękątna nr 1, Maks: " + max[0] + ", Min: " + min[0]);
        System.out.println("Przękątna nr 2, Maks: " + max[1] + ", Min: " + min[1]);
    }

    public void getSumOfMatrix(){
        int[][] A = this.getMatrix();
        int[][] B = this.getMatrix();
        int[][] C = new int[5][5];
        System.out.println("Macierz A:");
        this.printMatrix(A);
        System.out.println("Macierz B:");
        this.printMatrix(B);
        for (int i = 0; i < C.length; i++) {
            for (int j = 0; j < C[i].length ; j++) {
                C[i][j] = A[i][j] + B[i][j];
            }
        }
        System.out.println("Suma macierzy A i B: ");
        this.printMatrix(C);
    }
}
