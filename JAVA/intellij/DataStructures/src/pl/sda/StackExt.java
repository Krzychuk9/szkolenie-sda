package pl.sda;

public class StackExt extends Stack {
    private final int maxSize;

    public StackExt(int maxSize) {
        this.maxSize = maxSize;
    }

    public boolean isEmpty() {
        return (super.getStackArray() == null || super.getStackArray().length == 0);
    }

    public boolean isFull() {
        return (super.getStackArray() != null && super.getStackArray().length == maxSize);
    }

    public int peek() {
        if (super.getStackArray() == null) {
            super.print("Brak elementow");
            return -1;
        } else {
            int number = super.getStackArray()[super.getStackArray().length - 1];
            super.print("Ostatni element na stosie to: " + number);
            return number;
        }
    }

    @Override
    public void push(int number) {
        if (this.isFull()) {
            super.print("Stos jest pełny!");
        } else {
            super.push(number);
        }

    }
}
