package pl.sda;

public interface DataStructure {
    void push(int number);

    int pop();

    int getSize();

    void print(String info);
}