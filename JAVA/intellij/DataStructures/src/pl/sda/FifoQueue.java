package pl.sda;

import java.util.Arrays;

public class FifoQueue implements DataStructure {
    private int[] queue = null;

    @Override
    public void push(int number) {
        if (queue == null) {
            queue = new int[1];
        } else {
            queue = Arrays.copyOf(queue, queue.length + 1);
        }
        queue[queue.length - 1] = number;
        this.print("Dodano do kolejki elemenet: " + number);
    }

    @Override
    public int pop() {
        if (queue == null || queue.length == 0) {
            this.print("Brak elementów w kolejce!");
            return -1;
        } else {
            int number = queue[0];
            queue = Arrays.copyOfRange(queue, 1, queue.length);
            this.print("Pobrano element: " + number);
            return number;
        }
    }

    @Override
    public int getSize() {
        if (queue == null || queue.length == 0) {
            this.print("Aktualny rozmiar kolejki: 0");
            return 0;
        } else {
            this.print("Aktualny rozmiar kolejki: " + queue.length);
            return queue.length;
        }
    }

    @Override
    public void print(String info) {
        System.out.println(info);
    }
}
