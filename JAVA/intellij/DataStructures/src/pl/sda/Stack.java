package pl.sda;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Stack implements DataStructure {
    private int[] stackArray;

    public Stack() {
    }

    public int[] getStackArray() {
        return stackArray;
    }

    @Override
    public void push(int number) {
        if (stackArray == null) {
            stackArray = new int[1];
        } else {
            stackArray = Arrays.copyOf(stackArray, stackArray.length + 1);
        }
        stackArray[stackArray.length - 1] = number;
        this.print("Zapisoano na stos element: " + number);
    }

    @Override
    public int pop() {
        if (stackArray == null) {
            this.print("Brak elementów na stosie");
            return -1;
        }
        int number = stackArray[stackArray.length - 1];
        stackArray = Arrays.copyOf(stackArray, stackArray.length - 1);
        if (stackArray.length == 0)
            stackArray = null;
        this.print("Pobrano ze stosu element: " + number);
        return number;
    }

    @Override
    public int getSize() {
        this.print("Rozmiar stosu: ");
        if (stackArray == null) {
            return 0;
        }
        return stackArray.length;
    }

    @Override
    public void print(String info) {
        System.out.println(info);
    }
}