package pl.sda;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = null;
        try {
            sc = new Scanner(System.in);
            FifoQueue queue = new FifoQueue();
            System.out.println("Ile elementów umieścić w kolejce?");
            int numberOfElements = sc.nextInt();
            for (int i = 0; i < numberOfElements; i++) {
                queue.push(i);
            }
        } finally {
            if (sc != null) {
                sc.close();
            }
        }

//        Stack stack = new Stack();
//        stack.push(5);
//        stack.push(4);
//        stack.push(6);
//        stack.pop();
//        stack.pop();
//        stack.pop();
//        stack.pop();
//
//        StackExt stackExt = new StackExt(4);
//        if (stackExt.isEmpty()) {
//            System.out.println("Pusto");
//        }
//        stackExt.push(1);
//        stackExt.push(2);
//        stackExt.push(3);
//        stackExt.push(4);
//        if (stackExt.isFull()) {
//            System.out.println("Pełno");
//        }
//        stackExt.push(11);
//        stackExt.pop();
    }
}
