package pl.sda;

import javax.faces.bean.ManagedBean;

@ManagedBean(name = "mainController", eager = true)
public class MainController {
    public MainController() {
        System.out.println("Main Controller!");
    }

    public String getInfo() {
        return "info";
    }
}
