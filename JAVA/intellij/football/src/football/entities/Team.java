package football.entities;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Team extends AbstractEntity {
    private String name;
    private Date foundationDate;

    public Team(Integer id, String name, Date foundationDate) {
        super(id);
        this.name = name;
        this.foundationDate = foundationDate;
    }

    public Team(Team team) {
        super(team.getId());
        this.name = team.getName();
        this.foundationDate = team.getFoundationDate();
    }

    public String getName() {
        return name;
    }

    public Date getFoundationDate() {
        return foundationDate;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setFoundationDate(Date foundationDate) {
        this.foundationDate = foundationDate;
    }

    @Override
    public String toString() {
        return ("Team ID: " + super.getId() + ", name: " + name + ", foundation date: " + new SimpleDateFormat("yyyy-MM-dd").format(foundationDate));
    }
}
