package football.entities;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Player extends AbstractEntity {
    private String name;
    private String surname;
    private Date dayOfBirth;
    private int speed;
    private Integer teamId;

    public Player(int id, String name, String surname, Date dayOfBirth, int speed, Integer teamId) {
        super(id);
        this.name = name;
        this.surname = surname;
        this.dayOfBirth = dayOfBirth;
        this.speed = speed;
        this.teamId = teamId;
    }

    public Player(Player player) {
        super(player == null ? 1 : player.getId());
        if (player == null) {
            throw new RuntimeException("Nieprawidłowy parametr konstruktora gracza");
        }
        this.name = player.getName();
        this.surname = player.getSurname();
        this.dayOfBirth = player.getDayOfBirth();
        this.speed = player.getSpeed();
        this.teamId = player.getTeamId();
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public Date getDayOfBirth() {
        return dayOfBirth;
    }

    public int getSpeed() {
        return speed;
    }

    public Integer getTeamId() {
        return teamId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setDayOfBirth(Date dayOfBirth) {
        this.dayOfBirth = dayOfBirth;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }



    @Override
    public String toString() {
        return super.getId() + "," + name + "," + surname + "," + new SimpleDateFormat("yyyy-MM-dd").format(dayOfBirth) + "," + speed + "," + teamId;
    }
}
