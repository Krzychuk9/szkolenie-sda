package football;

import football.repositories.DB.AbstractRepository;
import football.repositories.DB.PlayerRepository;
import football.repositories.DB.TeamRepository;
import football.entities.Player;
import football.entities.Team;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class MainDB {
    public static void main(String[] args) {
        AbstractRepository<Player> playerRepository = new PlayerRepository();
        AbstractRepository<Team> teamRepository = new TeamRepository();
        Player newPlayer = null;
        Team team = null;
        try {
            newPlayer = new Player(0, "JAVA2", "JAVA2", new SimpleDateFormat("yyyy-MM-dd").parse("2017-02-28"), 100, 1);
            team = new Team(0, "JAVA", new SimpleDateFormat("yyyy-MM-dd").parse("2016-10-10"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //playerRepository.add(newPlayer);
        //playerRepository.delete(605023);
        //Player player = playerRepository.get(605019);
        //System.out.println(player.toString());

        //teamRepository.add(team);
        //teamRepository.delete(28);
        //Team team = teamRepository.get(1);
        //System.out.println(team.toString());
    }
}
