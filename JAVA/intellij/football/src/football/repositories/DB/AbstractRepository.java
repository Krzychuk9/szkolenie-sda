package football.repositories.DB;

import football.DBConnection;
import football.entities.AbstractEntity;
import football.repositories.Repository;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public abstract class AbstractRepository<T extends AbstractEntity> implements Repository<T> {
    protected CallableStatement addCallableStatement;
    protected CallableStatement deleteCallableStatement;
    protected PreparedStatement getPreparedStatement;

    public AbstractRepository() {
        Connection connection = DBConnection.getConnection();
        try {
            addCallableStatement = connection.prepareCall(this.getAddSQL());
            deleteCallableStatement = connection.prepareCall(this.getDeleteSQL());
            getPreparedStatement = connection.prepareStatement(this.getGetSQL());
        } catch (SQLException e) {
            throw new RuntimeException("Błąd inicjalizacji " + AbstractEntity.class.getName());
        }
    }

    protected abstract String getAddSQL();

    protected abstract String getDeleteSQL();

    protected abstract String getGetSQL();

    public abstract void add(T element);

    public abstract T get(int id);

    public void delete(int id) {
        try {
            deleteCallableStatement.setInt(1, id);
            deleteCallableStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
