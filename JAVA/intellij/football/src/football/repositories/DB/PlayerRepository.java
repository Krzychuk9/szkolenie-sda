package football.repositories.DB;

import football.entities.Player;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

public class PlayerRepository extends AbstractRepository<Player> {

    public PlayerRepository() {
        super();
    }

    @Override
    protected String getAddSQL() {
        return "{CALL add_player(?,?,?,?,?,?,?)}";
    }

    @Override
    protected String getDeleteSQL() {
        return "{CALL delete_player(?)}";
    }

    @Override
    protected String getGetSQL() {
        return "SELECT * FROM pilkarz WHERE id_pilkarz = ?";
    }

    @Override
    public void add(Player player) {
        try {
            addCallableStatement.setString(1, player.getName());
            addCallableStatement.setString(2, player.getSurname());
            addCallableStatement.setDate(3, new java.sql.Date(player.getDayOfBirth().getTime()));
            addCallableStatement.setInt(4, player.getSpeed());
            addCallableStatement.setInt(5, 100);
            addCallableStatement.setInt(6, player.getTeamId());
            addCallableStatement.registerOutParameter(7, Types.INTEGER);
            addCallableStatement.executeQuery();
            player.setId(addCallableStatement.getInt(7));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Player get(int id) {
        Player player = null;
        try {
            getPreparedStatement.setInt(1, id);
            ResultSet resultSet = getPreparedStatement.executeQuery();
            if (resultSet.next()) {
                String name = resultSet.getString("imie");
                String surname = resultSet.getString("nazwisko");
                Date date = resultSet.getDate("data_urodzenia");
                int speed = resultSet.getInt("szybkosc");
                int teamId = resultSet.getInt("id_aktualnej_druzyny");
                player = new Player(id, name, surname, date, speed, teamId);
            } else {
                throw new RuntimeException("Brak zawodnika o wskazanym id = " + id);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return player;
    }
}