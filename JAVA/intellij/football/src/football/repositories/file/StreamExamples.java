package football.repositories.file;

import football.entities.Player;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class StreamExamples {

    public static void run() {
        List<Player> players = new ArrayList<>();
        players.add(new Player(1, "Adam", "Adamowski", new Date(1990, 1, 1), 10, null));
        players.add(new Player(2, "Bartosz", "Bartoszowski", new Date(1980, 1, 1), 70, null));
        players.add(new Player(3, "Cyprian", "Cyprianowicz", new Date(1965, 1, 1), 55, null));

        players.stream().map(p -> p.getName()).filter(p -> p.endsWith("am")).forEach(System.out::println);

        List<String> firstNames = players.stream().map(s -> s.getName()).collect(toList());
        System.out.println("Zwrócona lista imion: ");
        firstNames.stream().forEach(System.out::println);

        Optional<Player> optionalPlayer = players.stream().filter(p -> p.getSurname().equals("Adamowski")).findFirst();

        if (optionalPlayer.isPresent()) {
            Player player = optionalPlayer.get();
            System.out.println(player.getSurname());
        } else {
            System.out.println("Nie ma takiego piłkarza");
        }

        System.out.println("Zadania:");
        System.out.println("Speed > 50");
        players.stream().filter(p -> p.getSpeed() > 50).map(p -> p.getSurname()).forEach(System.out::println);
        System.out.println("Pobrane daty:");
        List<Date> dates = players.stream().filter(p -> p.getName().startsWith("Ada")).map(p -> p.getDayOfBirth()).collect(toList());
        dates.stream().forEach(System.out::println);

        players.stream().min(Comparator.comparing(Player::getDayOfBirth)).ifPresent(p -> System.out.println(p.getSurname()));

        players.stream().min((p1, p2) -> p1.getDayOfBirth().compareTo(p2.getDayOfBirth())).ifPresent(p -> System.out.println(p.getSurname()));

        Map<Integer, Player> mapPlayers = players.stream().collect(Collectors.toMap(k -> k.getId(), p -> p));
        System.out.println(mapPlayers.get(1));

        Map<String, List<Player>> mapPlayersNames = players.stream().collect(Collectors.groupingBy(Player::getName));
        System.out.println(mapPlayersNames.get("Adam").get(0));

    }

}
