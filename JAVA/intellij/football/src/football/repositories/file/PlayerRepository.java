package football.repositories.file;

import football.Utils;
import football.entities.Player;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class PlayerRepository extends Repository<Player> {
    private static final String FILE_NAME = "players.csv";

    public PlayerRepository() {
        super();
    }

    @Override
    protected String getFileName() {
        return FILE_NAME;
    }

    @Override
    protected Player getNewObject(Player obj) {
        return new Player(obj);
    }

    @Override
    protected Player getObjectFromFile(String line) {
        List<String> words = Utils.splitToTrimmedList(line, ",", 6, "Nie prawidłowe dane");
        Integer id = Utils.getIntegerFromString(words.get(0), "Błędne ID");
        String name = words.get(1);
        String surname = words.get(2);
        Date date = Utils.getDataFromText(words.get(3), "Nie prawidłowy format daty");
        int speed = Utils.getIntegerFromString(words.get(4), "Błędne dane");
        Integer teamId = Utils.getIntegerFromString(words.get(5), "Błędne dane", true);
        return new Player(id, name, surname, date, speed, teamId);
    }

    @Override
    protected String getFileLineFromObject(Player player) {
        return player.getId() + "," + player.getName() + "," + player.getSurname() + "," + new SimpleDateFormat("yyyy-MM-dd").format(player.getDayOfBirth()) + "," + player.getSpeed() + "," + player.getTeamId();
    }

    @Override
    protected void upDateObj(Player p, Optional<Player> optionalPlayer) {
        Player player = optionalPlayer.get();
        player.setName(p.getName());
        player.setSurname(p.getSurname());
        player.setDayOfBirth(p.getDayOfBirth());
        player.setSpeed(p.getSpeed());
        player.setTeamId(p.getTeamId());
    }


    public List<Player> getByNamePart(String namePart) {
        return super.list.stream().filter(p -> (p.getName() + p.getSurname()).contains(namePart) || (p.getSurname() + p.getName()).contains(namePart)).map(p -> new Player(p)).collect(Collectors.toList());
    }
}