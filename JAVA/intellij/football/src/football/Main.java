package football;


import football.entities.Player;
import football.entities.Team;
import football.repositories.file.PlayerRepository;
import football.repositories.file.TeamRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Main {
    private static PlayerRepository playerRepository;
    private static TeamRepository teamRepository;

    public static void main(String[] args) {
        playerRepository = new PlayerRepository();
        teamRepository = new TeamRepository();
        try {

            Player newPlayer = new Player(4, "AktualnyZiomek", "Ziomek", new SimpleDateFormat("yyyy-MM-dd").parse("2016-01-01"), 100, 1);
            //playerRepository.add(newPlayer);
            //playerRepository.delete(4);
            //playerRepository.upData(newPlayer);
            System.out.println(playerRepository.exist(2));

            Team team = new Team(4, "AktualnyTeam", new SimpleDateFormat("yyyy-MM-dd").parse("2016-10-10"));
            //teamRepository.add(team);
            //teamRepository.delete(4);
            //teamRepository.upData(team);
            System.out.println(teamRepository.exist(4));

        } catch (ParseException e) {
        }
    }
}


