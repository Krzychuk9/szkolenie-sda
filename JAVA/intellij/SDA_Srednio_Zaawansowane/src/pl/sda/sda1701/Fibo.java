package pl.sda.sda1701;


public class Fibo {

    /**
     * calc Fibonacci number
     *
     * @param x int
     * @return Fibonacci number of x
     */
    public int fibo(int x) {
        if (x > 1) {
            return fibo(x - 1) + fibo(x - 2);
        } else if (x == 1) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * calc Fibonacci number
     *
     * @param x int
     * @return Fibonacci number of x
     */
    public int fibo2(int x) {
        int x0 = 0;
        int x1 = 1;
        int x2 = 0;
        for (int i = 2; i <= x; i++) {
            x2 = x1 + x0;
            x0 = x1;
            x1 = x2;
        }
        return x2;
    }

    /**
     * Return all Fibonacci numbers up to x
     *
     * @param x int
     * @return Fibonacci numbers up to x
     */
    public int[] fiboArray(int x) {
        int[] result = new int[x + 1];
        for (int i = 0; i < result.length; i++) {
            result[i] = this.fibo(i);
        }
        return result;
    }
}
