package pl.sda.sda1701;

public class MaxMin {
    public int[] maxMin(int[] array) {
        int max = array[0];
        int min = array[0];
        for (int i = 1; i < array.length; i++) {
            if (max < array[i]) {
                max = array[i];
            }
            if(min > array[i]){
                min = array[i];
            }
        }
        int[] result = new int[2];
        result[0] = max;
        result[1] = min;
        return result;
    }


}
