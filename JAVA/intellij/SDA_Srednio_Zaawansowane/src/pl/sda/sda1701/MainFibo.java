package pl.sda.sda1701;

import java.util.Scanner;

public class MainFibo {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Fibo f = new Fibo();
        System.out.println("Podaj liczbe: ");
        int x = sc.nextInt();
        System.out.println("Rekurencyjnie: " +f.fibo(x));
        System.out.println("Pętla for: " + f.fibo2(x));

        int[] fiboArray = f.fiboArray(x);
        System.out.println("Wszystkie elementy ciągu: ");
        for (int i : fiboArray) {
            System.out.print(i + ", ");
        }
    }
}
