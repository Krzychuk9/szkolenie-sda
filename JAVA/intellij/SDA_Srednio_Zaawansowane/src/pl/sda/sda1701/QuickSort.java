package pl.sda.sda1701;

public class QuickSort {

    public QuickSort() { }

    /**
     * Sortowanie przy użyciu algorytmu quicksort
     * @param numbers Tablica do posortowania
     * @param start Indeks pierwszego elementu sortowanej tablicy
     * @param end  Indeks ostatniego elementu sortowanej tablicy
     */
    public void quicksort(int numbers[], int start, int end) {

        int i, j, v, tmp;

        i = start;
        j = end;
        v = numbers[(start+end)/2]; //TODO: określamy punkt podziału tablicy
        do {
            //jeśli element i jest mniejszy od punktu podziału, to przesuwamy indeks i w prawą stronę
            while (numbers[i]<v)
                i++;

            //jeśli element j jest większy od punktu podziału, to przesuwamy indeks j w lewą stronę
            while (v < numbers[j])
                j--;

            //jeśli po lewej stronie punktu podziału jest element większy od punktu podziały
            //a po prawej stronie mniejszy od punktu podziału, to zamieniamy te elementy
            if (i<=j) {
                tmp = numbers[i];
                numbers[i]=numbers[j];
                numbers[j]=tmp;
                i++;
                j--;
            }
        }
        while (i<=j); //pętlę wykonujemy do czasu aż i "minie się" z j

        if (start < j)
            quicksort(numbers,start,j);//TODO: rekurencyjnie wywołujemy quicksort dla pierwszej części tablicy
        if (i < end)
            quicksort(numbers,i,end);//TODO: rekurencyjnie wywołujemy quicksort dla drugiej częsci tablicy
    }
}
