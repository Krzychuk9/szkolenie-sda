package pl.sda.sda1701;

public class Sort {
    public int[] bubbleSort(int[] array) {
        int n = array.length;
        int[] result = array.clone();
        long startTime = System.currentTimeMillis();
        do {
            for (int i = 0; i < n - 1; i++) {
                if (result[i] > result[i + 1]) {
                    int tmp = result[i];
                    result[i] = result[i + 1];
                    result[i + 1] = tmp;
                }
            }
            n = n - 1;
        } while (n > 1);
        System.out.println(System.currentTimeMillis()-startTime);
        return result;
    }
}
