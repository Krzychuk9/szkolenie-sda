package pl.sda.sda1701;


import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Input {
    /**
     * return array of ints from a file
     * @param fileIn path to file with file name
     * @return int array
     * @throws IOException
     */
    public int[] returnIntArray(String fileIn) throws IOException {
        File f = new File(fileIn);
        Scanner sc = null;
        int[] intArray = null;
        try {
            sc = new Scanner(f);
            if (sc.hasNext()) {
                intArray = new int[sc.nextInt()];
            } else {
                return null;
            }
            for (int i = 0; i < intArray.length; i++) {
                if (sc.hasNextInt()) {
                    intArray[i] = sc.nextInt();
                }
            }
        } finally {
            if (sc != null) {
                sc.close();
            }
        }
        return intArray;
    }
    public Employe[] returnEmpArray(String fileIn) throws IOException{
        Employe[] emp = new Employe[10];
        int counter = 0;
        String line;
        try(BufferedReader br = new BufferedReader(new FileReader(fileIn))){
            while((line = br.readLine()) != null){
                String[] data = line.split(" ");
                emp[counter] = new Employe(data[0], Integer.parseInt(data[1]),Integer.parseInt(data[2]));
                counter++;
            }
        }
        return emp;
    }

}
