package pl.sda.sda1701;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


public class Output {
    /**
     * Save array to file
     * @param array array of ints
     * @throws IOException
     */
    public void saveIntArray(int[] array) throws IOException {
        File f = new File("result.txt");
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(f))) {
            for (int i = 0; i < array.length; i++) {
                bw.write(Integer.toString(array[i])+ " ");
            }
        }
    }
}
