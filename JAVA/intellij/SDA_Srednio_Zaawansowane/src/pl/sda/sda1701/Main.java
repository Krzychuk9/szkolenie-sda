package pl.sda.sda1701;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        Input in = new Input();
        Output out = new Output();
        Sort sort = new Sort();
        QuickSort qs = new QuickSort();
        MaxMin maxMin = new MaxMin();

        try {
            int[] array = in.returnIntArray("duzoLiczb.txt");
            array = sort.bubbleSort(array);
            out.saveIntArray(array);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            int[] array = in.returnIntArray("duzoLiczb.txt");
            long startTime = System.currentTimeMillis();
            qs.quicksort(array, 0, array.length - 1);
            long endTime = System.currentTimeMillis();
            System.out.println(endTime-startTime);
            out.saveIntArray(array);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try{
            int[] array = in.returnIntArray("duzoLiczb.txt");
            int[] eks = maxMin.maxMin(array);
            for (int element : eks) {
                System.out.println(element);
            }
        }catch (IOException e) {
            e.printStackTrace();
        }

    }
}
