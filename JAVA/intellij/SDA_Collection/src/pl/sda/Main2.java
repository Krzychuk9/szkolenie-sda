package pl.sda;

import java.util.Random;

public class Main2 {
    public static void main(String[] args) {

        int[][] intArray = new int[100][100];
        fillArray(intArray);
        showArray(intArray);
        changeInt(intArray);
        showArray(intArray);
    }

    public static void showArray(int[][] args) {
        for (int i = 0; i < args.length; i++) {
            for (int j = 0; j < args[i].length; j++) {
                System.out.print(args[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void changeInt(int[][] args) {
        for (int i = 0; i < args.length; i++) {
            for (int j = 0; j < args[i].length; j++) {
                if (args[i][j] > 50) {
                    args[i][j] = -100;
                }
            }
        }
    }

    public static void fillArray(int[][] args) {
        Random random = new Random();
        for (int i = 0; i < args.length; i++) {
            for (int j = 0; j < args[i].length; j++) {
                args[i][j] = random.nextInt(5000);
            }
        }
    }
}
