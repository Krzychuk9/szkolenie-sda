package pl.sda;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        double[] doubleTable = new double[7]; //one way
        double[] sampleTable = new double[]{1, 2, 3, 4, 5, 6, 7}; //second way
        String[] stringTable = {"Ala", "ma", "kota"}; //third way
        System.out.println(Arrays.toString(stringTable));
        String[] stringTable2 = new String[4];
//        stringTable2[0] = stringTable[0];
//        stringTable2[1] = stringTable[1];
//        stringTable2[2] = "czarnego";
//        stringTable2[3] = stringTable[2];
//        System.out.println(Arrays.toString(stringTable2));

        for (int i = 0; i < stringTable2.length; i++) {
            if (i == 2) {
                stringTable2[i] = "czanrego";
                stringTable2[i + 1] = stringTable[i];
                break;
            }
            stringTable2[i] = stringTable[i];
        }
        System.out.println(Arrays.toString(stringTable2));
        readArray(stringTable2);

        String task = "Polecenie ktorego nie widac na talicy elo elo elo elo";
        String[] split = task.split(" ");
        readArray(split);
        String[] newTask = deleteShort(split);
        readArray(newTask);
        readArray(split);
//        System.out.println(Arrays.toString(sampleTable));
//
//        for (double element : doubleTable) {
//            System.out.println(element);
//        }
//        for (String s : stringTable) {
//            System.out.println(s);
//        }
    }

    /**
     * reads array
     *
     * @param arrayToRead array of strings
     */
    public static void readArray(String... arrayToRead) {
        for (String s : arrayToRead) {
            System.out.print(s);
            System.out.print(", ");
        }
        System.out.println();
    }

//    public static String[] deleteShort(String[] array) {
//        String[] result = new String[array.length];
//        for (int i = 0; i < array.length; i++) {
//            if (array[i].length() < 4) {
//                result[i] = array[i];
//                array[i] = null;
//            }
//        }
//        return result;
//    }
//
//    public static String[] deleteShort2(String[] array) {
//        String[] result = new String[5];
//        int counter = 0;
//        for (int i = 0; i < array.length; i++) {
//            if (result[result.length - 1] != null) {
//                newArray(result);
//            }
//            if (array[i].length() < 4) {
//                result[counter] = array[i];
//                counter++;
//                array[i] = null;
//            }
//        }
//        return result;
//    }
//
//    public static String[] newArray(String... array) {
//        String[] result = Arrays.copyOf(array, array.length + 10);
//        return result;
//    }

    /**
     * delete short words and return them in array without nulls
     * @param array array of Strings
     * @return array of deleted Strings
     */
    public static String[] deleteShort(String[] array) {
        String[] result = new String[array.length];
        int counter = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i].length() < 4) {
                result[counter++] = array[i];
                array[i] = null;
            }
        }
        result = deleteNulls(result);
        return result;
    }

    public static String[] deleteNulls(String... array) {
        int lastIndex = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == null) {
                lastIndex = i;
                break;
            }
        }
        String[] result = new String[lastIndex];
        for (int i = 0; i < result.length; i++) {
            result[i] = array[i];
        }
        return result;
    }
}
