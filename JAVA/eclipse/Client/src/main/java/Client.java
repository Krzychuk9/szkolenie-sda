import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public class Client {
	private CloseableHttpClient httpClient;

	/**
	 * Tworzenie instancji httpClienta
	 */
	public void createClient() {
		httpClient = HttpClients.createDefault();
	}

	/**
	 * Wys�anie danych na serwer
	 * 
	 * @throws Exception
	 */
	public void send() throws Exception {
		try {
			// definiujemy adres serwera
			//HttpPost postRequest = new HttpPost("http://localhost:8444/FirstWebProject/reverse");
			HttpPost postRequest = new HttpPost("http://10.60.21.8:8444/Server/bank/reverse");
			//HttpPost postRequest = new HttpPost("http://10.60.21.8:8444/Bank/myBank/add");

			// tworzymy plik json i wysy�amy go do serwera
			StringEntity input = new StringEntity("Ala ma kota");
			//StringEntity input = new StringEntity("{\"name\": \"Krzysztof\", \"surname\": \"Kasprowski\"}");

			input.setContentType("application/text");
			//input.setContentType("application/json");
			postRequest.setEntity(input);

			HttpResponse response = httpClient.execute(postRequest);

			// b��d po stronie serwera
			if (response.getStatusLine().getStatusCode() != 201) {
				throw new RuntimeException("B��d po stronie serwera : " + response.getStatusLine().getStatusCode());
			}

			// parsowanie odpowiedzi z serwera
			BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent()), "UTF-8"));

			String output;
			System.out.println("Odpowied� z serwera: \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void send2() throws Exception {
		try {
			// definiujemy adres serwera
			//HttpGet getRequest = new HttpGet("http://localhost:8444/FirstWebProject/generator");
			HttpGet getRequest = new HttpGet("http://10.60.21.8:8444/Server/bank/reverse");

			HttpResponse response = httpClient.execute(getRequest);

			// b��d po stronie serwera
			if (response.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("B��d po stronie serwera : " + response.getStatusLine().getStatusCode());
			}

			// parsowanie odpowiedzi z serwera
			BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent()), "UTF-8"));

			String output;
			System.out.println("Odpowied� z serwera: \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Zamkni�cie po��czenia
	 * 
	 * @throws Exception
	 */
	public void disconnect() throws Exception {
		if (httpClient != null)
			httpClient.close();
	}

	public static void main(String[] args) {
		try {
			Client client = new Client();
			client.createClient();
			client.send();
			//client.send2();
			client.disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
