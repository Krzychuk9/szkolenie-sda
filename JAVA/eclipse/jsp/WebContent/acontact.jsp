
<%
	String name = (request.getParameter("name") != "") ? request.getParameter("name") : "SuperZiomek";
	session.setAttribute("name", name);
	String surname = (request.getParameter("surname") != "") ? request.getParameter("surname") : "Ziomek";
%>
<div class="container">

	<div class="starter-template">
		<h1><%=name%>
			<%=surname%>
			<small>Thanks for you contact!</small>
		</h1>
		<p>
			Your message:<strong> <%=request.getParameter("textarea")%></strong>
			have been sent.
		</p>
	</div>
</div>
<!-- /.container -->