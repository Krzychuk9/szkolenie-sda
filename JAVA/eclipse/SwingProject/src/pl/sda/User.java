package pl.sda;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class User implements Serializable {
	private String name;
	private String surname;
	private String address;
	private String phone;
	private String age;
	private String image;

	public User(){
		
	}
	
	public User(String name, String surname, String adres, String phone, String age) {
		this.name = name;
		this.surname = surname;
		this.address = adres;
		this.phone = phone;
		this.age = age;
	}

	@XmlElement
	public String getName() {
		return name;
	}

	@XmlElement
	public String getSurname() {
		return surname;
	}

	@XmlElement
	public String getAdres() {
		return address;
	}

	@XmlElement
	public String getPhone() {
		return phone;
	}

	@XmlElement
	public String getAge() {
		return age;
	}

	public String getAddress() {
		return address;
	}

	@XmlElement
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

}
