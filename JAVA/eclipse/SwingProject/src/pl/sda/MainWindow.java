package pl.sda;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Base64.Encoder;
import java.util.stream.Stream;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.awt.BorderLayout;

public class MainWindow {
	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private File jpgFile;
	private File userXMLFile;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		LookAndFeelInfo[] instakkedLookAndFeels = UIManager.getInstalledLookAndFeels();
		for (LookAndFeelInfo look : instakkedLookAndFeels) {
			System.out.println(look.getClassName());
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 138, 262);
		frame.getContentPane().add(panel);
		panel.setLayout(null);

		JButton btnWylij = new JButton("Wy\u015Blij");
		btnWylij.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					//Socket socket = new Socket("10.60.21.112", 9012);
					Socket socket = new Socket("10.60.21.1", 9012);
					
					InputStream inputStream = socket.getInputStream();
					OutputStream outputStream = socket.getOutputStream();

					final DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
					final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

					String line = null;
					dataOutputStream.writeBytes("HI/r");
					while ((line = bufferedReader.readLine()) != null) {
						if (line.equals("HELLO")) {
							dataOutputStream.writeBytes("SEND\r");
							dataOutputStream.flush();
						} else if (line.equals("OK")) {
							dataOutputStream.write(Files.readAllBytes(Paths.get(userXMLFile.getAbsolutePath())));
							dataOutputStream.flush();
						}
					}
					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		});
		btnWylij.setBounds(20, 35, 89, 23);
		panel.add(btnWylij);

		JButton btnDodaj = new JButton("Zapisz");
		btnDodaj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String name = textField.getText();
				String surname = textField_1.getText();
				String adres = textField_2.getText();
				String phone = textField_3.getText();
				String age = textField_4.getText();
				User user = new User(name, surname, adres, phone, age);

				try {
					// BufferedImage image = ImageIO.read(jpgFile);
					// DataBufferByte dataBuffer = (DataBufferByte)
					// image.getRaster().getDataBuffer();
					// String encodeToString =
					// encoder.encodeToString(dataBuffer.getData());
					Encoder encoder = Base64.getEncoder();
					String temp = encoder.encodeToString(Files.readAllBytes(Paths.get(jpgFile.getAbsolutePath())));
					user.setImage(temp);

					jaxbObjectToXml(user);
				} catch (IOException ex) {
					ex.printStackTrace();
				}
				// File file = new File(name.toLowerCase() +
				// surname.toLowerCase() + ".txt");
				// try (ObjectOutputStream os = new ObjectOutputStream(new
				// FileOutputStream(file))) {
				// os.writeObject(user);
				// os.flush();
				// } catch (IOException ex) {
				// ex.printStackTrace();
				// }
			}
		});
		btnDodaj.setBounds(20, 75, 89, 23);
		panel.add(btnDodaj);

		JButton btnZakocz = new JButton("Zobacz");
		btnZakocz.setBounds(20, 115, 89, 23);
		panel.add(btnZakocz);

		JPanel panel_1 = new JPanel();
		panel_1.setBounds(137, 0, 297, 262);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(null);

		JPanel panel_2 = new JPanel();
		panel_2.setBounds(30, 10, 142, 110);
		panel_1.add(panel_2);
		panel_2.setLayout(new BorderLayout(0, 0));

		JLabel lblNewLabel = new JLabel("Imie");
		lblNewLabel.setBounds(30, 130, 142, 22);
		panel_1.add(lblNewLabel);

		JLabel label = new JLabel("Nazwisko");
		label.setBounds(30, 170, 142, 22);
		panel_1.add(label);

		JLabel label_1 = new JLabel("Adres");
		label_1.setBounds(30, 212, 142, 22);
		panel_1.add(label_1);

		textField = new JTextField();
		textField.setBounds(30, 150, 86, 20);
		panel_1.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(30, 192, 86, 20);
		panel_1.add(textField_1);

		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(30, 234, 86, 20);
		panel_1.add(textField_2);

		JButton button = new JButton("Dodaj zdj\u0119cie");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				FileNameExtensionFilter filter = new FileNameExtensionFilter("JPG & GIF Images", "jpg", "gif");
				chooser.setFileFilter(filter);
				int returnVal = chooser.showOpenDialog(MainWindow.this.frame);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					jpgFile = chooser.getSelectedFile();
					JPicture picture = new JPicture();
					try {
						BufferedImage image = ImageIO.read(jpgFile);
						picture.setImage(image);
						picture.setBounds(0, 0, panel_2.getWidth(), panel_2.getHeight());
						panel_2.add(picture);
					} catch (IOException ex) {
						ex.printStackTrace();
					}
				}
			}
		});
		button.setBounds(174, 97, 123, 23);
		panel_1.add(button);

		JLabel label_2 = new JLabel("Telefon");
		label_2.setBounds(145, 130, 142, 22);
		panel_1.add(label_2);

		JLabel label_3 = new JLabel("Wiek");
		label_3.setBounds(145, 170, 142, 22);
		panel_1.add(label_3);

		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(145, 150, 86, 20);
		panel_1.add(textField_3);

		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(145, 192, 86, 20);
		panel_1.add(textField_4);
	}

	private User jaxbXMLToObject() {
		try {
			JAXBContext context = JAXBContext.newInstance(User.class);
			Unmarshaller un = context.createUnmarshaller();
			User emp = (User) un.unmarshal(new File("user.xml"));
			return emp;
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return null;
	}

	private void jaxbObjectToXml(User emp) {
		try {
			JAXBContext context = JAXBContext.newInstance(User.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(emp, System.out);
			userXMLFile = new File("user.xml");
			m.marshal(emp, userXMLFile);
		} catch (JAXBException ex) {
			ex.printStackTrace();
		}
	}
}
