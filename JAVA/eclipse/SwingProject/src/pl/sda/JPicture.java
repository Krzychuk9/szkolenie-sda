package pl.sda;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

public class JPicture extends JPanel {
	private BufferedImage image;

	public JPicture() {
	}

	public void setImage(BufferedImage image) {
		this.image = image;
		this.repaint();
	}

	@Override
	public void paint(Graphics g) {
		Graphics2D graphics = (Graphics2D) g;
		graphics.drawImage(image, 0, 0, this.getWidth(), this.getHeight(), null);
	}

}
