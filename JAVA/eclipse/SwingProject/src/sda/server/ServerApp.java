package sda.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerApp {

	public static void main(String[] args) {
		try {
			final ServerSocket serverSocket = new ServerSocket(9012, 20);
			while (true) {
				final Socket accept = serverSocket.accept();
				final Thread thread = new Connection(accept);
				thread.start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
