package sda.server;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class Connection extends Thread {

	private Socket socket;

	public Connection(Socket socket) {
		this.socket = socket;
	}

	@Override
	public void run() {
		try {
			InputStream inputStream = this.socket.getInputStream();
			OutputStream outputStream = this.socket.getOutputStream();

			final DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
			final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

			String line = null;
			File serverFile = new File("serverFile.xml");
			//serverFile.createNewFile();
			byte[] xml = null;

			while ((line = bufferedReader.readLine()) != null) {
				System.out.println(line);
				if (line.equals("HI")) {
					dataOutputStream.writeBytes("HELLO\r");
					dataOutputStream.flush();
				} else if (line.equals("SEND")) {
					dataOutputStream.writeBytes("OK\r");
					dataOutputStream.flush();
				} else {
					break;
				}
			}
			xml = line.getBytes();
			Files.write(Paths.get(serverFile.getAbsolutePath()), xml, StandardOpenOption.APPEND);
			this.socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
