package dz.text;

import java.util.Collection;
import java.util.HashSet;

import org.junit.*;

import dz.products.Products;
import dz.products.SetOfProducts;
import dz.products.SimpleProduct;

public class ZestawTest {

	@Test
	public void testPobierzCene() {
		Collection<Products> products = new HashSet<>();
		products.add(new SimpleProduct("Simple", 10, 1));
		SetOfProducts set = new SetOfProducts("Zestaw", products, 20);
		Assert.assertEquals(set.getPrice(), 8);
	}

}
