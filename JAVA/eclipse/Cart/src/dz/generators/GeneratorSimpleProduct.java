package dz.generators;

import java.util.Collection;
import java.util.HashSet;

import dz.products.SimpleProduct;

public class GeneratorSimpleProduct {
	private GeneratorSimpleProduct() {
	}

	public static SimpleProduct getSimple() {
		return new SimpleProduct("Simple", 50, 1);
	}

	public static Collection<SimpleProduct> getSimple(int amount, String name, int price, int weight) {
		Collection<SimpleProduct> simples = new HashSet<>();
		for (int i = 0; i < amount; i++) {
			simples.add(new SimpleProduct(name + i, price, weight));
		}
		return simples;
	}
}
