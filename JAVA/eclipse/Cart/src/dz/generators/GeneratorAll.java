package dz.generators;

import java.util.Collection;
import java.util.HashSet;

import dz.products.Products;
/**
 * super generic generator for all products in world!
 * @author Me?
 *
 * @param <T>products implements interface Products
 */
public abstract class GeneratorAll<T extends Products> {

	abstract T getInstance();

	public T getProduct() {
		T instance = getInstance();
		return instance;
	}

	public Collection<T> getProducts(int amount) {
		Collection<T> products = new HashSet<>();
		for(int i = 0; i < amount; i++){
			products.add(this.getInstance());
		}
		return products;
	}

}
