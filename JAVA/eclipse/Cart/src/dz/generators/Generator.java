package dz.generators;

import dz.cart.Cart;
import dz.cart.Person;

public class Generator {
	public static Cart getCart() {
		return new Cart(new Person("Imie", "Nazwisko"), 20);
	}
}
