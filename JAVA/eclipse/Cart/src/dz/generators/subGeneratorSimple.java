package dz.generators;

import dz.products.SimpleProduct;

public class subGeneratorSimple extends GeneratorAll<SimpleProduct> {

	@Override
	SimpleProduct getInstance() {
		return new SimpleProduct("Simple", 10, 2);
	}

}
