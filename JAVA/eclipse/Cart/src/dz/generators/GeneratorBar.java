package dz.generators;

import java.util.Collection;
import java.util.HashSet;

import dz.products.CandyBar;

public class GeneratorBar {
	private GeneratorBar() {
	}

	public static CandyBar getCandyBar() {
		return new CandyBar();
	}

	/**
	 * Generates a lot of candy bars 
	 * @param amount	how many you can eat?
	 * @return	collection of you favourite candy bars!
	 */
	public static Collection<CandyBar> getCandyBar(int amount) {
		Collection<CandyBar> bars = new HashSet<>();
		for (int i = 0; i < amount; i++){
			bars.add(new CandyBar());
		}
		return bars;
	}
}
