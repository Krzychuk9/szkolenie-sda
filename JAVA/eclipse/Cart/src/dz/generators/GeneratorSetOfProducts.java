package dz.generators;

import java.util.Collection;
import java.util.HashSet;

import dz.products.Products;
import dz.products.SetOfProducts;

public class GeneratorSetOfProducts {
	private GeneratorSetOfProducts() {
	}

	public static SetOfProducts getSetOfProducts(int discount, int barnumber, int simplenumber, int weight, int price) {
		Collection<Products> products = new HashSet<>();
		products.addAll(GeneratorBar.getCandyBar(barnumber));
		products.addAll(GeneratorSimpleProduct.getSimple(simplenumber, "Simple", price, weight));
		return new SetOfProducts("Zestaw", products, discount);
	}
}
