package dz.generators;

import dz.products.CandyBar;
import dz.products.Products;

public class SubGeneratorCandyBar extends GeneratorAll<CandyBar> {

	@Override
	CandyBar getInstance() {
		return new CandyBar();
	}
}
