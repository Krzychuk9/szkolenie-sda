package dz.products;

public class CandyBar implements Products {

	@Override
	public int getPrice() {
		return 100;
	}

	@Override
	public int getWeight() {
		return 20;
	}

	@Override
	public String getName() {
		return "Batonik";
	}

}
