package dz.products;

public interface Products {
	int getPrice();
	int getWeight();
	String getName();
}
