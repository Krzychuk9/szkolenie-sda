package dz.products;

public class SimpleProduct implements Products {
	private String name;
	private int price;
	private int weight;

	public SimpleProduct(String name, int price, int weight) {
		this.name = name;
		this.price = price;
		this.weight = weight;
	}

	@Override
	public int getPrice() {
		return price;
	}

	@Override
	public int getWeight() {
		return weight;
	}

	@Override
	public String getName() {
		return name;
	}

}
