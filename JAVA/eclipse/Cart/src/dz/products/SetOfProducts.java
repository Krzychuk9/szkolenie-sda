package dz.products;

import java.util.Collection;

public class SetOfProducts implements Products {
	private String name;
	private Collection<Products> products;
	private int discount;

	public SetOfProducts(String name, Collection<Products> products, int discount) {
		this.name = name;
		this.products = products;
		this.discount = discount;
	}

	@Override
	public int getPrice() {
		int price = 0;
		for (Products p : products) {
			price += p.getPrice();
		}
		return price * (100 - discount) / 100;
	}

	@Override
	public int getWeight() {
		int weight = 0;
		for (Products p : products) {
			weight += p.getWeight();
		}
		return weight;
	}

	@Override
	public String getName() {
		return name;
	}

}
