package dz.cart;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import dz.products.Products;

public class Cart {
	private Person person;
	private Collection<Products> products;
	private Date date;
	private int capacity;

	public Cart(Person person, int capacity) {
		this.person = person;
		this.capacity = capacity;
		this.date = new Date();
		this.products = new HashSet<>();
	}

	public Person getPerson() {
		return person;
	}

	public Collection<Products> getProducts() {
		return products;
	}

	public Date getDate() {
		return date;
	}

	public int getPrice() {
		int price = 0;
		for (Products p : products) {
			price += p.getPrice();
		}
		return price;
	}

	private int getFreeCapacity() {
		int freeCapacity = capacity;
		for (Products p : products) {
			capacity -= p.getWeight();
		}
		return freeCapacity;
	}

	public boolean addProduct(Products product) {
		if (product.getWeight() > this.getFreeCapacity()) {
			return false;
		} else {
			products.add(product);
			return true;
		}
	}
}
