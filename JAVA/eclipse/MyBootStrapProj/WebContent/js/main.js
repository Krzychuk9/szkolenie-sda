var players = [];
function player(name, surname, age, speed, tricks) {
	this.name = name;
	this.surname = surname;
	this.age = age;
	this.speed = speed;
	this.tricks = tricks;
}
players.push(new player("Tomasz", "Zimoch", 60, 40, 99));
players.push(new player("Tomasz", "Kukulski", 25, 60, 69));
players.push(new player("Robert", "Lewandowski", 27, 100, 100));
players.push(new player("Jakub", "Błaszczykowski", 28, 90, 100));
players.push(new player("Łukasz", "Piszczek", 25, 60, 50));

function addPlayers() {
	var $menuPlayers = $("#players-menu");
	for (var i = 0; i < players.length; i++) {
		$menuPlayers
				.append('<li><a class="player" href="details.html?player_index='
						+ i
						+ '"><span class="glyphicon glyphicon-user" aria-haspopup="true"></span>'
						+ players[i].name
						+ " "
						+ players[i].surname
						+ '</a></li>');
	}
}

function parametrUrl() {
    var qs = window.location.search.substring(1);
    qs = qs.split('+').join(' ');
    var params = {},
            tokens,
            re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
        params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
    }
    return params;
}

$(document).ready(function() {
	addPlayers();
});