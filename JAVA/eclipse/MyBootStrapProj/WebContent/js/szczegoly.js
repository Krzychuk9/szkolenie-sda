function pobierzParametryZURL() {
    var qs = window.location.search.substring(1);
    qs = qs.split('+').join(' ');
    var params = {},
            tokens,
            re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
        params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
    }

    return params;
}

function wyswietlSzczegolyPilkarza() {
    var parametryZURL = pobierzParametryZURL();
    var indexPilkarza = parametryZURL.indeks_pilkarza;
    var pilkarz = listaPilkarzy[indexPilkarza];
	var imieINazwisko = pilkarz.imie + " " + pilkarz.nazwisko;
	document.title = imieINazwisko;
    document.getElementById("imie_i_nazwisko").innerHTML = imieINazwisko;
    document.getElementById("wiek").innerHTML = pilkarz.wiek;
    document.getElementById("drybling").innerHTML = pilkarz.drybling;
    document.getElementById("szybkosc").innerHTML = pilkarz.szybkosc;
}