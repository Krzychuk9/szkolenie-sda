function serachAndOutput (){
	var paramURL = parametrUrl();
	var param = paramURL["searched-value"];
	if(param == null || param.trim() == ""){
		var result = [];
	}else{
		var result = serach(param);
	}
	
	if(result.length){
		$("#alert-success").fadeIn();
		showResult(result);
	}else{
		$("#alert-danger").fadeIn();
	}

}

function setModal($template, player){
	$template.click(function(){
		$("#name-modal").text(player.name + " " + player.surname);
		$("#input-age").val(player.age);
		$("#input-speed").val(player.speed);
		$("#input-tricks").val(player.tricks);
		$('#player-modal').modal('show');
	});	
}


function showResult(result){
	var $resultTemplate = $("#result-template");
	var $resultOuter = $("#result-template-full");
	$resultOuter.show();
	for(var i=0; i<result.length; i++){
		var player = result[i];
		var $template = $resultTemplate.clone();
		$template.removeAttr("id");
		$template.find(".nameAndSurname").text(player.name + " " + player.surname);
		$template.find(".player-age").text(player.age);
		$template.find(".player-speed").text(player.speed);
		$template.find(".player-tricks").text(player.tricks);
		$template.show();
		$("#result").append($template);
		setModal($template, player);
	}
	
}



function serach(param){
	var result = [];
	var data;
	for(var i = 0; i < players.length; i++){
		data = players[i].name + " " + players[i].surname + " " + players[i].name;
		if(data.toLowerCase().indexOf(param) != -1){
			result.push(players[i]);
		}
	}
	return result;
}

$(document).ready(function(){
	serachAndOutput();
});