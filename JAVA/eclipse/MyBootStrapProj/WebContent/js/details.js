
function showPlayerInfo() {
    var indexPlayer = parametrUrl().player_index;
    var $spanPlayer = $("#player-details");
    if (players[indexPlayer] === undefined) {
        $spanPlayer.text("Błędne dane!");
        return;

    }
    $spanPlayer.text(players[indexPlayer].name + " " + players[indexPlayer].surname);
    
    $("#player-age").text(players[indexPlayer].age);
    $("#player-speed").text(players[indexPlayer].speed);
    $("#player-tricks").text(players[indexPlayer].tricks);
}

$(document).ready(function(){
	showPlayerInfo();
});