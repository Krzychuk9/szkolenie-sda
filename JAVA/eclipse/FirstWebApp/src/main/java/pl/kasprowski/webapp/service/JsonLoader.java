package pl.kasprowski.webapp.service;

import com.google.gson.Gson;
import pl.sda.banktransations.Customer;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class JsonLoader {
    private Transations transations;

    public JsonLoader() {
        this.getData();
    }

    private void getData() {
        Gson gson = new Gson();
        try (FileReader fr = new FileReader(new File("C:\\apache-tomcat-9.0.0.M17\\Kowalski.json"))) {
            transations = gson.fromJson(fr, Transations.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public Customer getCustromer(){
        return transations.getCustomer();
    }

    public String getHtml() {
        StringBuffer sb = new StringBuffer();
        String link = "<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">";
        String scriptJQ = "<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js\"></script>";
        String scriptBS = "<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\" integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\" crossorigin=\"anonymous\"></script>";

        sb.append("<!DOCTYPE html>");
        sb.append("<head>");
        sb.append("<title>Transations</title>");
        sb.append(link);
        sb.append("</head>");
        sb.append("<body>");
        sb.append("<div class=\"container\">");
        sb.append("<table class=\"table\">");
        sb.append("<thead>");
        sb.append("<tr>");
        sb.append("<th>Name</th>");
        sb.append("<th>Surname</th>");
        sb.append("<th>Start balance</th>");
        sb.append("<th>End balance</th>");
        sb.append("</tr>");
        sb.append("</thead>");
        sb.append("<tbody>");
        sb.append("<tr><td>");
        sb.append(transations.getCustomer().getName());
        sb.append("</td>");
        sb.append("<td>");
        sb.append(transations.getCustomer().getSurname());
        sb.append("</td>");
        sb.append("<td>");
        sb.append(transations.getStartBalance());
        sb.append("</td>");
        sb.append("<td>");
        sb.append(transations.getEndBalance());
        sb.append("</td>");
        sb.append("</tr>");
        sb.append("</tbody>");
        sb.append("</table>");
        sb.append("</div>");
        sb.append(scriptJQ);
        sb.append(scriptBS);
        sb.append("</body>");
        return sb.toString();
    }

}
