package pl.kasprowski.webapp.service;

import pl.sda.banktransations.Customer;
import pl.sda.banktransations.Transation;

import java.util.List;

public class Transations {
    private List<Transation> transations;
    private Customer customer;
    private double startBalance;
    private double endBalance;

    @Override
    public String toString() {
        return "{\"transations\": " + "\"" + transations.toString() + "\"" + ",\"customer\": " + "\""
                + customer.toString() + "\"" + ",\"startBalance\": " + "\"" + startBalance + "\"" + ",\"endBalance\": "
                + "\"" + endBalance + "\"";
    }

    public List<Transation> getTransations() {
        return transations;
    }

    public double getStartBalance() {
        return startBalance;
    }

    public double getEndBalance() {
        return endBalance;
    }

    public Customer getCustomer() {
        return customer;
    }
}
