package pl.kasprowski.webapp;

import pl.kasprowski.webapp.service.JsonLoader;
import pl.kasprowski.webapp.service.LoginService;
import pl.sda.banktransations.Customer;

import javax.servlet.ServletContext;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.io.File;
import java.util.List;

@Path("/")
public class HelloWorld {

    @GET
    @Path("/hello")
    @Produces(MediaType.TEXT_PLAIN)
    public String getMessage() {
        return "Hello World";
    }

    @GET
    @Path("/json")
    @Produces("application/json")
    public String getJson() {
        return "{\"komunukat\": \"Hello user!\"}";
    }

    @GET
    @Path("/multiplay")
    @Produces("text/plain")
    public String multiply(@QueryParam("x") int x, @QueryParam("y") int y) {
        return Integer.toString(x * y);
    }

    @GET
    @Path("/devision")
    @Produces("text/plain")
    public String devision(@QueryParam("x") double x, @QueryParam("y") double y) {
        if (y == 0) {
            return "Nie dziel przez 0!";
        } else {
            return Double.toString(x / y);
        }
    }

    @GET
    @Path("/fibo")
    @Produces("text/plain")
    public String getFibo(@QueryParam("n") int n) {
        return Integer.toString(this.fibo(n));
    }

    public int fibo(int n) {
        if (n > 1) {
            return fibo(n - 1) + fibo(n - 2);
        } else if (n == 1) {
            return 1;
        } else {
            return 0;
        }
    }

    @GET
    @Path("/add")
    @Produces("text/plain")
    public String getSum(@Context UriInfo info) {
        MultivaluedMap<String, String> multi = info.getQueryParameters();
        int result = 0;
        for (List<String> list : multi.values()) {
            for (String s : list) {
                result += Integer.valueOf(s);
            }
        }
        return Integer.toString(result);
    }

    static int i = 1;

    @GET
    @Path("/generator")
    @Produces("text/plain")
    public String generator() {
        do {
            i++;
        } while (i % 3 != 0);
        return Integer.toString(i);
    }

    @GET
    @Path("/index")
    public Response showLoginPage() {
        String path = "C:\\Users\\Admin\\IdeaProjects\\FirstWebApp\\web\\index.jsp";
        File f = new File(path);
        Response response = Response.ok((Object) f, MediaType.TEXT_HTML).build();
        return response;
    }

    @POST
    @Path("/index")
    public String login(@FormParam("user") String name, @FormParam("password") String pass) {
        LoginService service = new LoginService("C:\\Users\\Admin\\IdeaProjects\\FirstWebApp\\web\\password.txt");
        boolean validUser = service.isValid(name, pass);
        if (validUser) {
            JsonLoader jsonLoader = new JsonLoader();
            Customer customer = jsonLoader.getCustromer();
            if(customer.getName().equalsIgnoreCase(name)){
                return jsonLoader.getHtml();
            }else{
                return "Brak tranzakcji dla uzytkownika " + name;
            }
        } else {
            return "Wrong Password!";
        }
    }
}
