package pl.kasprowski.webapp.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class LoginService {
    private Map<String, String> passwords = new HashMap<>();

    public LoginService(String fileIn) {
        this.getData(fileIn);
    }

    private void getData(String fileIn){
        try(BufferedReader br = new BufferedReader(new FileReader(new File(fileIn)))){
            String line;
            while ((line = br.readLine()) != null){
             String[] record = line.split(" ");
             passwords.put(record[0], record[1]);
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public boolean isValid(String user, String password){
        for (String s : passwords.keySet()) {
            if(s.equals(user) && passwords.get(s).equals(password)){
                return true;
            }
        }
        return false;
    }


}
