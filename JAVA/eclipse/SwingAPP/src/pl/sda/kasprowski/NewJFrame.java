package pl.sda.kasprowski;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class NewJFrame extends JFrame {

	JTextArea textArea;
	JButton button;
	JButton resetButton;
	JTextField textFiled;
	int counter = 0;

	public NewJFrame() {
		this.setSize(400, 400);
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension dimension = toolkit.getScreenSize();
		int width = dimension.width / 2 - this.getWidth() / 2;
		int height = dimension.height / 2 - this.getHeight() / 2;
		this.setLocation(width, height);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel panel1 = new JPanel();
		ListnerForMouse listnerForMouse = new ListnerForMouse();
		panel1.addMouseListener(listnerForMouse);
		JPanel panel2 = new JPanel();

		JLabel label = new JLabel("super labelka");
		label.setToolTipText("elo");
		label.setFont(new Font("Comic Sans MS", Font.PLAIN, 25));
		panel1.add(label);

		button = new JButton("button");
		button.setBorderPainted(false);
		button.setToolTipText("do crazy things");
		button.setPreferredSize(new Dimension(200, 30));
		ListnerForButton listnerForButton = new ListnerForButton();
		button.addActionListener(listnerForButton);
		button.addMouseListener(listnerForMouse);
		panel1.add(button);

		resetButton = new JButton("Clear");
		resetButton.addActionListener(new ListnerForButton());
		panel1.add(resetButton);

		textFiled = new JTextField(20);
		textFiled.setBackground(Color.CYAN);
		ListnerForKey listnerForKey = new ListnerForKey();
		textFiled.addKeyListener(listnerForKey);
		panel1.add(textFiled);

		JCheckBox box = new JCheckBox("srly?");
		box.setSelected(true);
		panel1.add(box);

		textArea = new JTextArea(5, 15);
		textArea.setText("TEXT\n");
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		JScrollPane jScrollPane = new JScrollPane(textArea, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		panel1.add(jScrollPane);

		JRadioButton radio1 = new JRadioButton("option1");
		JRadioButton radio2 = new JRadioButton("option2");
		JRadioButton radio3 = new JRadioButton("option3");
		JRadioButton radio4 = new JRadioButton("option4");
		ButtonGroup group = new ButtonGroup();
		group.add(radio1);
		group.add(radio2);
		group.add(radio3);
		group.add(radio4);
		panel2.add(radio1);
		panel2.add(radio2);
		panel2.add(radio3);
		panel2.add(radio4);
		radio1.setSelected(true);
		panel1.add(panel2);

		JSlider slider = new JSlider(0, 99, 0);
		slider.setMinorTickSpacing(1);
		slider.setMajorTickSpacing(10);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		panel1.add(slider);

		ListnerForWindow listnerForWindow = new ListnerForWindow();
		this.add(panel1);
		this.addWindowListener(listnerForWindow);
		this.setVisible(true);
		textFiled.requestFocus();
	}

	private class ListnerForButton implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == button) {
				textArea.append("Button clicked " + ++counter + " times\n");
			} else if (e.getSource() == resetButton) {
				textArea.setText("");
			}

		}

	}

	private class ListnerForKey implements KeyListener {

		@Override
		public void keyPressed(KeyEvent e) {
			if (e.getSource() == textFiled) {
				textArea.append("key " + e.getKeyChar() + " pressed\n");
			}

		}

		@Override
		public void keyReleased(KeyEvent e) {
		}

		@Override
		public void keyTyped(KeyEvent e) {
		}
	}

	private class ListnerForWindow implements WindowListener {

		@Override
		public void windowActivated(WindowEvent arg0) {
			textArea.append("window activated\n");

		}

		@Override
		public void windowClosed(WindowEvent arg0) {
		}

		@Override
		public void windowClosing(WindowEvent arg0) {
		}

		@Override
		public void windowDeactivated(WindowEvent arg0) {
			textArea.append("window deactivated\n");

		}

		@Override
		public void windowDeiconified(WindowEvent arg0) {
		}

		@Override
		public void windowIconified(WindowEvent arg0) {
		}

		@Override
		public void windowOpened(WindowEvent arg0) {
		}
	}

	private class ListnerForMouse implements MouseListener {
		private String text;

		@Override
		public void mouseClicked(MouseEvent e) {
			textArea.append("Mouse clicked: " + e.getClickCount() + " times\n");
			textArea.append("Mouse panel pos: " + e.getX() + " " + e.getY() + "\n");
			textArea.append("Mouse screen pos: " + e.getXOnScreen() + " " + e.getYOnScreen() + "\n");
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			if (e.getSource() == button) {
				text = textArea.getText();
				textArea.append("hohoho");
			}

		}

		@Override
		public void mouseExited(MouseEvent e) {
			if (e.getSource() == button) {
				textArea.setText(text);
			}
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub

		}

	}
}
