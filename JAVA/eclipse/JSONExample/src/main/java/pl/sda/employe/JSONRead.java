package pl.sda.employe;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

public class JSONRead {
	public void readyFromJeason(String path) throws IOException {
		JsonReader reader = new JsonReader(new FileReader(new File(path)));

		while (reader.hasNext()) {
			JsonToken token = reader.peek();
			switch (token) {
			case BEGIN_OBJECT:
				reader.beginObject();
				System.out.println(reader.nextName());
				break;
			case BEGIN_ARRAY:
				reader.beginArray();
				while (!reader.peek().equals(JsonToken.END_ARRAY)) {
					if (reader.peek().equals(JsonToken.BEGIN_OBJECT)) {
						reader.beginObject();
						do {
							reader.nextName();
							JsonToken token2 = reader.peek();
							switch (token2) {
							case STRING:
								System.out.print(reader.nextString() + " ");
								break;
							case NUMBER:
								System.out.print(reader.nextDouble() + " ");
								break;
							}
						} while (!reader.peek().equals(JsonToken.END_OBJECT));
						System.out.println();
						reader.endObject();
					}
				}
				reader.endArray();
				break;
			case STRING:
				System.out.println(reader.nextString());
				break;
			case NAME:
				System.out.println(reader.nextName());
				break;
			case NUMBER:
				System.out.println(reader.nextDouble());
				break;
			}

		}
	}
}
