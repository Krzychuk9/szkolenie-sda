package pl.sda;

import com.google.gson.Gson;

public class JSONExample {
	public static void main(String[] args) {
		Person p = new Person("Jan","Kowalski",44,true);
		Gson gson = new Gson();
		
		String text = gson.toJson(p);
		System.out.println(text);
		
		String text2 = "{\"name\":\"Jan\",\"surname\":\"Nowak\",\"age\":44,\"isMan\":true}";
		Person p1 = gson.fromJson(text2, Person.class);
		System.out.println(p1.toString());
	}
}
