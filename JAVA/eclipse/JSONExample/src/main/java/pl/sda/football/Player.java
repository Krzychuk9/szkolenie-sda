package pl.sda.football;

public class Player {
	private String name;
	private String surname;
	private int age;
	private Position position;

	public Player(String name, String surname, int age, Position position) {
		this.name = name;
		this.surname = surname;
		this.age = age;
		this.position = position;
	}

	@Override
	public String toString() {
		return "\nImie: " + name + ", Nazwisko: " + surname + ", wiek: " + age + ", pozycja: " + position;
	}

}
