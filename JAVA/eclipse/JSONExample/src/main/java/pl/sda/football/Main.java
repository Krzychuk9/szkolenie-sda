package pl.sda.football;

import java.io.IOException;

public class Main {
	public static void main(String[] args) {
		JsonLogger logger = new JsonLogger();
		FootBallClub club = new FootBallClub("InterMechnica", 1999, "Jan", "Kowalski", 20000);
		club.addPlayer(new Player("Robert", "Lewandowski", 25, Position.STRIKER));
		club.addPlayer(new Player("Jakub", "Blaszczykowski", 25, Position.DEFENDER));
		club.addPlayer(new Player("Arkadiusz", "Milik", 25, Position.STRIKER));
		club.addPlayer(new Player("Wojciech", "Szczesny", 25, Position.GOALKEPPER));

		try {
			logger.saveToFile("C:\\Users\\RENT\\workspace\\JSONExample\\club.json", club);
			logger.loadFromFile2("club.json");
		} catch (IOException e) {
			System.out.println("Something goes wrong");
		}

	}
}
