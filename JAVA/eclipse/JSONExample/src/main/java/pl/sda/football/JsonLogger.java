package pl.sda.football;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonLogger {
	private Gson gson;

	public void saveToFile(String fileOut, FootBallClub element) throws IOException {
		gson = new GsonBuilder().setPrettyPrinting().create();
		FileWriter fw = null;
		try {
			fw = new FileWriter(new File(fileOut));
			String json = gson.toJson(element);
			fw.write(json);
			System.out.println("Plik zapisany!");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (fw != null) {
				fw.close();
			}
		}
	}

	public void loadFromFile(String fileIn) throws IOException {
		gson = new GsonBuilder().setPrettyPrinting().create();
		try (FileReader fr = new FileReader(new File(fileIn))) {
			StringBuffer sb = new StringBuffer();
			int c;
			while ((c = fr.read()) != -1) {
				sb.append((char) c);
			}
			String json = sb.toString();
			System.out.println(json);
			System.out.println(gson.fromJson(json, FootBallClub.class));

		}
	}
	public void loadFromFile2(String fileIn) throws IOException {
		gson = new Gson();
		try (FileReader fr = new FileReader(new File(fileIn))) {
			FootBallClub team = gson.fromJson(fr, FootBallClub.class);
			System.out.println(team.toString());

		}
	}
	
	

}
