package pl.sda;

public class Person {
	private String name;
	private String surname;
	private int age;
	private boolean isMan;

	public Person(String name, String surname, int age, boolean isMan) {
		super();
		this.name = name;
		this.surname = surname;
		this.age = age;
		this.isMan = isMan;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public boolean isMan() {
		return isMan;
	}

	public void setMan(boolean isMan) {
		this.isMan = isMan;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", surname=" + surname + ", age=" + age + ", isMan=" + isMan + "]";
	}

}
