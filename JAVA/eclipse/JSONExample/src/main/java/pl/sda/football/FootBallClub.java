package pl.sda.football;

import java.util.Arrays;

public class FootBallClub {
	private String name;
	private int year;
	private String nameOfBoss;
	private String surnameOfBoss;
	private int stadionCapacity;
	private Player[] players;

	public FootBallClub(String name, int year, String nameOfBoss, String surnameOfBoss, int stadionCapacity) {
		this.name = name;
		this.year = year;
		this.nameOfBoss = nameOfBoss;
		this.surnameOfBoss = surnameOfBoss;
		this.stadionCapacity = stadionCapacity;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		if (players != null) {
			for (Player player : players) {
				sb.append(player.toString());
			}
		}
		return "Klub pilkarski: " + name + "\nRok zalozenia: " + year + "\nPrezes: " + nameOfBoss + " " + surnameOfBoss
				+ "\nPojemnosc stadionu: " + stadionCapacity + "\nZawodnicy: " + sb.toString();
	}

	public void addPlayer(Player player) {
		if (players == null) {
			players = new Player[1];
			players[0] = player;
		} else {
			this.players = Arrays.copyOf(this.players, players.length + 1);
			this.players[players.length - 1] = player;
		}
	}

}
