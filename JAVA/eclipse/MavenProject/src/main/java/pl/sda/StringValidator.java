package pl.sda;

import org.apache.commons.lang3.StringUtils;

public class StringValidator {
	public double getEquals(String text1, String text2) {
		return StringUtils.getJaroWinklerDistance(text1, text2);
	}
}
