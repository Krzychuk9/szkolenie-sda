package pl.sda;

import org.apache.commons.validator.routines.EmailValidator;
import org.apache.commons.validator.routines.ISBNValidator;
import org.apache.commons.validator.routines.UrlValidator;

public class Validator {
	private ISBNValidator isbn = new ISBNValidator();
	private UrlValidator url = new UrlValidator();
	private EmailValidator email = EmailValidator.getInstance();

	public boolean validateISBM(String code) {
		return isbn.isValid(code);
	}

	public boolean validateURL(String value) {
		return url.isValid(value);
	}

	public boolean validate(String value) {
		return email.isValid(value);
	}
}
