package pl.sda;

import org.apache.commons.lang3.math.NumberUtils;

public class NumberValidator {
	public int getMax(int[] array){
		return NumberUtils.max(array);
	}
	public int getMin(int[] array){
		return NumberUtils.min(array);
	}
}
