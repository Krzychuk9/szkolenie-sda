package pl.sda.java.service;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import com.google.gson.Gson;

import pl.sda.banktransations.transations.Transation;
import pl.sda.banktransations.transations.Transations;

public class Reader {

	public Transations readers(String fileIn) throws IOException{
		FileReader fr = null;
		try
		{
			Gson gson = new Gson();
			fr = new FileReader(new File(fileIn));
			Transations transations = gson.fromJson(fr, Transations.class);
			return transations;
		}
		finally
		{
					
		}	
	}
	
	
	
	public void toHTML() throws IOException{
		
		PrintWriter print = null;
		Transations transations = this.readers("C:\\Users\\RENT\\workspace\\FirstWebProject\\transation.json");
			
		String style_1 = "table, th, td {\n" +
                "    border: 1px solid black;\n" +
                "    border-collapse: collapse;\n" +
                "}\n" +
                "th, td {\n" +
                "    padding: 5px;\n" +
                "    text-align: left;\n" +
                "}\n" +
                "table tr:nth-child(even) {\n" +
                "    background-color: #eee;\n" +
                "}\n" +
                "table tr:nth-child(odd) {\n" +
                "   background-color:#fff;\n" +
                "}\n" +
                "table th {" +
                "    background-color: black;" +
                "    color: white;" +
                "}";
		
		try{
			print = new PrintWriter("WebContent/wyjscie.html"); 
			 print.print("<!DOCTYPE html>"
					 + "<html>"
					 + "<head>"
					 +"<meta charset = 'UTF=8'>"
					 +"<style>" +style_1 + "</style>"
					 +"</head>"
					 +"<body>"
					 +"<table>"
						  +"<thead>"
						    +"<tr>"
							   +"<th> Imie </th>"  
							   +"<th> Nazwisko </th>"  
							   +"<th> Wiek </th>"  
							   +"<th> Adres </th>" 
							   +"<th> Kwota poczatkowa </th>" 
							   +"<th> Kwota ko�cowa </th>" 
						   +"</tr>"		
						 +"</thead>"
					 +"<tbody>"

					 +"<tr>"
				 		+"<td>"+ transations.getCustomer().getName() + "</td>"
				 				+"<td>"+ transations.getCustomer().getSurname() + "</td>"
				 						+"<td>"+ transations.getCustomer().getAge() + "</td>"
				 								+"<td>"+ transations.getCustomer().getAdres() + "</td>"			
				 										+"<td>"+ transations.getStartBalance()+ "</td>"	
				 												+"<td>"+ transations.getEndBalance()+ "</td>"	
					 +"</tr>"					 
					 +"</tbody>"					 
					 +"</table>"
					 
					 
					 +"<table>"
					 +"<thead>"
					   +"<tr>"
						   +"<th> Numer Tranzakcji</th>"  
						   +"<th> Tranzakcje </th>"  
					  +"</tr>"		
					+"</thead>"
					+"<tbody>"
					 );                 
			    int i =0;
			 	for ( Transation  el: transations.getTransations() ) {
					
					print.print("<tr>");				
					print.print("<td style='text-align:center;'>" + i++ +"</td>");
					print.print("<td>");
					print.print(el.getDate()+"   " + el.getPrice()+"$"+"  "+ el.getType());
					print.print("</td>");					
					print.print("</tr>");
				}
				
				print.print(		 
					"</tbody>"					 
					+"</table>"
					 +"</body>"
					 +"</html>"
					 );
		}
		finally
		{
		if(print != null)
			print.close();		
		}

	}

}
