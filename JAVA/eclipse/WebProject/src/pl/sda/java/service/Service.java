package pl.sda.java.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;

import javax.ws.rs.core.Response.ResponseBuilder;

@Path("/")
public class Service {
	@GET
	@Path("/json")
	@Produces("application/json")
	public String getProductInJSON() {
		return "{\"komunikat\" : \"Dzie� dobry, chyba nam si� uda�o;)\"";
	}

	@GET
	@Path("/multiply")
	@Produces("text/html")
	public String multiply(@QueryParam("num1") int x, @QueryParam("num2") int y) {
		int result = x * y;
		return Integer.toString(result);
	}

	@GET
	@Path("/devision")
	@Produces("text/html")
	public String devide(@QueryParam("num1") double x, @QueryParam("num2") double y) {
		if (y == 0) {
			return "Nie dziel przez zero!";
		} else {
			return "Wynik " + Double.toString(x / y);
		}
	}

	@GET
	@Path("/fibo")
	@Produces("text/html")
	public String fibo(@QueryParam("n") int n) {
		return "Wynik " + Integer.toString(this.fiboImpl(n));
	}

	public int fiboImpl(int n) {
		if (n > 1) {
			return fiboImpl(n - 1) + fiboImpl(n - 2);
		} else if (n == 1) {
			return 1;
		} else {
			return 0;
		}
	}

	@GET
	@Path("/add")
	@Produces("text/plain")
	public String add(@Context UriInfo info) {
		MultivaluedMap<String, String> multi = info.getQueryParameters();
		int result = 0;
		for (List<String> list : multi.values()) {
			for (String s : list) {
				result += Integer.valueOf(s);
			}
		}
		// multi.values().stream().forEach((s) -> s.forEach((p)->result +=
		// Integer.valueOf(p)));
		return "Wynik " + Integer.toString(result);
	}

	static int result = 0;

	@GET
	@Path("/generator")
	@Produces("text/plain")
	public String generator() {

		do {
			result++;
		} while (result % 3 != 0);
		return Integer.toString(result);
	}

	@POST
	@Path("/reverse")
	@Consumes("application/text")
	public Response reverse(String str) {
		str = StringUtils.reverse(str);
		return Response.status(201).entity(str).build();
	}

//	@GET
//	@Path("/login")
//	@Produces("text/html")
//	public InputStream showLoginPage() throws Exception {
//		File f = new File("C:/Users/RENT/workspace/FirstWebProject/WebContent/index.html");
//		return new FileInputStream(f);
//	}

	@POST
	@Path("/login")
	public Response login(@Context ServletContext context, @FormParam("name") String login, @FormParam("password") String password) {
		Reader reader = new Reader();
		try {
			reader.toHTML();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String path = context.getRealPath("/" + "wyjscie.html");
	    File file = new File(path);
		ResponseBuilder response = Response.ok((Object) file, MediaType.TEXT_HTML);
		//LoginValidator validator = new LoginValidator();
		if (true)
			return response.build();
		else
			return Response.status(401).entity("Niepoprawne has�o " + login).build();
	}
}
