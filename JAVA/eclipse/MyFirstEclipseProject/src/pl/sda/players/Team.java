package pl.sda.players;

import java.util.*;
import java.util.function.Function;

import javax.xml.transform.Templates;

public class Team {
	private Set<TeamPleyers> team = new HashSet<>();
	private String adres;
	private String teamName;

	public Team(String teamName, String adres) {
		this.adres = adres;
		this.teamName = teamName;
		this.setTeam();
	}

	@Override
	public String toString() {
		String result = "Nazwa drużyny: " + teamName + ", adres: " + adres + " piłkarze w drużynie: [\n";

		for (TeamPleyers teamplayer : team) {
			result += teamplayer.toString() + "\n";
		}
		result += " ]";
		return result;
	}

	public Player getMostExpesivePlayer() {
		Player max = null;

		for (TeamPleyers teamplayer : team) {
			if (max == null) {
				max = teamplayer.getPlayer();
			} else if (teamplayer.getPlayer().getPrice() > max.getPrice()) {
				max = teamplayer.getPlayer();
			}
		}
		return max;
	}

	public Player getMostExpensivePlayerStream() {
		return team.stream().max(new PleyerComparator().thenComparing(c -> c.getPlayer().getPrice())).get().getPlayer();
	}

	public Player getMostExpensivePlayerStream2() {
		return team.stream().max(new PleyerComparator()).get().getPlayer();
	}

	public double getTeamPrice() {
		double sum = 0.0;
		for (TeamPleyers teamplayer : team) {
			sum += teamplayer.getPlayer().getPrice();
		}
		return sum;
	}

	public double getTeamPriceStream() {
		double sum = 0.0;
		// team.stream().forEach((s) -> sum += s.getPlayer().getPrice());
		return sum;
	}

	private void setTeam() {
		Player p1 = new Player("Jakub", "Błaszczykowski", new PlayerStats(30, 50, 80));
		Player p2 = new Player("Arkadiusz", "Milik", new PlayerStats(50, 50, 60));
		Player p3 = new Player("Rober", "Lewandowski", new PlayerStats(90, 80, 80));
		Player p4 = new Player("Ktos", "Fajny", new PlayerStats(100, 100, 100));
		Player p5 = new Player("Jakub", "Błaszczykowski", new PlayerStats(30, 50, 80));
		Player p6 = new Player("SuperPilkarz", "Bramkarz", new PlayerStats(30, 50, 80));
		Player p7 = new Player("Labda", "Stream", new PlayerStats(120, 120, 120));

		TeamPleyers tp1 = new TeamPleyers(p1, 10, Position.MIDFIELDER);
		TeamPleyers tp2 = new TeamPleyers(p2, 5, Position.STRIKER);
		TeamPleyers tp3 = new TeamPleyers(p3, 7, Position.STRIKER);
		TeamPleyers tp4 = new TeamPleyers(p4, 19, Position.DEFENDER);
		TeamPleyers tp5 = new TeamPleyers(p5, 2, Position.DEFENDER);
		TeamPleyers tp6 = new TeamPleyers(p6, 99, Position.GOALKEPPER);
		TeamPleyers tp7 = new TeamPleyers(p7, 12, Position.GOALKEPPER);

		team.add(tp1);
		team.add(tp2);
		team.add(tp3);
		team.add(tp4);
		team.add(tp5);
		team.add(tp6);
		team.add(tp7);
	}

}
