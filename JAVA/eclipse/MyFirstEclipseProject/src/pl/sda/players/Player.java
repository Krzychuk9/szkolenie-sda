package pl.sda.players;

public class Player {
	private String name;
	private String surname;
	private PlayerStats stats;

	public Player(String name, String surname, PlayerStats stats) {
		this.name = name;
		this.surname = surname;
		this.stats = stats;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public PlayerStats getStats() {
		return stats;
	}

	public void setStats(PlayerStats stats) {
		this.stats = stats;
	}

	public double getPrice() {
		double price = 0.0;
		if (stats != null) {
			price = stats.getSpeed() * 50 / 3 + stats.getTricks() * 10000 + stats.getShot() * 50 / 4;
		}
		return price;
	}

	@Override
	public String toString() {
		return "{Imi�: " + name + ", Nazwisko: " + surname + ", " + stats.toString() + "}";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((surname == null) ? 0 : surname.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Player other = (Player) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (surname == null) {
			if (other.surname != null)
				return false;
		} else if (!surname.equals(other.surname))
			return false;
		return true;
	}

}
