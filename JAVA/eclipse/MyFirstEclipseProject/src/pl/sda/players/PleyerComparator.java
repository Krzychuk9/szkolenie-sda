package pl.sda.players;

import java.util.Comparator;

public class PleyerComparator implements Comparator<TeamPleyers> {

	@Override
	public int compare(TeamPleyers arg0, TeamPleyers arg1) {
		return (int) (arg0.getPlayer().getPrice() - arg1.getPlayer().getPrice());
	}

}
