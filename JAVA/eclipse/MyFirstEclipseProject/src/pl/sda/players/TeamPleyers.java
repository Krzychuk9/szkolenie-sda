package pl.sda.players;

public class TeamPleyers {
	private Player player;
	private int number;
	private Position position;

	public TeamPleyers(Player player, int number, Position position) {
		this.player = player;
		this.number = number;
		this.position = position;
	}

	public Player getPlayer() {
		return player;
	}

	@Override
	public String toString() {
		return "{pi�karz:  " + player.toString() + ", number: " + Integer.toString(number) + ", pozycja: "
				+ position.toString() + "}";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((player == null) ? 0 : player.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TeamPleyers other = (TeamPleyers) obj;
		if (player == null) {
			if (other.player != null)
				return false;
		} else if (!player.equals(other.player))
			return false;
		return true;
	}

}
