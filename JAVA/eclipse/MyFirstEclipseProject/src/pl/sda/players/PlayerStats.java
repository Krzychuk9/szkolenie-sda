package pl.sda.players;

public class PlayerStats {
	private int speed;
	private int tricks;
	private int shot;

	public PlayerStats(int speed, int tricks, int shot) {
		this.speed = speed;
		this.tricks = tricks;
		this.shot = shot;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getTricks() {
		return tricks;
	}

	public void setTricks(int tricks) {
		this.tricks = tricks;
	}

	public int getShot() {
		return shot;
	}

	public void setShot(int shot) {
		this.shot = shot;
	}

	@Override
	public String toString() {
		return "Stats: speed: " + Integer.toString(speed) + " tricks: " + Integer.toString(tricks) + " shot: "
				+ Integer.toString(shot);
	}

}
