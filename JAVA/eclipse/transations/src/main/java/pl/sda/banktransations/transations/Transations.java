package pl.sda.banktransations.transations;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Transations {

	private List<Transation> transations = new ArrayList<>();
	private final Customer customer;
	private double startBalance;
	private double endBalance;

	public Transations(Customer customer, double startBalance) {
		this.customer = customer;
		this.startBalance = startBalance;
	}

	public void addTransation() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Set date (DD-MM-YY): ");
		String date = sc.nextLine();
		System.out.println("Choose option 1.CASH 2. NON-CASH");
		String option = sc.nextLine();
		TransType type;
		switch (option) {
		case "1":
			type = TransType.CASH;
			break;
		case "2":
			type = TransType.NON_CASH;
			break;
		default:
			type = TransType.CASH;
		}

		System.out.println("Set price: ");
		double price = sc.nextDouble();
		sc.nextLine();
		transations.add(new Transation(date, type, price));
		this.setEndBalance();

	}

	private void setEndBalance() {
		double result = 0.0;
		for (Transation tran : transations) {
			result += tran.getPrice();
		}
		this.endBalance = startBalance - result;
	}

	public void print() {
		for (Transation tran : transations) {
			System.out.println(tran.toString());
		}
	}

	@Override
	public String toString() {
		return "{\"transations\": " + "\"" + transations.toString() + "\"" + ",\"customer\": " + "\""
				+ customer.toString() + "\"" + ",\"startBalance\": " + "\"" + startBalance + "\"" + ",\"endBalance\": "
				+ "\"" + endBalance + "\"";
	}

	public List<Transation> getTransations() {
		return transations;
	}

	public Customer getCustomer() {
		return customer;
	}

	public double getStartBalance() {
		return startBalance;
	}

	public double getEndBalance() {
		return endBalance;
	}

	public void setEndBalance(double endBalance) {
		this.endBalance = endBalance;
	}

}
