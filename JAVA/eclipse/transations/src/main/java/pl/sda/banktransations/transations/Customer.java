package pl.sda.banktransations.transations;

import java.util.Scanner;

public class Customer {
	private String name;
	private String surname;
	private int age;
	private String adres;

	private Customer() {

	}

	private void setName(String name) {
		this.name = name;
	}

	private void setSurname(String surname) {
		this.surname = surname;
	}

	private void setAge(int age) {
		this.age = age;
	}

	private void setAdres(String adres) {
		this.adres = adres;
	}

	@Override
	public String toString() {
		return "{\"name\": " + "\"" + name + "\"" + ",\"surname\": " + "\"" + surname + "\"" + ",\"age\": " + "\"" + age
				+ "\"" + ",\"adres\": " + "\"" + adres + "\"";
	}

	public static Customer getCustomer() {
		Scanner sc = new Scanner(System.in);
		Customer customer = new Customer();
		System.out.println("Name: ");
		customer.setName(sc.nextLine());
		System.out.println("Surname: ");
		customer.setSurname(sc.nextLine());
		System.out.println("Age: ");
		customer.setAge(sc.nextInt());
		sc.nextLine();
		System.out.println("Adres: ");
		customer.setAdres(sc.nextLine());
		return customer;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public int getAge() {
		return age;
	}

	public String getAdres() {
		return adres;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((adres == null) ? 0 : adres.hashCode());
		result = prime * result + age;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((surname == null) ? 0 : surname.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (adres == null) {
			if (other.adres != null)
				return false;
		} else if (!adres.equals(other.adres))
			return false;
		if (age != other.age)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (surname == null) {
			if (other.surname != null)
				return false;
		} else if (!surname.equals(other.surname))
			return false;
		return true;
	}

}
