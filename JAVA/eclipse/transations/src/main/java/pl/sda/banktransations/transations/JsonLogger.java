package pl.sda.banktransations.transations;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonLogger {
	public void saveToJSON(String fileout, Transations element) throws IOException {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		File file = new File(fileout);
		if (file.exists()) {
			try (FileReader fr = new FileReader(file)) {
				Transations elementIn = gson.fromJson(fr, Transations.class);
				if (elementIn.getCustomer().equals(element.getCustomer())) {
					element.getTransations().addAll(elementIn.getTransations());
					element.setEndBalance(
							element.getEndBalance() - (elementIn.getStartBalance() - elementIn.getEndBalance()));
				} else {
					file = new File(element.getCustomer().getSurname() + element.getCustomer().hashCode() + ".json");
				}
			}
			try (FileWriter fw = new FileWriter(file)) {
				String text = gson.toJson(element);
				fw.write(text);
			}
		} else {
			try (FileWriter fw = new FileWriter(file)) {
				String text = gson.toJson(element);
				fw.write(text);
			}

		}
	}
}
