package pl.sda.banktransations.transations;

import java.util.Date;

public class Transation {
	private String date;
	private TransType type;
	private double price;

	public Transation(String date, TransType type, double price) {
		this.date = date;
		this.type = type;
		this.price = price;
	}

	@Override
	public String toString() {
		return "{\"date\": " + "\"" + date + "\"" + ",\"type\": " + "\"" + type + "\"" + ",\"price\": " + "\"" + price
				+ "\"";
	}

	public double getPrice() {
		return price;
	}

	public String getDate() {
		return date;
	}

	public TransType getType() {
		return type;
	}
	
}
