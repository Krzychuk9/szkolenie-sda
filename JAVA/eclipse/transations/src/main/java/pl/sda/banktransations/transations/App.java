package pl.sda.banktransations.transations;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class App {
	public void start() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Hello!");
		Customer customer = Customer.getCustomer();
		System.out.println("Set start balance: ");
		double startBalance = sc.nextDouble();
		Transations trans = new Transations(customer, startBalance);
		
		while (true) {
			System.out.println("Choose option:\n1. Add transation \n2. Save to file \n3.Exit");
			try{
			
			int option = sc.nextInt();
			switch (option) {
			case 1:
				trans.addTransation();
				break;
			case 2:
				JsonLogger logger = new JsonLogger();
				logger.saveToJSON(trans.getCustomer().getSurname()+".json", trans);
				break;
			case 3:
				System.out.println("Bye!");
				return;
			default:
				System.out.println("default");
			}
			}catch(InputMismatchException e){
				System.out.println("Wrong option");
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

	}
}
