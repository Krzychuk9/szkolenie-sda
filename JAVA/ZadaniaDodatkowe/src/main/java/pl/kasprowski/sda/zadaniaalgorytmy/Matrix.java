package pl.kasprowski.sda.zadaniaalgorytmy;

public class Matrix {

    public void print(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public int[][] sum(int[][] matrix1, int[][] matrix2) {
        int[][] result = new int[matrix1.length][matrix1[1].length];
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[i].length; j++) {
                result[i][j] = matrix1[i][j] + matrix2[i][j];
            }
        }
        return result;
    }

    public int[][] multiply(int[][] matrix1, int[][] matrix2) {
        int[][] result = null;

        if (matrix1.length == matrix2[0].length) {
            result = matrix1;
            matrix1 = matrix2;
            matrix2 = result;
        }else if(matrix1[0].length != matrix2.length){
            result = new int[1][1];
            System.out.println("Blędne dane!");
            return result;
        }
        result = new int[matrix1.length][matrix2[0].length];

        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[i].length; j++) {
                int r = 0;
                for (int x = 0; x < matrix1[i].length; x++) {
                    r += matrix1[i][x] * matrix2[x][j];
                }
                result[i][j] = r;
            }
        }
        return result;
    }
}
