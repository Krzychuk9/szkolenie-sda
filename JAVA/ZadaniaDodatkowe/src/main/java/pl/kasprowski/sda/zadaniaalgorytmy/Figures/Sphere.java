package pl.kasprowski.sda.zadaniaalgorytmy.Figures;

public class Sphere {

    public double getVolume(double R) {
        return 4 / 3 * Math.PI * R * R * R;
    }

    public double getArea(double R, double H) {
        return 4 * Math.PI * R * R;
    }
}
