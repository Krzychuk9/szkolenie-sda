package pl.kasprowski.sda.zadaniabiblioteki;

import com.itextpdf.text.DocumentException;

import java.io.IOException;
import java.text.ParseException;

public class Main {
    public static void main(String[] args) {
        System.out.println(JaroWinkler.analyze("Frog", "Fog"));

        PDFCreator pdfCreator = new PDFCreator();
        try {
            pdfCreator.creatPDF("C:\\Users\\Admin\\IdeaProjects\\ZadaniaDodatkowe\\src\\main\\resources\\simplePDF.pdf", pdfCreator.getDataFromTXT("C:\\Users\\Admin\\IdeaProjects\\ZadaniaDodatkowe\\src\\main\\resources\\test.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }

        XLSCreator xlsCreator = new XLSCreator();
        try {
            xlsCreator.parseXml("C:\\Users\\Admin\\IdeaProjects\\ZadaniaDodatkowe\\src\\main\\resources\\xmlFile.xml");
            xlsCreator.saveToXLS("products.xls");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (org.dom4j.DocumentException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
