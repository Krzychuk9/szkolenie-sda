package pl.kasprowski.sda.zadaniabiblioteki;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Product {
    private int code;
    private String name;
    private double price;
    private int quantity;
    private Date expDate;
    private String date;
    private double value;

    public Product(int code, String name, double price, int quantity, String date) {
        this.code = code;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.date = date;
        this.setExpDate();
        this.setValue();
    }

    private void setExpDate() {
        try {
            SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd");
            this.expDate = parser.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void setValue() {
        this.value = price * quantity;
    }

    @Override
    public String toString() {
        return "Product{" +
                "code=" + code +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                ", expDate=" + expDate +
                ", date='" + date + '\'' +
                ", value=" + value +
                '}';
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getExpDate() {
        return new SimpleDateFormat("yyyy-MM-dd").format(expDate);
    }

    public double getValue() {
        return value;
    }
}

