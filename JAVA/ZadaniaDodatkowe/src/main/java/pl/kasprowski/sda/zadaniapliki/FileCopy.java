package pl.kasprowski.sda.zadaniapliki;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class FileCopy {

    /**
     * copy files
     *
     * @param path    path to directory with files to copy
     * @param desc    descination path
     * @param modDate "YYYY-MM-DD H:M" date format, files before this time will be coped
     * @throws IOException
     */
    public void copyFiles(String path, String desc, String modDate) throws IOException {
        File fileIn = new File(path);
        Date date;
        try {
            SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd h:mm");
            date = parser.parse(modDate);
        } catch (ParseException e) {
            System.out.println("Błędny format daty!");
            return;
        }
        long time = date.getTime();
        Path newDir = FileSystems.getDefault().getPath(desc);
        for (File f : fileIn.listFiles()) {
            if (f.lastModified() < time && !f.isDirectory()) {
                Path source = FileSystems.getDefault().getPath(f.getAbsolutePath());
                Files.copy(source, newDir.resolve(source.getFileName()), REPLACE_EXISTING);
                System.out.println("File " + source.getFileName() + " copied!");
            }
        }


    }


}
