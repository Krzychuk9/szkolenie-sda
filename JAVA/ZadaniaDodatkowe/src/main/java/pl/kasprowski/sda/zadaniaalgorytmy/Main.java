package pl.kasprowski.sda.zadaniaalgorytmy;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        MathFunc math = new MathFunc();
        System.out.println(math.sumaArytmetyczny(8, -2, 10));
        System.out.println(math.sumaGeometryczny(0.1, 3, 7));
        System.out.println(math.getNWD(1122, 867));

        Matrix matrix = new Matrix();
        int[][] matrix1 = {{1, 2, 3}, {1, 2, 3}, {1, 2, 3}};
        int[][] matrix2 = {{3, 4, 5}, {3, 4, 5}, {3, 4, 5}};

        System.out.println("Dodawanie macierzy: ");
        matrix.print(matrix1);
        matrix.print(matrix2);
        matrix.print(matrix.sum(matrix1, matrix2));

        System.out.println("Mnożenie macierzy: ");
        int[][] matrix3 = {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 8, 7, 6}};
        int[][] matrix4 = {{4, 3, 2, 1, 2}, {3, 4, 5, 6, 7}, {8, 9, 8, 7, 6}, {5, 4, 3, 2, 1}};
        matrix.print(matrix3);
        matrix.print(matrix4);
        matrix.print(matrix.multiply(matrix3, matrix4));

        System.out.println("Sortowanie: ");
        int[] array = {1,6,43,78,3,4,7,32,56,7,2};
        new Sort().heapSort(array);
        System.out.println(Arrays.toString(array));
    }

}
