package pl.kasprowski.sda.zadaniaalgorytmy;

public class MathFunc {

    public int sumaArytmetyczny(int a1, int r, int n) {
        return (2 * a1 + (n - 1) * r) * n / 2;
    }

    public double sumaGeometryczny(double a1, double q, int n) {
        if (q == 1) {
            return n * a1;
        } else {
            return ((a1 * (1 - Math.pow(q, n))) / (1 - q));
        }
    }

    public int getNWD(int a, int b) {
        int c;
        while (b != 0) {
            c = a % b;
            a = b;
            b = c;
        }
        return a;
    }

}
