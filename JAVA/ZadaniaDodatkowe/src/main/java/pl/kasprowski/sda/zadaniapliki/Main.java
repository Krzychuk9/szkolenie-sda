package pl.kasprowski.sda.zadaniapliki;

import java.io.IOException;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Input input = new Input();
        try {
            List<String> list = input.getWordsFromFile("C:\\Users\\Admin\\IdeaProjects\\ZadaniaDodatkowe\\src\\main\\resources\\test.txt");
            WordValidator validator = new WordValidator();
            System.out.println("Słowa o pażystej liczbie znaków: " + validator.countEvenWords(list));
            System.out.println("Najdłuższe słowa: ");
            validator.getLongestWords(list);
            System.out.println("Slowa posortowane alfabetycznie: ");
            validator.sortWords(list);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileCopy fc = new FileCopy();
        try {
            fc.copyFiles("C:\\Users\\Admin\\IdeaProjects\\ZadaniaDodatkowe\\src\\main\\resources", "C:\\Users\\Admin\\IdeaProjects\\ZadaniaDodatkowe\\src\\main\\resources\\pliki", "2017-03-11 19:26");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Crusher crusher = new Crusher();
        crusher.deleteFiles("C:\\Users\\Admin\\IdeaProjects\\ZadaniaDodatkowe\\src\\main\\resources", "pli");
    }

}
