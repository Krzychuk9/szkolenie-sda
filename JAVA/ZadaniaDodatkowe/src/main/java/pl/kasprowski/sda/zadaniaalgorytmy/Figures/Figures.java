package pl.kasprowski.sda.zadaniaalgorytmy.Figures;

public interface Figures {
    double getVolume(double R, double H);
    double getArea(double R, double H);

}
