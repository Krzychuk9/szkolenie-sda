package pl.kasprowski.sda.zadaniaalgorytmy;

public class Sort {

    public void heapSort(int[] array) {
        int count = array.length;
        heapify(array, count);
        int end = count - 1;
        while (end > 0) {
            int tmp = array[end];
            array[end] = array[0];
            array[0] = tmp;
            siftDown(array, 0, end - 1);
            end--;
        }
    }

    private void heapify(int[] a, int count) {
        int start = (count - 2) / 2;
        while (start >= 0) {
            siftDown(a, start, count - 1);
            start--;
        }
    }

    private void siftDown(int[] array, int start, int end) {
        int root = start;
        while ((root * 2 + 1) <= end) {
            int child = root * 2 + 1;
            if (child + 1 <= end && array[child] < array[child + 1]) {
                child = child + 1;
            }
            if (array[root] < array[child]) {
                int tmp = array[root];
                array[root] = array[child];
                array[child] = tmp;
                root = child;
            } else {
                return;
            }
        }
    }
}
