package pl.kasprowski.sda.zadaniajava8;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        Countries countries = new Countries();
        countries.sortedCountires();
        System.out.println();
        countries.getCountries(4);
        System.out.println();
        countries.getCountries("Po");

        Company company = new Company();
        System.out.println("Pracownicy w wieku powyżej 40 lat: ");
        company.getEmployee((e) -> e.getAge() > 40);
        System.out.println("Męższczyźni zarabiający ponad 3000zł: ");
        company.getEmployee((e) -> e.getSex().equals("Male") && e.getSalary() > 3000);
        System.out.println("Kobity o nazwiskach zaczynających się na literę K, pensja z zakresu od 2000zł do 5000zł");
        company.getEmployee((e) -> e.getSex().equals("Female") && e.getSurname().startsWith("K") && e.getSalary() > 2000 && e.getSalary() < 5000);

        try {
            company.saveToJson("company.json");
            company.getJsonFromFile("C:\\Users\\Admin\\IdeaProjects\\ZadaniaDodatkowe\\company.json");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
