package pl.kasprowski.sda.zadaniabiblioteki;


import org.apache.commons.lang3.StringUtils;

public class JaroWinkler {
    public static double analyze(String s1, String s2){
        return StringUtils.getJaroWinklerDistance(s1,s2);
    }

}
