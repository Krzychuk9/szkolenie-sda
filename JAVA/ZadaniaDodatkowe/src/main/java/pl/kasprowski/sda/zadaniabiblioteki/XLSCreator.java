package pl.kasprowski.sda.zadaniabiblioteki;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class XLSCreator {
    private List<Product> products = new ArrayList<>();
    private List<String> productsCols = new ArrayList<>();

    /**
     * Parses xml file with products data and add it to list
     *
     * @param fileIn xml file to parse
     * @throws IOException
     * @throws DocumentException
     */
    public void parseXml(String fileIn) throws IOException, DocumentException {
        File file = new File(fileIn);
        SAXReader reader = new SAXReader();
        Document document = reader.read(file);
        List<Node> nodes = document.selectNodes("/produkty/produkt");
        for (Node node : nodes) {
            int code = Integer.parseInt(node.selectSingleNode("kod").getText());
            String name = node.selectSingleNode("nazwa").getText();
            double price = Double.parseDouble(node.selectSingleNode("cena").getText());
            int quantity = Integer.parseInt(node.selectSingleNode("ilosc").getText());
            String date = node.selectSingleNode("data_waznosci").getText();
            products.add(new Product(code, name, price, quantity, date));
        }
        productsCols.add("Id");
        productsCols.add("Nazwa");
        productsCols.add("Cena");
        productsCols.add("Ilość");
        productsCols.add("Wartość");
        productsCols.add("Data ważności");
    }

    /**
     * Creates xls file based on data from parsed xml file
     *
     * @param fileName output file name
     * @throws IOException
     * @throws ParseException
     */
    public void saveToXLS(String fileName) throws IOException, ParseException {
        Calendar calendar = Calendar.getInstance();
        Calendar calendarAfter = Calendar.getInstance();
        calendarAfter.add(Calendar.MONDAY, 1);
        SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd");

        Workbook wb = new HSSFWorkbook();
        Sheet sheet = wb.createSheet("Products Summery");
        CellStyle csBold = wb.createCellStyle();
        CellStyle csDateR = wb.createCellStyle();
        CellStyle csDateY = wb.createCellStyle();
        Font f = wb.createFont();
        f.setBold(true);
        csBold.setFont(f);
        csDateR.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        csDateY.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        csDateY.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
        csDateR.setFillForegroundColor(IndexedColors.RED.getIndex());

        Row initialRow = sheet.createRow(0);
        for (int x = 0; x < productsCols.size(); x++) {
            Cell cell = initialRow.createCell(x);
            cell.setCellStyle(csBold);
            cell.setCellValue(productsCols.get(x));
        }

        for (int i = 0; i < products.size(); i++) {
            Row row = sheet.createRow(i + 1);
            int j = 0;
            row.createCell(j++).setCellValue(products.get(i).getCode());
            row.createCell(j++).setCellValue(products.get(i).getName());
            row.createCell(j++).setCellValue(products.get(i).getPrice());
            row.createCell(j++).setCellValue(products.get(i).getQuantity());
            row.createCell(j++).setCellValue(products.get(i).getValue());
            sheet.setColumnWidth(j, 4000);

            Cell cell = row.createCell(j++);
            if (calendar.getTime().compareTo(parser.parse(products.get(i).getExpDate())) == 1) {
                cell.setCellStyle(csDateR);
            } else if (calendarAfter.getTime().compareTo(parser.parse(products.get(i).getExpDate())) == 1) {
                cell.setCellStyle(csDateY);
            }
            cell.setCellValue(products.get(i).getExpDate());
        }
        Row endRow = sheet.createRow(products.size() + 1);
        Cell cell = endRow.createCell(3);
        cell.setCellValue("SUMA: ");
        cell.setCellStyle(csBold);
        cell = endRow.createCell(4);
        cell.setCellValue(products.stream().mapToDouble(product -> product.getValue()).sum());
        cell.setCellStyle(csBold);

        try (FileOutputStream fileOut = new FileOutputStream(fileName)) {
            wb.write(fileOut);
        }
    }
}
