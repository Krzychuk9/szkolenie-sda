package pl.kasprowski.sda.zadaniapliki;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Input {

    /**
     * Reads words from file and return them in list
     *
     * @param fileIn file path
     * @return String list
     * @throws IOException
     */
    public List<String> getWordsFromFile(String fileIn) throws IOException {
        List<String> list = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(new File(fileIn)))) {
            String line;
            while ((line = br.readLine()) != null) {
                list.addAll(Arrays.asList(line.split("\\s+")));
            }
        }
        return list;
    }


}
