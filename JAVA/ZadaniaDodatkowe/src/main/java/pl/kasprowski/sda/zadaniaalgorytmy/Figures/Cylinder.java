package pl.kasprowski.sda.zadaniaalgorytmy.Figures;

public class Cylinder implements Figures {

    @Override
    public double getVolume(double R, double H) {
        return Math.PI*R*R*H;
    }

    @Override
    public double getArea(double R, double H) {
        return 2*Math.PI*R*(R+H);
    }
}
