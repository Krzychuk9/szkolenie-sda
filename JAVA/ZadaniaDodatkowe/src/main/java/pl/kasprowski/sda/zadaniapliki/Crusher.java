package pl.kasprowski.sda.zadaniapliki;

import java.io.File;
import java.util.Arrays;

public class Crusher {

    /**
     * Deletes file from path, which name starts with indicated chars
     *
     * @param path
     */
    public void deleteFiles(String path, String chars) {
        File directory = new File(path);
        File[] files = directory.listFiles((File dir, String name) -> name.startsWith(chars));
        Arrays.stream(files).filter((p) -> !p.isDirectory()).forEach((p) -> p.delete());
        System.out.println("Files has beed deleted!");
    }
}
