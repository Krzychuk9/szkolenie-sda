package pl.kasprowski.sda.zadaniabiblioteki;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.*;

public class PDFCreator {

    /**
     * Reads content from txt file and return it as String
     *
     * @param fileIn path to file
     * @return string
     * @throws IOException
     */
    public String getDataFromTXT(String fileIn) throws IOException {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(new File(fileIn)))) {
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        }
        return sb.toString();
    }

    /**
     * Creates pdf file with content from param
     *
     * @param fileOut path to file
     * @param content String to write in pdf
     * @throws IOException
     * @throws DocumentException
     */
    public void creatPDF(String fileOut, String content) throws IOException, DocumentException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(fileOut));
        document.open();
        Paragraph paragraph = new Paragraph(content);
        document.add(paragraph);
        document.close();
    }
}
