package pl.kasprowski.sda.zadaniajava8;

public class Employee {
    private String name;
    private String surname;
    private int age;
    private double salary;
    private String sex;

    public Employee(String name, String surname, int age, double salary, String sex) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.salary = salary;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getAge() {
        return age;
    }

    public double getSalary() {
        return salary;
    }

    public String getSex() {
        return sex;
    }

    @Override
    public String toString() {
        return name + " " + surname + ", " + age + " lat " + salary + "zl ";
    }
}
