package pl.kasprowski.sda.zadaniaalgorytmy.Figures;

public class Cube extends Cuboid {

    public double getVolume(double a) {
        return super.getVolume(a, a, a);
    }

    public double getArea(double a) {
        return super.getArea(a, a, a);
    }
}
