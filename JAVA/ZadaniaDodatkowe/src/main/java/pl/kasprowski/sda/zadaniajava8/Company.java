package pl.kasprowski.sda.zadaniajava8;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class Company {
    private List<Employee> employees = new ArrayList<>();

    public Company() {
        this.getEmployees();
    }

    /**
     * Fill the list with some employees
     */
    private void getEmployees() {
        employees.add(new Employee("Jan", "Kowalski", 40, 2500, "Male"));
        employees.add(new Employee("Mariusz", "Nowak", 36, 3600, "Male"));
        employees.add(new Employee("Andrzej", "Bargiel", 30, 5200, "Male"));
        employees.add(new Employee("Dawid", "Basinski", 25, 2200, "Male"));
        employees.add(new Employee("Juliusz", "Cezar", 99, 1000, "Male"));
        employees.add(new Employee("Pan", "Kierownik", 50, 10000, "Male"));
        employees.add(new Employee("Aneta", "Kowalska", 30, 2100, "Female"));
        employees.add(new Employee("Agata", "Nowak", 44, 3200, "Female"));
        employees.add(new Employee("Klaudia", "Bargiel", 33, 5100, "Female"));
        employees.add(new Employee("Agnieszka", "Basinska", 21, 4000, "Female"));
        employees.add(new Employee("Julia", "Cezar", 25, 1200, "Female"));
        employees.add(new Employee("Michalina", "Janeczek", 30, 1800, "Female"));
    }

    /**
     * Prints to console employee who satisfy the condition in param
     *
     * @param predicate lambda condition
     */
    public void getEmployee(Predicate<Employee> predicate) {
        employees.stream().filter(predicate).forEach(System.out::println);
    }

    /**
     * Save this obj to JSON file
     *
     * @param fileOut path
     * @throws IOException
     */
    public void saveToJson(String fileOut) throws IOException {
        try (FileWriter fw = new FileWriter(new File(fileOut))) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            fw.write(gson.toJson(this));
        }
    }

    /**
     * Reads employees from a file
     *
     * @param fileIn path
     * @throws IOException
     */
    public void getJsonFromFile(String fileIn) throws IOException {
        try (FileReader fr = new FileReader(new File(fileIn))) {
            Gson gson = new Gson();
            Company company = (gson.fromJson(fr, Company.class));
            System.out.println("Pracownicy odczytani z pliku Json: ");
            company.employees.stream().forEach(System.out::println);
        }
    }
}
