package pl.kasprowski.sda.zadaniaalgorytmy.Figures;

public class Cone implements Figures {

    @Override
    public double getVolume(double R, double H) {
        return 1 / 3 * Math.PI * R * R * H;
    }

    @Override
    public double getArea(double R, double H) {
        double l = Math.sqrt(R * R + H * H);
        return Math.PI * R * (R + l);
    }
}
